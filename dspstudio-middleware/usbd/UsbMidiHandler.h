#pragma once

#include "daisy_seed.h"
#include "MidiParser.h"

namespace daisy {

class UsbMidiHandler {
public:

    /** Specified which of the two USB Peripherals to initialize. */
    enum UsbPeriph
    {
        FS_INTERNAL, /**< Internal pin */
        FS_EXTERNAL, /**< FS External D+ pin is Pin 38 (GPIO32). FS External D- pin is Pin 37 (GPIO31) */
        FS_BOTH,     /**< Both */
    };

    UsbMidiHandler();
    void Init(UsbPeriph dev);
    void StartReceive();
    void Listen();

    bool HasEvents() const { return event_q_.readable(); }
    MidiEvent PopEvent() { return event_q_.Read(); }

    RingBuffer<MidiEvent, 256> event_q_;
    MidiParser parser;
};

}
