
#pragma once
#include <stdint.h>

extern uint16_t USBD_VID;
extern uint16_t USBD_LANGID_STRING;
extern const char* USBD_MANUFACTURER_STRING;
extern uint16_t USBD_PID_HS;
extern const char* USBD_PRODUCT_STRING_HS;
extern const char* USBD_CONFIGURATION_STRING_HS;
extern const char* USBD_INTERFACE_STRING_HS;
extern uint16_t USBD_PID_FS;
extern const char* USBD_PRODUCT_STRING_FS;
extern const char* USBD_CONFIGURATION_STRING_FS;
extern const char* USBD_INTERFACE_STRING_FS;
