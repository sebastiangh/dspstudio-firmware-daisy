#pragma once

#include "daisy_seed.h"

namespace daisy {

struct MidiParser {
	enum {
		NOTE_OFF = 8,
		NOTE_ON = 9,
		AFTERTOUCH_POLY = 10,
		CC = 11,
		PROGRAM_CHANGE = 12,
		AFTERTOUCH_CHANNEL = 13,
		PITCH_BEND = 14,
		SYSTEM = 15
	};
	int pos;
    MidiEvent event;
	uint8_t channel;
	uint8_t type;
	MidiParser() : pos() { }
	MidiEvent* parse(uint8_t byte) {
		if (byte & 128) { // Bit 7 set - indicates this is a status byte (first byte of message)
			pos = 0;
			type = byte >> 4;
			channel = byte & 0xF;
			event.data[0] = 0;
			event.data[1] = 0;
		} else if (pos == 1) {
			event.data[0] = byte;
			switch (type) {
			case PROGRAM_CHANGE: event.type = MidiMessageType::ProgramChange; return &event;
			case AFTERTOUCH_CHANNEL: event.type = MidiMessageType::ChannelPressure; return &event;
			}

		} else if (pos == 2) {
			event.data[1] = byte;
			switch (type) {
			case NOTE_ON: event.type = MidiMessageType::NoteOn; return &event;
			case NOTE_OFF: event.type = MidiMessageType::NoteOff; return &event;
			case AFTERTOUCH_POLY: event.type = MidiMessageType::PolyphonicKeyPressure; return &event;
			case CC: event.type = MidiMessageType::ControlChange; return &event;
			case PITCH_BEND: event.type = MidiMessageType::PitchBend; return &event;
			}
		}
		pos++;
        return nullptr;
	}
};

}