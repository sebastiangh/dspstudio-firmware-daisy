/*
 * usbd_midi.c
 *
 *  Created on: 28.09.2017
 *      Author: ingo
 */

#include "usbd_midi.h"
#include "usbd_desc.h"
#include "usbd_ctlreq.h"

__ALIGN_BEGIN uint8_t USB_Receive_Buffer[MIDI_PACKET_SIZE_OUT1] __ALIGN_END;

static uint8_t USBD_MIDI_Init (USBD_HandleTypeDef *pdev, uint8_t cfgidx);
static uint8_t USBD_MIDI_DeInit (USBD_HandleTypeDef *pdev, uint8_t cfgidx);
static uint8_t USBD_MIDI_Setup (USBD_HandleTypeDef *pdev, USBD_SetupReqTypedef *req);
static uint8_t *USBD_MIDI_GetCfgDesc (uint16_t *length);
static uint8_t *USBD_MIDI_GetDeviceQualifierDesc (uint16_t *length);
static uint8_t USBD_MIDI_DataIn (USBD_HandleTypeDef *pdev, uint8_t epnum);
static uint8_t USBD_MIDI_DataOut (USBD_HandleTypeDef *pdev, uint8_t epnum);
static uint8_t USBD_MIDI_EP0_RxReady (USBD_HandleTypeDef *pdev);
static uint8_t USBD_MIDI_EP0_TxReady (USBD_HandleTypeDef *pdev);
static uint8_t USBD_MIDI_SOF (USBD_HandleTypeDef *pdev);
static uint8_t USBD_MIDI_IsoINIncomplete (USBD_HandleTypeDef *pdev, uint8_t epnum);
static uint8_t USBD_MIDI_IsoOutIncomplete (USBD_HandleTypeDef *pdev, uint8_t epnum);

USBD_ClassTypeDef  USBD_MIDI =
{
  USBD_MIDI_Init,
  USBD_MIDI_DeInit,
  USBD_MIDI_Setup,
  USBD_MIDI_EP0_TxReady,
  USBD_MIDI_EP0_RxReady,
  USBD_MIDI_DataIn,
  USBD_MIDI_DataOut,
  USBD_MIDI_SOF,
  USBD_MIDI_IsoINIncomplete,
  USBD_MIDI_IsoOutIncomplete,
  USBD_MIDI_GetCfgDesc,
  USBD_MIDI_GetCfgDesc,
  USBD_MIDI_GetCfgDesc,
  USBD_MIDI_GetDeviceQualifierDesc,
};


// USB AUDIO/MIDI Descriptor (see midi10.pdf for reference)

/* USB AUDIO device Configuration Descriptor */
__ALIGN_BEGIN static uint8_t USBD_MIDI_CfgDesc[USB_MIDI_CONFIG_DESC_SIZ] __ALIGN_END =
{
  /* Configuration Descriptor */
  0x09,                                 /* bLength */
  USB_DESC_TYPE_CONFIGURATION,          /* bDescriptorType */
  LOBYTE(USB_MIDI_CONFIG_DESC_SIZ),    /* wTotalLength  101 bytes*/
  HIBYTE(USB_MIDI_CONFIG_DESC_SIZ),
  0x02,                                 /* bNumInterfaces */
  0x01,                                 /* bConfigurationValue */
  0x00,                                 /* iConfiguration */
  0xC0,                                 /* bmAttributes  BUS Powred*/ //XXX Change to self-powered!!!
  0x32,                                 /* bMaxPower = 100 mA*/
  /* 09 byte*/

  /* USB Standard interface descriptor */
  AUDIO_INTERFACE_DESC_SIZE,            /* bLength */
  USB_DESC_TYPE_INTERFACE,              /* bDescriptorType */
  0x00,                                 /* bInterfaceNumber */
  0x00,                                 /* bAlternateSetting */
  0x00,                                 /* bNumEndpoints */
  USB_DEVICE_CLASS_AUDIO,               /* bInterfaceClass */
  AUDIO_SUBCLASS_AUDIOCONTROL,          /* bInterfaceSubClass */
  AUDIO_PROTOCOL_UNDEFINED,             /* bInterfaceProtocol */
  0x00,                                 /* iInterface */
  /* 09 byte*/

  /* USB Class-specific AC Interface Descriptor */
  AUDIO_INTERFACE_DESC_SIZE,            /* bLength */
  AUDIO_INTERFACE_DESCRIPTOR_TYPE,      /* bDescriptorType */
  AUDIO_CONTROL_HEADER,                 /* bDescriptorSubtype */
  0x00,          /* 1.00 */             /* bcdADC */
  0x01,
  0x09,                                 /* wTotalLength = 9*/
  0x00,
  0x01,                                 /* bInCollection */
  0x01,                                 /* baInterfaceNr */
  /* 09 byte*/

  /* MIDIStreaming Interface Descriptors */
  MIDI_STREAMING_DESC_SIZE,				/* bLength */
  MIDI_STREAMING_INTERFACE_DESCRIPTOR,	/* bDescriptorType */
  MIDI_STREAMING_INTERFACE_NUMBER,		/* bInteraceNumber */
  0x0,									/* bAlternateSetting */
  0x2,									/* bNumEndpoints */
  USB_DEVICE_CLASS_AUDIO,				/* bInterfaceClass */
  MIDI_AUDIO_SUBCLASS_MIDISTREAMING,	/* bInterfaceSubclass */
  0x0,									/* unused */
  0x0,									/* unused */
  /* 09 byte */

  /* USB Class-specific Midi-Streaming (MS) Interface Header Descriptor */
  MIDI_MS_INTERFACE_DESC_SIZE,			/* bLength */
  MIDI_DESC_TYPE_CS_INTERFACE,			/* bDescriptorType */
  MIDI_DESC_SUBTYPE_MS_HEADER,			/* bDescriptorSubtype */
  0x00,          /* 1.00 */             /* bcdADC */
  0x01,
  LOBYTE(MIDI_CLASS_SPECIFIC_DESC_SIZE),/* wTotalLength  41 bytes*/
  HIBYTE(MIDI_CLASS_SPECIFIC_DESC_SIZE),
  /* 07 byte */

  /* MIDI IN Jack Descriptor */
  MIDI_INPUT_JACK_DESC_SIZE,			/* bLength */
  MIDI_DESC_TYPE_CS_INTERFACE,			/* bDescriptorType */
  MIDI_DESC_SUBTYPE_INPUT_JACK,			/* bDescriptorSubtype */
  MIDI_JACK_TYPE_EMBEDDED,				/* bJackType */
  0x01,									/* bJackID */
  0x0,									/* iJack, unused */
  /* 06 byte */

  /* MIDI IN Jack Descriptor */
  MIDI_INPUT_JACK_DESC_SIZE,			/* bLength */
  MIDI_DESC_TYPE_CS_INTERFACE,			/* bDescriptorType */
  MIDI_DESC_SUBTYPE_INPUT_JACK,			/* bDescriptorSubtype */
  MIDI_JACK_TYPE_EXTERNAL,				/* bJackType */
  0x02,									/* bJackID */
  0x0,									/* iJack, unused */
  /* 06 byte */


  /* MIDI OUT Jack Descriptor */
  MIDI_OUTPUT_JACK_DESC_SIZE,			/* bLength */
  MIDI_DESC_TYPE_CS_INTERFACE,			/* bDescriptorType */
  MIDI_DESC_SUBTYPE_OUTPUT_JACK,		/* bDescriptorSubtype */
  MIDI_JACK_TYPE_EMBEDDED,				/* bJackType */
  0x03,									/* bJackID */
  0x01,									/* bNrInputPins */
  0x02,									/* BaSourceID, ID of the Entity to which this pin is connected (MIDI IN 2). */
  0x01,									/* BaSourcePin, Output Pin number of the Entity to which this Input Pin is connected. */
  0x0,									/* iJack, unused */
  /* 09 byte */


  /* MIDI OUT Jack Descriptor */
  MIDI_OUTPUT_JACK_DESC_SIZE,			/* bLength */
  MIDI_DESC_TYPE_CS_INTERFACE,			/* bDescriptorType */
  MIDI_DESC_SUBTYPE_OUTPUT_JACK,		/* bDescriptorSubtype */
  MIDI_JACK_TYPE_EXTERNAL,				/* bJackType */
  0x04,									/* bJackID */
  0x01,									/* bNrInputPins */
  0x01,									/* BaSourceID, ID of the Entity to which this pin is connected (MIDI IN 2). */
  0x01,									/* BaSourcePin, Output Pin number of the Entity to which this Input Pin is connected. */
  0x0,									/* iJack, unused */
  /* 09 byte */

  /* Standard Bulk OUT Endpoint Descriptor */
  MIDI_BULK_DESC_SIZE,					/* bLength */
  MIDI_DESC_TYPE_ENDPOINT,				/* bDescriptorType */
  MIDI_ENDPOINT_OUT1,					/* bEndpointAddress, OUT Endpoint 1 */
  MIDI_BULK_MODE_BULK_NOT_SHARED,		/* bmAttributes */
  LOBYTE(MIDI_MAX_PACKAGE_SIZE),		/* wMaxPackageSize = 64 bytes per package */
  HIBYTE(MIDI_MAX_PACKAGE_SIZE),
  0x0,									/* bInterval, ignored for bulk */
  0x0,									/* bRefresh */
  0x0,									/* bSynchAddress */
  /* 09 bytes */

  /* Class Specific Bulk OUT Endpoint Descriptor */
  MIDI_CS_BULK_DESC_SIZE, 				/* bLength */
  MIDI_DESC_TYPE_CS_ENDPOINT, 			/* bDescriptorType */
  MIDI_DESC_SUBTYPE_MS_GENERAL,			/* bDescriptorSubtype */
  0x1,									/* bNumEmbMIDIJack, Number of embedded MIDI IN Jacks */
  0x1,									/* BaAssocJackID, ID of the Embedded MIDI IN Jack */
  /* 05 bytes */


  /* Standard Bulk IN Endpoint Descriptor */
  MIDI_BULK_DESC_SIZE,					/* bLength */
  MIDI_DESC_TYPE_ENDPOINT,				/* bDescriptorType */
  MIDI_ENDPOINT_IN1,					/* bEndpointAddress, IN Endpoint 1 */
  MIDI_BULK_MODE_BULK_NOT_SHARED,		/* bmAttributes */
  LOBYTE(MIDI_MAX_PACKAGE_SIZE),		/* wMaxPackageSize = 64 bytes per package */
  HIBYTE(MIDI_MAX_PACKAGE_SIZE),
  0x0,									/* bInterval, ignored for bulk */
  0x0,									/* bRefresh */
  0x0,									/* bSynchAddress */
  /* 09 bytes */

  /* Class Specific Bulk IN Endpoint Descriptor */
  MIDI_CS_BULK_DESC_SIZE, 				/* bLength */
  MIDI_DESC_TYPE_CS_ENDPOINT, 			/* bDescriptorType */
  MIDI_DESC_SUBTYPE_MS_GENERAL,			/* bDescriptorSubtype */
  0x1,									/* bNumEmbMIDIJack, Number of embedded MIDI OUT Jacks */
  0x3,									/* BaAssocJackID, ID of the Embedded MIDI OUT Jack */
  /* 05 bytes */
} ;

/* USB Standard Device Descriptor */
__ALIGN_BEGIN static uint8_t USBD_MIDI_DeviceQualifierDesc[USB_LEN_DEV_QUALIFIER_DESC] __ALIGN_END=
{
  USB_LEN_DEV_QUALIFIER_DESC,
  USB_DESC_TYPE_DEVICE_QUALIFIER,
  0x00,
  0x02,
  0x00,
  0x00,
  0x00,
  0x40,
  0x01,
  0x00,
};

/**
  * @}
  */

/** @defgroup USBD_MIDI_Private_Functions
  * @{
  */

/**
  * @brief  USBD_MIDI_Init
  *         Initialize the AUDIO interface
  * @param  pdev: device instance
  * @param  cfgidx: Configuration index
  * @retval status
  */
static uint8_t  USBD_MIDI_Init (USBD_HandleTypeDef *pdev,
                               uint8_t cfgidx)
{
	/* Open MIDI_ENDPOINT_IN1 */
	USBD_LL_OpenEP(pdev, MIDI_ENDPOINT_IN1, USBD_EP_TYPE_BULK, MIDI_PACKET_SIZE_IN1);

	/* Open MIDI_ENDPOINT_OUT1 */
	USBD_LL_OpenEP(pdev, MIDI_ENDPOINT_OUT1, USBD_EP_TYPE_BULK, MIDI_PACKET_SIZE_OUT1);



	USBD_LL_PrepareReceive(pdev, MIDI_ENDPOINT_OUT1, (uint8_t*)USB_Receive_Buffer, MIDI_PACKET_SIZE_OUT1);

	return USBD_OK;
}

/**
  * @brief  USBD_MIDI_DeInit
  *         DeInitialize the AUDIO layer
  * @param  pdev: device instance
  * @param  cfgidx: Configuration index
  * @retval status
  */
static uint8_t  USBD_MIDI_DeInit (USBD_HandleTypeDef *pdev,
                                 uint8_t cfgidx)
{

  /* Close Endpoint IN1 */
  USBD_LL_CloseEP(pdev, MIDI_ENDPOINT_IN1);

  /* Close Endpoint OUT1 */
  USBD_LL_CloseEP(pdev, MIDI_ENDPOINT_OUT1);

  return USBD_OK;
}

/**
  * @brief  USBD_MIDI_Setup
  *         Handle the AUDIO specific requests
  * @param  pdev: instance
  * @param  req: usb requests
  * @retval status
  */
static uint8_t  USBD_MIDI_Setup (USBD_HandleTypeDef *pdev, USBD_SetupReqTypedef *req)
{
	// XXX implement as necessary
	return USBD_OK;
}


/**
  * @brief  USBD_MIDI_GetCfgDesc
  *         return configuration descriptor
  * @param  speed : current device speed
  * @param  length : pointer data length
  * @retval pointer to descriptor buffer
  */
static uint8_t  *USBD_MIDI_GetCfgDesc (uint16_t *length)
{
  *length = sizeof (USBD_MIDI_CfgDesc);
  return USBD_MIDI_CfgDesc;
}

/**
  * @brief  USBD_MIDI_DataIn
  *         handle data IN Stage
  * @param  pdev: device instance
  * @param  epnum: endpoint index
  * @retval status
  */
static uint8_t  USBD_MIDI_DataIn (USBD_HandleTypeDef *pdev,
                              uint8_t epnum)
{

  /* Only OUT data are processed */
  return USBD_OK;
}

/**
  * @brief  USBD_MIDI_EP0_RxReady
  *         handle EP0 Rx Ready event
  * @param  pdev: device instance
  * @retval status
  */
static uint8_t  USBD_MIDI_EP0_RxReady (USBD_HandleTypeDef *pdev)
{
  return USBD_OK;
}
/**
  * @brief  USBD_MIDI_EP0_TxReady
  *         handle EP0 TRx Ready event
  * @param  pdev: device instance
  * @retval status
  */
static uint8_t  USBD_MIDI_EP0_TxReady (USBD_HandleTypeDef *pdev)
{
  return USBD_OK;
}
/**
  * @brief  USBD_MIDI_SOF
  *         handle SOF event
  * @param  pdev: device instance
  * @retval status
  */
static uint8_t  USBD_MIDI_SOF (USBD_HandleTypeDef *pdev)
{
  return USBD_OK;
}


/**
  * @brief  USBD_MIDI_IsoINIncomplete
  *         handle data ISO IN Incomplete event
  * @param  pdev: device instance
  * @param  epnum: endpoint index
  * @retval status
  */
static uint8_t  USBD_MIDI_IsoINIncomplete (USBD_HandleTypeDef *pdev, uint8_t epnum)
{

  return USBD_OK;
}
/**
  * @brief  USBD_MIDI_IsoOutIncomplete
  *         handle data ISO OUT Incomplete event
  * @param  pdev: device instance
  * @param  epnum: endpoint index
  * @retval status
  */
static uint8_t  USBD_MIDI_IsoOutIncomplete (USBD_HandleTypeDef *pdev, uint8_t epnum)
{

  return USBD_OK;
}

typedef void (*ReceiveMidiCallback)(uint8_t* buff, uint32_t len);
ReceiveMidiCallback rx_midi_callback = 0;


/**
  * @brief  USBD_MIDI_DataOut
  *         handle data OUT Stage
  * @param  pdev: device instance
  * @param  epnum: endpoint index
  * @retval status
  */
static uint8_t  USBD_MIDI_DataOut (USBD_HandleTypeDef *pdev,
                              uint8_t epnum)
{
	uint32_t count = USBD_LL_GetRxDataSize (pdev, MIDI_ENDPOINT_OUT1);

	if (count > 1 && rx_midi_callback) {

    // first byte contains some meta information (we do not use it)
    // uint8_t firstByte = USB_Receive_Buffer[0];
		// uint8_t cable = firstByte >> 4;
		// uint8_t code_idx_nr = firstByte & 0xf;

    rx_midi_callback(&USB_Receive_Buffer[1], count - 1);
	}
	USBD_LL_PrepareReceive(pdev, MIDI_ENDPOINT_OUT1, (uint8_t*)USB_Receive_Buffer, MIDI_PACKET_SIZE_OUT1);

  return USBD_OK;
}

/**
* @brief  DeviceQualifierDescriptor
*         return Device Qualifier descriptor
* @param  length : pointer data length
* @retval pointer to descriptor buffer
*/
static uint8_t  *USBD_MIDI_GetDeviceQualifierDesc (uint16_t *length)
{
  *length = sizeof (USBD_MIDI_DeviceQualifierDesc);
  return USBD_MIDI_DeviceQualifierDesc;
}
//
///**
//* @brief  USBD_MIDI_RegisterInterface
//* @param  fops: Audio interface callback
//* @retval status
//*/
//uint8_t  USBD_MIDI_RegisterInterface  (USBD_HandleTypeDef   *pdev,
//                                        USBD_MIDI_ItfTypeDef *fops)
//{
//  if(fops != NULL)
//  {
//    pdev->pUserData= fops;
//  }
//  return 0;
//}
/**
  * @}
  */


/**
  * @}
  */


/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
