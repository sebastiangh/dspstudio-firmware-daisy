#include "UsbMidiHandler.h"

using namespace daisy;

#include "hid/usb.h"
#include "usbd_core.h"
#include "usbd_desc.h"
#include "usbd_midi.h"

using namespace daisy;

static void UsbErrorHandler();

// Externs for IRQ Handlers
extern "C"
{
    // Globals from Cube generated version:
    USBD_HandleTypeDef       hUsbDeviceHS;
    USBD_HandleTypeDef       hUsbDeviceFS;
    extern PCD_HandleTypeDef hpcd_USB_OTG_FS;
    extern PCD_HandleTypeDef hpcd_USB_OTG_HS;
}

// Static Function Implementation
static void UsbErrorHandler()
{
    while(1) {}
}

// IRQ Handler
extern "C"
{
    void OTG_HS_EP1_OUT_IRQHandler(void)
    {
        HAL_PCD_IRQHandler(&hpcd_USB_OTG_HS);
    }

    void OTG_HS_EP1_IN_IRQHandler(void)
    {
        HAL_PCD_IRQHandler(&hpcd_USB_OTG_HS);
    }

    void OTG_HS_IRQHandler(void) { HAL_PCD_IRQHandler(&hpcd_USB_OTG_HS); }

    void OTG_FS_EP1_OUT_IRQHandler(void)
    {
        HAL_PCD_IRQHandler(&hpcd_USB_OTG_FS);
    }

    void OTG_FS_EP1_IN_IRQHandler(void)
    {
        HAL_PCD_IRQHandler(&hpcd_USB_OTG_FS);
    }

    void OTG_FS_IRQHandler(void) { HAL_PCD_IRQHandler(&hpcd_USB_OTG_FS); }
}


extern USBD_DescriptorsTypeDef HS_Desc2;
extern USBD_DescriptorsTypeDef FS_Desc2;

static void InitFS()
{
    if(USBD_Init(&hUsbDeviceFS, &FS_Desc2, DEVICE_FS) != USBD_OK)
    {
        UsbErrorHandler();
    }
    if(USBD_RegisterClass(&hUsbDeviceFS, USBD_MIDI_CLASS) != USBD_OK)
    {
        UsbErrorHandler();
    }
    if(USBD_Start(&hUsbDeviceFS) != USBD_OK)
    {
        UsbErrorHandler();
    }
}

static void InitHS()
{
    // HS as FS
    if(USBD_Init(&hUsbDeviceHS, &HS_Desc2, DEVICE_HS) != USBD_OK)
    {
        UsbErrorHandler();
    }
    if(USBD_RegisterClass(&hUsbDeviceHS, USBD_MIDI_CLASS) != USBD_OK)
    {
        UsbErrorHandler();
    }
    if(USBD_Start(&hUsbDeviceHS) != USBD_OK)
    {
        UsbErrorHandler();
    }
}

typedef void (*ReceiveMidiCallback)(uint8_t* buffer, uint32_t len);
extern ReceiveMidiCallback rx_midi_callback;

static UsbMidiHandler* g_instance;

UsbMidiHandler::UsbMidiHandler() {
    g_instance = this;
}

static void RxMidiCallback(uint8_t* buffer, uint32_t len) {
    while (len--) {
        MidiEvent* event = g_instance->parser.parse(*buffer++);
        if (event) {
            g_instance->event_q_.Write(*event);
        }
    }
}

void UsbMidiHandler::Init(UsbPeriph dev)
{
    switch(dev)
    {
        case FS_INTERNAL: InitFS(); break;
        case FS_EXTERNAL: InitHS(); break;
        case FS_BOTH:
            InitHS();
            InitFS();
            break;
        default: break;
    }
    // Enable USB Regulator
    HAL_PWREx_EnableUSBVoltageDetector();
}

void UsbMidiHandler::StartReceive() {
    rx_midi_callback = RxMidiCallback;
}
void UsbMidiHandler::Listen() {
}