/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : usbd_desc.c
  * @version        : v1.0_Cube
  * @brief          : This file implements the USB device descriptors.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "usbd_core.h"
#include "usbd_desc.h"
#include "usbd_conf.h"

// BOTH
uint16_t USBD_VID = 1155;
uint16_t USBD_LANGID_STRING = 1033;
const char* USBD_MANUFACTURER_STRING = "ACL";

// HS
uint16_t USBD_PID_HS = 22336;
const char* USBD_PRODUCT_STRING_HS = "Product Name";
const char* USBD_CONFIGURATION_STRING_HS = "MIDI Config";
const char* USBD_INTERFACE_STRING_HS = "MIDI Interface";

// FS
uint16_t USBD_PID_FS = 22336;
const char* USBD_PRODUCT_STRING_FS = "Product Name";
const char* USBD_CONFIGURATION_STRING_FS = "MIDI Config";
const char* USBD_INTERFACE_STRING_FS = "MIDI Interface";



#if defined(__ICCARM__) /* IAR Compiler */
#pragma data_alignment = 4
#endif
static __ALIGN_BEGIN uint8_t USBD_FS_DeviceDesc_2[USB_LEN_DEV_DESC] __ALIGN_END = {
    0x12,                 /*bLength */
    USB_DESC_TYPE_DEVICE, /*bDescriptorType*/
    0x00,                 /*bcdUSB */
    0x02,
    0x02,                /*bDeviceClass*/
    0x02,                /*bDeviceSubClass*/
    0x00,                /*bDeviceProtocol*/
    USB_MAX_EP0_SIZE,    /*bMaxPacketSize*/
    0, //LOBYTE(USBD_VID),    /*idVendor*/
    0, //HIBYTE(USBD_VID),    /*idVendor*/
    0, //LOBYTE(USBD_PID_FS), /*idProduct*/
    0, //HIBYTE(USBD_PID_FS), /*idProduct*/
    0x00,                /*bcdDevice rel. 2.00*/
    0x02,
    USBD_IDX_MFC_STR,          /*Index of manufacturer  string*/
    USBD_IDX_PRODUCT_STR,      /*Index of product string*/
    USBD_IDX_SERIAL_STR,       /*Index of serial number string*/
    USBD_MAX_NUM_CONFIGURATION /*bNumConfigurations*/
};



#if defined(__ICCARM__) /* IAR Compiler */
#pragma data_alignment = 4
#endif
static __ALIGN_BEGIN uint8_t USBD_HS_DeviceDesc_2[USB_LEN_DEV_DESC] __ALIGN_END = {
    0x12,                 /*bLength */
    USB_DESC_TYPE_DEVICE, /*bDescriptorType*/
    0x00,                 /*bcdUSB */
    0x02,
    0x02,                /*bDeviceClass*/
    0x02,                /*bDeviceSubClass*/
    0x00,                /*bDeviceProtocol*/
    USB_MAX_EP0_SIZE,    /*bMaxPacketSize*/
    0,//LOBYTE(USBD_VID),    /*idVendor*/
    0,//HIBYTE(USBD_VID),    /*idVendor*/
    0,//LOBYTE(USBD_PID_HS), /*idProduct*/
    0,//HIBYTE(USBD_PID_HS), /*idProduct*/
    0x00,                /*bcdDevice rel. 2.00*/
    0x02,
    USBD_IDX_MFC_STR,          /*Index of manufacturer  string*/
    USBD_IDX_PRODUCT_STR,      /*Index of product string*/
    USBD_IDX_SERIAL_STR,       /*Index of serial number string*/
    USBD_MAX_NUM_CONFIGURATION /*bNumConfigurations*/
};

#if defined(__ICCARM__) /* IAR Compiler */
#pragma data_alignment = 4
#endif
static __ALIGN_BEGIN uint8_t USBD_LangIDDesc_2[USB_LEN_LANGID_STR_DESC] __ALIGN_END = {
    USB_LEN_LANGID_STR_DESC,
    USB_DESC_TYPE_STRING,
    0, //LOBYTE(USBD_LANGID_STRING),
    0, //HIBYTE(USBD_LANGID_STRING)
};

#if defined(__ICCARM__) /* IAR Compiler */
#pragma data_alignment = 4
#endif
static __ALIGN_BEGIN uint8_t USBD_StrDesc_2[USBD_MAX_STR_DESC_SIZ] __ALIGN_END;

#if defined(__ICCARM__) /*!< IAR Compiler */
#pragma data_alignment = 4
#endif
static __ALIGN_BEGIN uint8_t USBD_StringSerial_2[USB_SIZ_STRING_SERIAL] __ALIGN_END = {
    USB_SIZ_STRING_SERIAL,
    USB_DESC_TYPE_STRING,
};

static void IntToUnicode_2(uint32_t value, uint8_t *pbuf, uint8_t len) {
    for(uint8_t idx = 0; idx < len; idx++) {
        if(((value >> 28)) < 0xA) pbuf[2 * idx] = (value >> 28) + '0';
        else pbuf[2 * idx] = (value >> 28) + 'A' - 10;
        value = value << 4;
        pbuf[2 * idx + 1] = 0;
    }
}

static void Get_SerialNum_2(void)
{
    uint32_t deviceserial0 = *(uint32_t *)DEVICE_ID1;
    uint32_t deviceserial1 = *(uint32_t *)DEVICE_ID2;
    uint32_t deviceserial2 = *(uint32_t *)DEVICE_ID3;
    deviceserial0 += deviceserial2;
    if(deviceserial0 != 0)    {
        IntToUnicode_2(deviceserial0, &USBD_StringSerial_2[2], 8);
        IntToUnicode_2(deviceserial1, &USBD_StringSerial_2[18], 4);
    }
}

static uint8_t* USBD_HS_DeviceDescriptor_2(USBD_SpeedTypeDef speed, uint16_t *length) {
    UNUSED(speed);
    *length = sizeof(USBD_HS_DeviceDesc_2);
    USBD_HS_DeviceDesc_2[8] = LOBYTE(USBD_VID);
    USBD_HS_DeviceDesc_2[9] = HIBYTE(USBD_VID);
    USBD_HS_DeviceDesc_2[10] = LOBYTE(USBD_PID_HS);
    USBD_HS_DeviceDesc_2[11] = HIBYTE(USBD_PID_HS);
    return USBD_HS_DeviceDesc_2;
}

static uint8_t* USBD_LangIDStrDescriptor_2(USBD_SpeedTypeDef speed, uint16_t *length) {
    UNUSED(speed);
    *length = sizeof(USBD_LangIDDesc_2);
    USBD_LangIDDesc_2[2] = LOBYTE(USBD_LANGID_STRING);
    USBD_LangIDDesc_2[3] = HIBYTE(USBD_LANGID_STRING);
    return USBD_LangIDDesc_2;
}

static uint8_t* USBD_HS_ProductStrDescriptor_2(USBD_SpeedTypeDef speed, uint16_t *length) {
    USBD_GetString((uint8_t *)USBD_PRODUCT_STRING_HS, USBD_StrDesc_2, length);
    return USBD_StrDesc_2;
}

static uint8_t* USBD_HS_ManufacturerStrDescriptor_2(USBD_SpeedTypeDef speed, uint16_t* length) {
    UNUSED(speed);
    USBD_GetString((uint8_t *)USBD_MANUFACTURER_STRING, USBD_StrDesc_2, length);
    return USBD_StrDesc_2;
}

static uint8_t* USBD_HS_SerialStrDescriptor_2(USBD_SpeedTypeDef speed, uint16_t *length) {
    UNUSED(speed);
    *length = USB_SIZ_STRING_SERIAL;
    Get_SerialNum_2();
    return (uint8_t *)USBD_StringSerial_2;
}

static uint8_t* USBD_HS_ConfigStrDescriptor_2(USBD_SpeedTypeDef speed, uint16_t *length) {
    USBD_GetString((uint8_t *)USBD_CONFIGURATION_STRING_HS, USBD_StrDesc_2, length);
    return USBD_StrDesc_2;
}

static uint8_t* USBD_HS_InterfaceStrDescriptor_2(USBD_SpeedTypeDef speed, uint16_t* length) {
    USBD_GetString((uint8_t *)USBD_INTERFACE_STRING_HS, USBD_StrDesc_2, length);
    return USBD_StrDesc_2;
}

static uint8_t* USBD_FS_DeviceDescriptor_2(USBD_SpeedTypeDef speed, uint16_t* length) {
    UNUSED(speed);
    *length = sizeof(USBD_FS_DeviceDesc_2);
    USBD_FS_DeviceDesc_2[8] = LOBYTE(USBD_VID);
    USBD_FS_DeviceDesc_2[9] = HIBYTE(USBD_VID);
    USBD_FS_DeviceDesc_2[10] = LOBYTE(USBD_PID_FS);
    USBD_FS_DeviceDesc_2[11] = HIBYTE(USBD_PID_FS);
    return USBD_FS_DeviceDesc_2;
}


static uint8_t* USBD_FS_ProductStrDescriptor_2(USBD_SpeedTypeDef speed, uint16_t *length) {
    USBD_GetString((uint8_t *)USBD_PRODUCT_STRING_FS, USBD_StrDesc_2, length);
    return USBD_StrDesc_2;
}

static uint8_t* USBD_FS_ManufacturerStrDescriptor_2(USBD_SpeedTypeDef speed, uint16_t* length) {
    UNUSED(speed);
    USBD_GetString((uint8_t *)USBD_MANUFACTURER_STRING, USBD_StrDesc_2, length);
    return USBD_StrDesc_2;
}

static uint8_t* USBD_FS_SerialStrDescriptor_2(USBD_SpeedTypeDef speed, uint16_t *length) {
    UNUSED(speed);
    *length = USB_SIZ_STRING_SERIAL;
    Get_SerialNum_2();
    return (uint8_t *)USBD_StringSerial_2;
}

static uint8_t* USBD_FS_ConfigStrDescriptor_2(USBD_SpeedTypeDef speed, uint16_t *length) {
    USBD_GetString((uint8_t *)USBD_CONFIGURATION_STRING_FS, USBD_StrDesc_2, length);
    return USBD_StrDesc_2;
}

static uint8_t* USBD_FS_InterfaceStrDescriptor_2(USBD_SpeedTypeDef speed, uint16_t* length) {
    USBD_GetString((uint8_t *)USBD_INTERFACE_STRING_FS, USBD_StrDesc_2, length);
    return USBD_StrDesc_2;
}

USBD_DescriptorsTypeDef FS_Desc2 = {
    USBD_FS_DeviceDescriptor_2,
    USBD_LangIDStrDescriptor_2,
    USBD_FS_ManufacturerStrDescriptor_2,
    USBD_FS_ProductStrDescriptor_2,
    USBD_FS_SerialStrDescriptor_2,
    USBD_FS_ConfigStrDescriptor_2,
    USBD_FS_InterfaceStrDescriptor_2
};

USBD_DescriptorsTypeDef HS_Desc2 = {
    USBD_HS_DeviceDescriptor_2,
    USBD_LangIDStrDescriptor_2,
    USBD_HS_ManufacturerStrDescriptor_2,
    USBD_HS_ProductStrDescriptor_2,
    USBD_HS_SerialStrDescriptor_2,
    USBD_HS_ConfigStrDescriptor_2,
    USBD_HS_InterfaceStrDescriptor_2
};