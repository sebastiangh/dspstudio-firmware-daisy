#ifndef __USBD_MIDI_H
#define __USBD_MIDI_H

#ifdef __cplusplus
 extern "C" {
#endif

#include  "usbd_ioreq.h"

// Magic numbers for USB AUDIO/MIDI Descriptor (see midi10.pdf for reference)

// AUDIO
#define AUDIO_INTERFACE_DESC_SIZE                   9
#define USB_DEVICE_CLASS_AUDIO						0x01
#define AUDIO_SUBCLASS_AUDIOCONTROL                 0x01
#define AUDIO_PROTOCOL_UNDEFINED                    0x00
#define AUDIO_INTERFACE_DESC_SIZE                   9
#define AUDIO_INTERFACE_DESCRIPTOR_TYPE             0x24
#define AUDIO_CONTROL_HEADER                        0x01
#define AUDIO_ENDPOINT_DESCRIPTOR_TYPE              0x25


// MIDI
#define USB_MIDI_CONFIG_DESC_SIZ 					101
#define MIDI_CLASS_SPECIFIC_DESC_SIZE				41
#define MIDI_STREAMING_DESC_SIZE 					9
#define MIDI_INPUT_JACK_DESC_SIZE					6
#define MIDI_OUTPUT_JACK_DESC_SIZE					9
#define MIDI_MS_INTERFACE_DESC_SIZE				7
#define MIDI_BULK_DESC_SIZE						9
#define MIDI_CS_BULK_DESC_SIZE						5
#define MIDI_ENDPOINT_OUT1							0x1
#define MIDI_ENDPOINT_IN1							0x81
#define MIDI_PACKET_SIZE_IN1						0x40
#define MIDI_PACKET_SIZE_OUT1						0x40

#define MIDI_DESC_TYPE_CS_INTERFACE				0x24
#define MIDI_DESC_TYPE_CS_ENDPOINT					0x25
#define MIDI_DESC_TYPE_ENDPOINT					0x05
#define MIDI_DESC_SUBTYPE_MS_HEADER				0x01
#define MIDI_DESC_SUBTYPE_INPUT_JACK				0x02
#define MIDI_DESC_SUBTYPE_OUTPUT_JACK				0x03
#define MIDI_DESC_SUBTYPE_MS_GENERAL				0x01
#define MIDI_JACK_TYPE_EMBEDDED					0x01
#define MIDI_JACK_TYPE_EXTERNAL					0x02
#define MIDI_BULK_MODE_BULK_NOT_SHARED				0x02
#define MIDI_MAX_PACKAGE_SIZE 						0x40


#define MIDI_STREAMING_INTERFACE_DESCRIPTOR 		0x4
#define MIDI_STREAMING_INTERFACE_NUMBER				0x1

#define MIDI_AUDIO_SUBCLASS_MIDISTREAMING			0x3

extern USBD_ClassTypeDef  USBD_MIDI;
#define USBD_MIDI_CLASS    &USBD_MIDI

#ifdef __cplusplus
}
#endif

#endif
