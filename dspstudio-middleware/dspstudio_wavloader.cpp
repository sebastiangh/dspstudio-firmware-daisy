#include <algorithm>
#include "dspstudio_wavloader.h"

using namespace wavloader;
namespace {

/** Constants for In-Header IDs */
const uint32_t kWavFileChunkId     = 0x46464952; /**< "RIFF" */
const uint32_t kWavFileWaveId      = 0x45564157; /**< "WAVE" */
const uint32_t kWavFileSubChunk1Id = 0x20746d66; /**< "fmt " */
const uint32_t kWavFileSubChunk2Id = 0x61746164; /**< "data" */

/** Standard Format codes for the waveform data.
 ** 
 ** According to spec, extensible should be used whenever:
 ** * PCM data has more than 16 bits/sample
 ** * The number of channels is more than 2
 ** * The actual number of bits/sample is not equal to the container size
 ** * The mapping from channels to speakers needs to be specified.
 ** */
enum WavFileFormatCode
{
    WAVE_FORMAT_PCM        = 0x0001,
    WAVE_FORMAT_IEEE_FLOAT = 0x0003,
    WAVE_FORMAT_ALAW       = 0x0006,
    WAVE_FORMAT_ULAW       = 0x0007,
    WAVE_FORMAT_EXTENSIBLE = 0xFFFE,
};



bool checkRiffHeader(FIL* file) {
    unsigned int br;
    RIFF_Header header;
    if (f_read(file, &header, sizeof(header), &br) != FR_OK) return false;
    if (br != sizeof(header)) return false;
    if (header.ChunkId != kWavFileChunkId) return false;
    if (header.FileFormat != kWavFileWaveId) return false;
    return true;
}


bool readChunk(FIL* file, RIFF_Chunk& chunk) {
    unsigned int br;
    return f_read(file, &chunk, sizeof(RIFF_Chunk), &br) == FR_OK && br == sizeof(RIFF_Chunk);
}

bool skipBytes(FIL* file, uint16_t bytes) {
    return f_lseek(file, f_tell(file) + bytes) == FR_OK;
}
bool findChunk(FIL* file, RIFF_Chunk& chunk, uint32_t ChunkID) {
    while (readChunk(file, chunk)) {
        if (chunk.ChunkID == ChunkID) {
            return true;
        }
        int paddingByte = chunk.ChunkSize % 2;
        skipBytes(file, chunk.ChunkSize + paddingByte);
    }
    return false;
}

bool skipExtension(FIL* file, uint16_t& extensionSize) {
    unsigned int br;
    if(f_read(file, &extensionSize, 2, &br) != FR_OK) return false;
    if (br != 2) return false;
    if (extensionSize > 0) {
        if (!skipBytes(file, extensionSize)) return false;
    }
    extensionSize += 2;
    return true;
}

bool readExtension(FIL* file, uint16_t audioFormat, uint16_t& extensionSize) {
    if (audioFormat == WAVE_FORMAT_PCM) {
        // pcm has no extension.
        extensionSize = 0;
        return true;
    } else if (audioFormat == WAVE_FORMAT_IEEE_FLOAT) {
        if (!skipExtension(file, extensionSize)) return false;
        if (extensionSize != 2) return false; 
        return true;
    } else {
        return skipExtension(file, extensionSize);
    }
}

bool readAudioFormat(FIL* file, WAV_AudioFormat& format) {
    unsigned int br;
    RIFF_Chunk chunk;
    if (!findChunk(file, chunk, kWavFileSubChunk1Id)) return false;
    if (f_read(file, &format, sizeof(WAV_AudioFormat), &br) != FR_OK) return false;
    if (br != sizeof(WAV_AudioFormat)) return false;
    uint16_t extensionSize = 0;
    if (!readExtension(file, format.AudioFormat, extensionSize)) return false;
    if (chunk.ChunkSize != sizeof(WAV_AudioFormat) + extensionSize) return false;
    return true;
}

ErrorCode readMetaData(FIL* file, WAV_MetaData& metaData) {
    if (!checkRiffHeader(file)) return BrokenFile;
    if (!readAudioFormat(file, metaData.format)) return BrokenFile;    
    uint16_t audioFormat = metaData.format.AudioFormat;
    if (audioFormat != WAVE_FORMAT_PCM && audioFormat != WAVE_FORMAT_IEEE_FLOAT) {
        // unsupported audio format
        return UnsupportedFormat;
    }
    return Success;
}

}

WavLoader::WavLoader() : file(0), metaData(), errorCode(Success) { 
}

bool WavLoader::load(FIL* file) {
    this->file = file;
    if (file == 0) {
        errorCode = FileNotFound;
        return false;
    }

    errorCode = readMetaData(file, metaData);

    if (!findChunk(file, dataChunk, kWavFileSubChunk2Id)){
        errorCode = BrokenFile;
        return false;
    }

    // TODO: This is only allowed for PCM and IEEE? Other formats use a fact-chunk.
    metaData.frames = dataChunk.ChunkSize / metaData.format.BytesPerSampleFrame;

    dataSeek = f_tell(file);

    return errorCode == Success;
}

size_t WavLoader::getChannelData(uint16_t channel, float* buffer, size_t bufferSize) {
    if (errorCode != Success) {
        return false;
    }

    int channels = metaData.format.NbrChannels;
    if (channel >= channels) {
        errorCode = InvalidChannel;
        return 0;
    }

    f_lseek(file, dataSeek);

    uint16_t bytesPerSample = getBytesPerSample();
    uint16_t bytesPerFrame = bytesPerSample * channels;
    size_t workspaceFrames = kWorkspaceBytes / bytesPerFrame;

    
    size_t framesToRead = std::min((size_t)dataChunk.ChunkSize / bytesPerSample / channels, bufferSize);
    size_t frameCount = 0;
    size_t framesLeft = framesToRead;

    // buffered mode
    uint8_t workspace[kWorkspaceBytes];
    size_t bufferPos = 0;
    while (framesLeft > 0) {
        size_t bytesRequested = std::min(framesLeft, workspaceFrames) * bytesPerFrame;
        unsigned int bytesRead = 0;
        f_read(file, workspace, bytesRequested, &bytesRead);
        size_t framesRead = bytesRead / bytesPerFrame;
        
        if (metaData.format.AudioFormat == WAVE_FORMAT_PCM) {
            if (metaData.format.BitPerSample == 16) {
                int16_t* workspaceInt16 = reinterpret_cast<int16_t*>(workspace);
                for (size_t s = channel; s < framesRead * channels; s += channels) {
                    buffer[bufferPos++] = s162f(workspaceInt16[s]);
                }
            } else if (metaData.format.BitPerSample == 24) {
                // TODO
                errorCode = UnsupportedFormat;
                return 0;
            } else {
                errorCode = UnsupportedFormat;
                return 0;
            }
        } else if (metaData.format.AudioFormat == WAVE_FORMAT_IEEE_FLOAT) {
            if (metaData.format.BitPerSample == 32) {
                float* workspaceFloat = reinterpret_cast<float*>(workspace);
                for (size_t s = channel; s < framesRead * channels; s += channels) {
                    buffer[bufferPos++] = workspaceFloat[s];
                }
            } else {
                errorCode = UnsupportedFormat;
                return 0;
            }
        }

        if (bytesRead != bytesRequested) {
            errorCode = BrokenFile;
            return frameCount;
        }

        frameCount += framesRead;
        framesLeft -= framesRead;
    }

    errorCode = Success;
    return frameCount;
}
uint16_t WavLoader::getBytesPerSample() {
    return metaData.format.BitPerSample / 8;
}
const WAV_MetaData& WavLoader::getMetaData() { return metaData; }
ErrorCode WavLoader::getErrorCode() { return errorCode; }
