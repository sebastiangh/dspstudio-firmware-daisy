#include "daisysp.h"
#include "dspstudio_util.h"

using namespace daisysp;
using namespace daisy;
using namespace wavloader;

Sdcard sdcard;
ErrorCode lastLoadTableError = ErrorCode::Success;
size_t lastLoadTableFrames = 0;

namespace dspstudio_device {
extern void* dspstudio_malloc(size_t size);
void dspstudio_load_table(const char* filename, float** data, size_t* size) {


    // Init SD Card
    sdcard.InitOnce();

    // \todo:
    // if(f_open(&SDFile, filename, FA_READ) == FR_OK)
    // {
    //     WavLoader loader;
    //     loader.load(&SDFile);
    //     ErrorCode errorCode = loader.getErrorCode();
    //     size_t framesTotal = loader.getMetaData().frames;
    //     float* buffer = (float*) dspstudio_device::dspstudio_malloc(framesTotal * sizeof(float));
    //     size_t framesLoaded = loader.getChannelData(0, buffer, framesTotal);
    //     f_close(&SDFile);

    //     for (size_t s = framesLoaded; s < framesTotal; s++) {
    //         buffer[s] = 0.0f;
    //     }

    //     *data = buffer;
    //     *size = framesTotal;

    //     lastLoadTableError = errorCode;
    //     lastLoadTableFrames = framesLoaded;
    // } else {
    //     lastLoadTableError = ErrorCode::FileNotFound;
    //     lastLoadTableFrames = 0;
    // }

}
}


void debug_break() {
    asm("bkpt 255");
}

uint32_t avgTicks = 0;
uint32_t avgDivisor = 0;
uint32_t wcetTicks = 0;
uint32_t ticksStart;

void cpuMeasurementBegin() {
    ticksStart = System::GetUs();

}
void cpuMeasurementEnd() {
    uint32_t ticksEnd = System::GetUs();
    uint32_t ticksDiff = ticksEnd - ticksStart;
    avgDivisor++;
    avgTicks += ticksDiff;
    wcetTicks = std::max(wcetTicks, ticksDiff);
}

void cpuMeasurementEval(float maxBlockTime, float& wcet, float& avg) {
    wcet = (wcetTicks / maxBlockTime * 100.0f);
    avg = avgDivisor == 0 ? wcet : (avgTicks / maxBlockTime / avgDivisor * 100.0f);
    avgTicks = 0;
    wcetTicks = 0;
    avgDivisor = 0;
}