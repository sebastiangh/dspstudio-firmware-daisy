#pragma once

#include "daisy_seed.h"
#include "dspstudio_wavloader.h"

void cpuMeasurementBegin();
void cpuMeasurementEnd();
void cpuMeasurementEval(float maxBlockTime, float& wcet, float& avg);

class Sdcard {
    bool init;
    daisy::SdmmcHandler sd;
    daisy::FatFSInterface fatFs;
public:
    void InitOnce()
    {
        if (init) return;
        init = true;
        
        // Init SD Card
        daisy::SdmmcHandler::Config sd_cfg;
        sd_cfg.Defaults();

        // 09.11.2021 sgh
        sd_cfg.speed = daisy::SdmmcHandler::Speed::STANDARD;

        sd.Init(sd_cfg);

        // Links libdaisy i/o to fatfs driver.
        daisy::FatFSInterface::Config fatFsConfig;
        fatFsConfig.media = daisy::FatFSInterface::Config::MEDIA_SD;
        fatFs.Init(fatFsConfig);

        // \todo: Mount SD Card
        //f_mount(fatFs.GetSDFileSystem(), SDPath, 1);
    }
};


// struct File {
//     std::string path;
//     File(std::string path) : path(path) {
//     } 

//     std::vector<File> list() {
//         std::vector<File> files;

//         FRESULT result = FR_OK;
//         FILINFO fno;
//         DIR     dir;
 
//         // Open Dir and scan for files.
//         if (f_opendir(&dir, SDPath) == FR_OK) {
//             do
//             {
//                 result = f_readdir(&dir, &fno);
//                 // Exit if bad read or NULL fname
//                 if(result != FR_OK || fno.fname[0] == 0)
//                     break;
//                 // Skip if its a directory or a hidden file.
//                 if(fno.fattrib & (AM_HID | AM_DIR))
//                     continue;
//                 // Now we'll check if its .wav and add to the list.
//                 fn = fno.fname;
//                 if(file_cnt_ < kMaxFiles - 1)
//                 {
//                     if(strstr(fn, ".wav") || strstr(fn, ".WAV"))
//                     {
//                         strcpy(file_info_[file_cnt_].name, fn);
//                         file_cnt_++;
//                         // For now lets break anyway to test.
//                         //                break;
//                     }
//                 }
//                 else
//                 {
//                     break;
//                 }
//             } while(result == FR_OK);
//             f_closedir(&dir);
//         }

//         return files;
//     }
// }
