#include "daisy_seed.h"

namespace dspstudio_device {
#define DSPSTUDIO_POOL_SIZE (48*1024*1024) // 48MB
DSY_SDRAM_BSS char dspstudio_pool[DSPSTUDIO_POOL_SIZE];
size_t dspstudio_pool_index = 0;
void* dspstudio_malloc(size_t size)
{
    size_t next_index = dspstudio_pool_index + size;
    if (next_index < DSPSTUDIO_POOL_SIZE)
    {       
        void* ptr = &dspstudio_pool[dspstudio_pool_index];
        dspstudio_pool_index = next_index;
        return ptr;
    }
    return 0;
}
}