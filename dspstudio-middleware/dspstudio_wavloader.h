#pragma once

#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include "fatfs.h"
#include "daisy_core.h"

namespace wavloader {

typedef struct
{
    uint32_t ChunkId;
    uint32_t FileSize;
    uint32_t FileFormat;
} RIFF_Header;

typedef struct {
    uint32_t ChunkID;
    uint32_t ChunkSize;
} RIFF_Chunk;

typedef struct {
    uint16_t AudioFormat;
    uint16_t NbrChannels;
    uint32_t SampleRate;
    uint32_t ByteRate;
    uint16_t BytesPerSampleFrame; //aka block align
    uint16_t BitPerSample;
} WAV_AudioFormat;

typedef struct {
    WAV_AudioFormat format;
    size_t frames;
} WAV_MetaData;


enum ErrorCode {
    Success,
    FileNotFound,
    BrokenFile,
    UnsupportedFormat,
    InvalidChannel,
};

class WavLoader {
    FIL* file;
    WAV_MetaData metaData;
    ErrorCode errorCode;
    RIFF_Chunk dataChunk;
    unsigned long dataSeek;
    static constexpr size_t kWorkspaceBytes = 143;

public:
    WavLoader();
    bool load(FIL* file);
    size_t getChannelData(uint16_t channel, float* buffer, size_t bufferSize);
    uint16_t getBytesPerSample();
    const WAV_MetaData& getMetaData();
    ErrorCode getErrorCode();
};


}