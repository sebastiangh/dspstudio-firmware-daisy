#pragma once
#include "daisy_seed.h"
#include "usbd/usbd_desc2.h"

extern dspstudio_device::mod_device* g_dev;

static constexpr bool cpuBoost = (
    #ifdef _DSPSTUDIO_MAPPING_CONTROL_CPU_BOOST_MODE
    true
    #else
    false
    #endif
);

void InitializeAudioRates(daisy::DaisySeed& hw) {
#ifdef SR
    dspstudio_device::set_sample_rate(g_dev, SR);
    #if SR == 8000
        hw.SetAudioSampleRate(daisy::SaiHandle::Config::SampleRate::SAI_8KHZ);
    #elif SR == 16000
        hw.SetAudioSampleRate(daisy::SaiHandle::Config::SampleRate::SAI_16KHZ);
    #elif SR == 32000
        hw.SetAudioSampleRate(daisy::SaiHandle::Config::SampleRate::SAI_32KHZ);
    #elif SR == 48000
        hw.SetAudioSampleRate(daisy::SaiHandle::Config::SampleRate::SAI_48KHZ);
    #elif SR == 96000
        hw.SetAudioSampleRate(daisy::SaiHandle::Config::SampleRate::SAI_96KHZ);
    #else
        #error "unsupported samplerate, please choose 8000, 16000, 32000, 48000 or 96000."
    #endif
#else
    dspstudio_device::set_sample_rate(g_dev, hw.AudioSampleRate());
#endif
#ifdef BS
    dspstudio_device::set_buffer_size(g_dev, BS);
    hw.SetAudioBlockSize(BS);
#else
    dspstudio_device::set_buffer_size(g_dev, hw.AudioBlockSize());
#endif
}


static inline bool to_bool(bool value) { return value; }
static inline bool to_bool(uint8_t value) { return value; }
static inline bool to_bool(float value) { return value != 0.0f; }
static inline float to_float(bool value) { return value ? 1.0f : 0.0f; }
static inline float to_float(uint8_t value) { return value ? 1.0f : 0.0f; }
static inline float to_float(float value) { return value; }

static inline daisy::Color rgb(float r, float g, float b) {
    daisy::Color color;
    color.Init(r, g, b);
    return color;
}

namespace SLIDER {
    template <typename Value> static inline void set_value(dspstudio_device::SLIDER slider, Value value, bool event) {
        if (event) {
            dspstudio_device::set_slider_value(g_dev, slider, to_float(value));
        }
    }
    static inline float get_value(dspstudio_device::SLIDER slider) {
        return dspstudio_device::get_slider_value(g_dev, slider);
    }

    static inline daisy::Color get_color(dspstudio_device::SLIDER slider) {
        float value = get_value(slider);
        return rgb(value, value, value);
    }
}

namespace BUTTON {
    template <typename Value> static inline void set_value(dspstudio_device::BUTTON button, Value value, bool event) {
        if (event) {
            dspstudio_device::set_button_value(g_dev, button, to_bool(value));
        }
    }
    static inline uint8_t get_value(dspstudio_device::BUTTON button) {
        return dspstudio_device::get_button_value(g_dev, button);
    }
    static inline daisy::Color get_color(dspstudio_device::BUTTON button) {
        float value = get_value(button);
        return rgb(value, value, value);
    }
}

namespace LABEL {
    static inline float get_value(dspstudio_device::LABEL label) {
        return dspstudio_device::get_label_value(g_dev, label);
    }
    static inline daisy::Color get_color(dspstudio_device::LABEL label) {
        float value = get_value(label);
        return rgb(value, value, value);
    }
}



namespace DISPLAY {
    template <typename Value> static inline void set_value(dspstudio_device::DISPLAY display, Value value, bool event) {
        dspstudio_device::set_display_value(g_dev, display, to_float(value), 0);
    }
    static inline float get_value(dspstudio_device::DISPLAY display) {
        return dspstudio_device::get_display_value(g_dev, display, 0);
    }
    static inline daisy::Color get_color(dspstudio_device::DISPLAY display) {
        return rgb(
            dspstudio_device::get_display_value(g_dev, display, 0),
            dspstudio_device::get_display_value(g_dev, display, 1),
            dspstudio_device::get_display_value(g_dev, display, 2)
        );
    }
}


namespace dspstudio_device {
void HandleNoteOn(uint8_t note, uint8_t velocity, Voice* voices, uint16_t poly) {
    for (uint16_t p = 0; p < poly; p++) {
        Voice* voice = voices++;
        if (!voice->active) {
            voice->active = true;
            voice->pressed = true;
            voice->pitch = note;
            voice->onvelocity = velocity * (1.0f / 127.0f);
            voice->offvelocity = 0;
            return;
        }
    }
}

void HandleNoteOff(uint8_t note, uint8_t velocity, Voice* voices, uint16_t poly) {
    for (uint16_t p = 0; p < poly; p++) {
        Voice* voice = voices++;
        if (voice->pressed && voice->pitch == note) {
            voice->pressed = false;
            voice->offvelocity = velocity * (1.0f / 127.0f);
        }
    }
}

void HandleMidiMessage(daisy::MidiEvent m)
{    
    // handle controller
    if (m.type == daisy::ControlChange) {
        daisy::ControlChangeEvent cc = m.AsControlChange();
        if (auto* controller = get_controller(g_dev, cc.channel)) {
            controller->controller[cc.control_number] = cc.value / 127.0f;
        }
    } else if (m.type == daisy::PolyphonicKeyPressure) {
        daisy::ControlChangeEvent cc = m.AsControlChange();
        if (auto* controller = get_controller(g_dev, cc.channel)) {
            controller->aftertouch = cc.value / 127.0f;
        }
    } else if (m.type == daisy::PitchBend) {
        daisy::PitchBendEvent pb = m.AsPitchBend();
        if (auto* controller = get_controller(g_dev, pb.channel)) {
            controller->pitchwheel = pb.value / 8192.0f;
        }
    } else if (m.type == daisy::NoteOn || m.type == daisy::NoteOff) {
        // handle voices
        dspstudio_device::VoiceInfo* voiceInfo = dspstudio_device::get_voice_info(g_dev);
        if (voiceInfo != 0) {
            for (int index = 0; index < voiceInfo->num_voices; index++) {
                daisy::NoteOnEvent p = m.AsNoteOn();
                if (p.velocity != 0 && m.type == daisy::NoteOn) {
                    HandleNoteOn(p.note, p.velocity, voiceInfo->voices[index], voiceInfo->poly[index]);
                } else {
                    HandleNoteOff(p.note, p.velocity, voiceInfo->voices[index], voiceInfo->poly[index]);
                }
            }
        }
    }
}
}

void InitializeUsbMappings() {

    #ifdef _DSPSTUDIO_MAPPING_CONTROL_USBD_VID
        USBD_VID = _DSPSTUDIO_MAPPING_CONTROL_USBD_VID;
    #endif

    #ifdef _DSPSTUDIO_MAPPING_CONTROL_USBD_PID
        USBD_PID_HS = _DSPSTUDIO_MAPPING_CONTROL_USBD_PID;
        USBD_PID_FS = _DSPSTUDIO_MAPPING_CONTROL_USBD_PID;
    #endif

    #ifdef _DSPSTUDIO_MAPPING_CONTROL_TEXT_USBD_MANUFACTURER_STRING
        USBD_MANUFACTURER_STRING = _DSPSTUDIO_MAPPING_CONTROL_TEXT_USBD_MANUFACTURER_STRING;
    #endif

    #ifdef _DSPSTUDIO_MAPPING_CONTROL_TEXT_USBD_PRODUCT_STRING
        USBD_PRODUCT_STRING_HS = _DSPSTUDIO_MAPPING_CONTROL_TEXT_USBD_PRODUCT_STRING;
        USBD_PRODUCT_STRING_FS = _DSPSTUDIO_MAPPING_CONTROL_TEXT_USBD_PRODUCT_STRING;
    #endif

}