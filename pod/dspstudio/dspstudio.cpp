#include "daisysp.h"
#include "daisy_pod.h"
namespace dspstudio_device {
#include "dspstudio_generated.h"
}
#include "dspstudio_gluecode.h"
#include "dspstudio_util.h"
#include "usbd/UsbMidiHandler.h"

using namespace daisysp;
using namespace daisy;
using namespace wavloader;

static DaisyPod hw;
static UsbMidiHandler usbMidi;

using namespace daisy;
using namespace dspstudio_device;

dspstudio_device::mod_device* g_dev;

static void ProcessControls();

static void AudioCallback(const float* const* in, float** out, size_t size)
{
    ProcessControls();
    dspstudio_device::array_process_device(g_dev, in, 2, out, 2, size);
}
int main(void)
{
    InitializeUsbMappings();
    hw.Init(cpuBoost);

    g_dev = dspstudio_device::create();
    InitializeAudioRates(hw.seed);

    usbMidi.Init(UsbMidiHandler::FS_INTERNAL);
    usbMidi.StartReceive();

    hw.StartAdc();
    hw.StartAudio(AudioCallback);

    for(;;)
    {

        usbMidi.Listen();
        while(usbMidi.HasEvents())
        {
            HandleMidiMessage(usbMidi.PopEvent());
        }

    }
}

static void ProcessControls() {

    hw.ProcessAnalogControls();
    hw.ProcessDigitalControls();

    // Sensors: Knob Pot1-2
    {
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_POT_1
            _DSPSTUDIO_MAPPING_TYPE_POT_1::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_POT_1, hw.knob1.Value(), true);
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_POT_2
            _DSPSTUDIO_MAPPING_TYPE_POT_2::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_POT_2, hw.knob2.Value(), true);
        #endif
    }

    // Sensor: Encoder Knob
    {
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_ENC
            _DSPSTUDIO_MAPPING_TYPE_ENC::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_ENC, hw.encoder.Increment() == 1, hw.encoder.Increment() != 0);
        #endif
    }

    // Sensor: Encoder Button
    {
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_ENC_BUT
            _DSPSTUDIO_MAPPING_TYPE_ENC_BUT::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_ENC_BUT, hw.encoder.Pressed() || hw.encoder.RisingEdge(), hw.encoder.FallingEdge() || hw.encoder.RisingEdge());
        #endif
    }

    // Actors: Rgb Led 1-2
    {
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_RGB_LED_1
            hw.led1.SetColor(_DSPSTUDIO_MAPPING_TYPE_RGB_LED_1::get_color(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_RGB_LED_1));
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_RGB_LED_2
            hw.led2.SetColor(_DSPSTUDIO_MAPPING_TYPE_RGB_LED_2::get_color(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_RGB_LED_2));
        #endif
    }

    // Sensor: Switch 1-2
    {
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_SWITCH_1
            _DSPSTUDIO_MAPPING_TYPE_SWITCH_1::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_SWITCH_1, hw.button1.Pressed() || hw.button1.RisingEdge(), hw.button1.FallingEdge() || hw.button1.RisingEdge());
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_SWITCH_2
            _DSPSTUDIO_MAPPING_TYPE_SWITCH_2::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_SWITCH_2, hw.button2.Pressed() || hw.button2.RisingEdge(), hw.button2.FallingEdge() || hw.button2.RisingEdge());
        #endif
    }
}