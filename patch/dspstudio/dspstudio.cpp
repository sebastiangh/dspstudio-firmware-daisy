#include "daisysp.h"
#include "daisy_patch.h"
namespace dspstudio_device {
#include "dspstudio_generated.h"
}
#include "dspstudio_gluecode.h"
#include "dspstudio_util.h"
#include "usbd/UsbMidiHandler.h"

using namespace daisysp;
using namespace daisy;
using namespace wavloader;

char line1[30], line2[30], line3[30], line4[30], line5[30], line6[30];

static DaisyPatch hw;
static UsbMidiHandler usbMidi;
typedef enum {
    AUDIO_INIT,
    AUDIO_CPLT
} AudioState;

static volatile AudioState audioState = AUDIO_INIT;
static float *inValue;
static float **outValue;
static volatile size_t sizeValue;

extern ErrorCode lastLoadTableError;
extern size_t lastLoadTableFrames;

using namespace daisy;
using namespace dspstudio_device;

dspstudio_device::mod_device* g_dev;

static void ProcessControls();
static void ProcessDisplay();

// AudioCallback rate = Fs / block size = 48000 Hz / 48 = 1000 Hz 
static void AudioCallback(const float* const* in, float** out, size_t size) {
    // cpuMeasurementBegin();
    ProcessControls();
    dspstudio_device::array_process_device(g_dev, in, 4, out, 4, size);
    // cpuMeasurementEnd();
}

int main(void)
{
    InitializeUsbMappings();
    hw.Init(cpuBoost);

    hw.display.SetCursor(0, 0);
    hw.display.WriteString("Hola Daisy!", Font_6x8, false);
    hw.display.Update();

    g_dev = dspstudio_device::create();
    InitializeAudioRates(hw.seed);

    hw.midi.StartReceive();
    
    usbMidi.Init(UsbMidiHandler::FS_INTERNAL);
    usbMidi.StartReceive();

    hw.StartAdc();
    hw.StartAudio(AudioCallback);

    uint32_t last = 0;
    for(;;)
    {
        hw.midi.Listen();
        while(hw.midi.HasEvents())
        {
            HandleMidiMessage(hw.midi.PopEvent());
        }

        usbMidi.Listen();
        while(usbMidi.HasEvents())
        {
            HandleMidiMessage(usbMidi.PopEvent());
        }

        // if(audioState == AUDIO_CPLT) {
        //     ProcessControls();
        //     dspstudio_device::array_process_device(g_dev, (float* const*)inValue, 4, (float**)outValue, 4, sizeValue);
        //     audioState = AUDIO_INIT;
        // }

        
        uint32_t now = System::GetUs();
        if (now - last > 30000) {
            ProcessDisplay();
            last = now;
        }
    }
}

namespace dspstudio_device {
void draw_point(dspstudio_device::DISPLAY display, float x, float y, float z, float w, float r, float g, float b, float a, float size) {
    if (x >= -1 && x <= 1 && y >= -1 && y <= 1) {
        hw.display.DrawPixel(
            (x + 1.0f) * 0.5f * 60 + 64 + 2,
            (-y + 1.0f) * 0.5f * 60 + 2,
            1//grey > 0.5
        );
    }
}
}

static void ProcessControls() {

    hw.ProcessDigitalControls();
    hw.ProcessAnalogControls();


    // Sensors: Ctrl 1-4
    {
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_CTRL_1
            _DSPSTUDIO_MAPPING_TYPE_CTRL_1::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_CTRL_1, hw.controls[DaisyPatch::CTRL_1].Value(), true);
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_CTRL_2
            _DSPSTUDIO_MAPPING_TYPE_CTRL_2::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_CTRL_2, hw.controls[DaisyPatch::CTRL_2].Value(), true);
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_CTRL_3
            _DSPSTUDIO_MAPPING_TYPE_CTRL_3::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_CTRL_3, hw.controls[DaisyPatch::CTRL_3].Value(), true);
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_CTRL_4
            _DSPSTUDIO_MAPPING_TYPE_CTRL_4::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_CTRL_4, hw.controls[DaisyPatch::CTRL_4].Value(), true);
        #endif
    }

    // Sensor: Encoder Knob
    {
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_ENC
            _DSPSTUDIO_MAPPING_TYPE_ENC::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_ENC, hw.encoder.Increment() == 1, hw.encoder.Increment() != 0);
        #endif
    }

    // Sensor: Encoder Button
    {
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_ENC_BUT
            _DSPSTUDIO_MAPPING_TYPE_ENC_BUT::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_ENC_BUT, hw.encoder.Pressed() || hw.encoder.RisingEdge(), hw.encoder.FallingEdge() || hw.encoder.RisingEdge());
        #endif
    }

    // Sensor: Gate In 1-2
    {
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_GATE_IN_1
            _DSPSTUDIO_MAPPING_TYPE_GATE_IN_1::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_GATE_IN_1, hw.gate_input[DaisyPatch::GATE_IN_1].State(), true);
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_GATE_IN_2
            _DSPSTUDIO_MAPPING_TYPE_GATE_IN_2::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_GATE_IN_2, hw.gate_input[DaisyPatch::GATE_IN_2].State(), true);
        #endif
    }

    // Actors: CV Out 1-2
    {
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_CV_OUT_1
            hw.seed.dac.WriteValue(DacHandle::Channel::ONE, static_cast<uint16_t>(4095.99f * to_float(_DSPSTUDIO_MAPPING_TYPE_CV_OUT_1::get_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_CV_OUT_1))));
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_CV_OUT_2
            hw.seed.dac.WriteValue(DacHandle::Channel::TWO, static_cast<uint16_t>(4095.99f * to_float(_DSPSTUDIO_MAPPING_TYPE_CV_OUT_2::get_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_CV_OUT_2))));
        #endif
    }

    // Actor: Gate Out
    {
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_GATE_OUT
            dsy_gpio_write(&hw.gate_output, to_bool(_DSPSTUDIO_MAPPING_TYPE_GATE_OUT::get_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_GATE_OUT)));
        #endif
    }

    // Actor: Display
    {
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_DISPLAY
            process_display(g_dev, dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_DISPLAY, g_dev->bufferSize);
        #endif
    }
}




static void ProcessDisplay() {

    // float maxBlockTime = 1.0f * hw.AudioBlockSize() / hw.AudioSampleRate() * 1000 * 1000; // allowed time to pass for one block to be calculated
    
    // float wcet, avg;
    // cpuMeasurementEval(maxBlockTime, wcet, avg);

    //sprintf(line1, "AVG: %d.%02d%%", (int)avg, ((int)(avg * 100)) % 100);
    // sprintf(line2, "Frms: %d", (int)lastLoadTableFrames);
    if (lastLoadTableError == Success) {
         sprintf(line3, "Success");
    } else if (lastLoadTableError == FileNotFound) {
        sprintf(line3, "FileNotFound");
    } else if (lastLoadTableError == BrokenFile) {
        sprintf(line3, "BrokenFile");
    } else if (lastLoadTableError == UnsupportedFormat) {
        sprintf(line3, "UnsupportedFmt");
    } else if (lastLoadTableError == InvalidChannel) {
        sprintf(line3, "InvalidChan");
    }


    // //hw.display.SetCursor(0, 0);
    // //hw.display.WriteString(line1, Font_6x8, false);
    // hw.display.SetCursor(0, 8);
    // hw.display.WriteString(line2, Font_6x8, false);
    hw.display.SetCursor(0, 16);
    hw.display.WriteString(line3, Font_6x8, false);

    hw.display.Update();
    hw.display.Fill(false);
}
