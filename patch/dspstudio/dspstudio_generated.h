#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))
#define CLAMP(x, lower, upper) (MIN(upper, MAX(x, lower)))
#define SR 48000 // Samplerate
#define BS 48 // Blocksize
extern void* dspstudio_malloc(size_t size);
extern void dspstudio_load_table(const char* name, float** data, size_t* size);
typedef struct {
    float pitch;
    float onvelocity;
    float offvelocity;
    uint8_t pressed;
    uint8_t active;
} Voice;
typedef struct {
} Voices;
typedef struct {
    Voice** voices;
    uint16_t* poly;
    uint16_t num_voices;
} VoiceInfo;
typedef struct {
    float controller[129]; // 128 elements, first starts at position 1
    float aftertouch;
    float pitchwheel;
} Controller;
typedef struct {
} Controllers;
typedef struct {
} mod_write_ev;
typedef struct {
    float variable_var;
    mod_write_ev _mod_write_ev;
} mod_latch_ev;
typedef struct {
    float variable_unnamed;
    float variable_var;
} mod_Counter_BiDir;
typedef struct {
} mod_P2F;
typedef struct {
    float variable_unnamed;
} mod_Counter;
typedef struct {
} mod_Clip_Min;
typedef struct {
    mod_Clip_Min _mod_Clip_Min;
} mod_X_Fade_lin;
typedef struct {
} mod_Float2Int;
typedef struct {
    mod_Float2Int _mod_Float2Int;
} mod_Intfract;
typedef struct {
} mod_Hyp_Shaper;
typedef struct {
    mod_Float2Int _mod_Float2Int;
} mod_Formants;
typedef struct {
} mod_Interpol_RT;
typedef struct {
    mod_Intfract _mod_Intfract;
    mod_Interpol_RT _mod_Interpol_RT;
} mod_GranPlay;
typedef struct {
    mod_P2F _mod_P2F;
    mod_Counter _mod_Counter;
    mod_X_Fade_lin _mod_X_Fade_lin;
    mod_latch_ev _mod_latch_ev;
    mod_Intfract _mod_Intfract;
    mod_Clip_Min _mod_Clip_Min;
    mod_Hyp_Shaper _mod_Hyp_Shaper;
    mod_X_Fade_lin _mod_X_Fade_lin1;
    mod_latch_ev _mod_latch_ev1;
    mod_Formants _mod_Formants;
    mod_GranPlay _mod_GranPlay;
    mod_GranPlay _mod_GranPlay1;
    mod_GranPlay _mod_GranPlay2;
    mod_GranPlay _mod_GranPlay3;
    mod_Interpol_RT _mod_Interpol_RT;
} mod_Single_Voice;
typedef struct {
} mod_Max;
typedef struct {
    float variable_var;
} mod_LP1;
typedef struct {
    mod_LP1 _mod_LP1;
} mod__smth;
typedef struct {
    float variable_var;
} mod_Counter1;
typedef struct {
    mod_Float2Int _mod_Float2Int;
} mod_Phase2Sine;
typedef struct {
    mod_Phase2Sine _mod_Phase2Sine;
    mod_Phase2Sine _mod_Phase2Sine1;
    mod_Phase2Sine _mod_Phase2Sine2;
    mod_Phase2Sine _mod_Phase2Sine3;
} mod_MultiTapDelay1;
typedef struct {
    mod_Clip_Min _mod_Clip_Min;
} mod_Clip;
typedef struct {
    float variable_var;
} mod_Counter2;
typedef struct {
} mod_Play;
typedef struct {
    float* array_unnamed;
    mod_Counter1 _mod_Counter1;
    mod_MultiTapDelay1 _mod_MultiTapDelay1;
    mod_Clip _mod_Clip;
    mod_Intfract _mod_Intfract;
    mod_Counter2 _mod_Counter2;
    mod_Clip_Min _mod_Clip_Min;
    mod_Play _mod_Play;
    mod_Play _mod_Play1;
    mod_Play _mod_Play2;
    mod_Play _mod_Play3;
    mod_Interpol_RT _mod_Interpol_RT;
    float* array_unnamed1;
    mod_Clip _mod_Clip1;
    mod_Intfract _mod_Intfract1;
    mod_Counter2 _mod_Counter21;
    mod_Clip_Min _mod_Clip_Min1;
    mod_Play _mod_Play4;
    mod_Play _mod_Play5;
    mod_Play _mod_Play6;
    mod_Play _mod_Play7;
    mod_Interpol_RT _mod_Interpol_RT1;
    float* array_unnamed2;
    mod_Clip _mod_Clip2;
    mod_Intfract _mod_Intfract2;
    mod_Counter2 _mod_Counter22;
    mod_Clip_Min _mod_Clip_Min2;
    mod_Play _mod_Play8;
    mod_Play _mod_Play9;
    mod_Play _mod_Play10;
    mod_Play _mod_Play11;
    mod_Interpol_RT _mod_Interpol_RT2;
    float* array_unnamed3;
    mod_Clip _mod_Clip3;
    mod_Intfract _mod_Intfract3;
    mod_Counter2 _mod_Counter23;
    mod_Clip_Min _mod_Clip_Min3;
    mod_Play _mod_Play12;
    mod_Play _mod_Play13;
    mod_Play _mod_Play14;
    mod_Play _mod_Play15;
    mod_Interpol_RT _mod_Interpol_RT3;
    float* array_unnamed4;
    mod_Clip _mod_Clip4;
    mod_Intfract _mod_Intfract4;
    mod_Counter2 _mod_Counter24;
    mod_Clip_Min _mod_Clip_Min4;
    mod_Play _mod_Play16;
    mod_Play _mod_Play17;
    mod_Play _mod_Play18;
    mod_Play _mod_Play19;
    mod_Interpol_RT _mod_Interpol_RT4;
    float* array_unnamed5;
    mod_Clip _mod_Clip5;
    mod_Intfract _mod_Intfract5;
    mod_Counter2 _mod_Counter25;
    mod_Clip_Min _mod_Clip_Min5;
    mod_Play _mod_Play20;
    mod_Play _mod_Play21;
    mod_Play _mod_Play22;
    mod_Play _mod_Play23;
    mod_Interpol_RT _mod_Interpol_RT5;
    float* array_unnamed6;
    mod_Clip _mod_Clip6;
    mod_Intfract _mod_Intfract6;
    mod_Counter2 _mod_Counter26;
    mod_Clip_Min _mod_Clip_Min6;
    mod_Play _mod_Play24;
    mod_Play _mod_Play25;
    mod_Play _mod_Play26;
    mod_Play _mod_Play27;
    mod_Interpol_RT _mod_Interpol_RT6;
    float* array_unnamed7;
    mod_Clip _mod_Clip7;
    mod_Intfract _mod_Intfract7;
    mod_Counter2 _mod_Counter27;
    mod_Clip_Min _mod_Clip_Min7;
    mod_Play _mod_Play28;
    mod_Play _mod_Play29;
    mod_Play _mod_Play30;
    mod_Play _mod_Play31;
    mod_Interpol_RT _mod_Interpol_RT7;
} mod_MultiTapDelay;
typedef struct {
    mod_MultiTapDelay _mod_MultiTapDelay;
} mod_8VoiceChorusCore;
typedef struct {
    mod__smth _mod__smth;
    mod_8VoiceChorusCore _mod_8VoiceChorusCore;
} mod_8_VoiceChorusWet;
typedef struct {
    float variable_idx;
} mod_EncoderControl;
typedef struct {
} mod_Sample_Draw;
typedef struct {
    float variable_var;
    mod_write_ev _mod_write_ev;
    mod_write_ev _mod_write_ev1;
    mod_write_ev _mod_write_ev2;
    mod_write_ev _mod_write_ev3;
    mod_write_ev _mod_write_ev4;
    mod_write_ev _mod_write_ev5;
    mod_write_ev _mod_write_ev6;
    mod_write_ev _mod_write_ev7;
    mod_write_ev _mod_write_ev8;
    mod_write_ev _mod_write_ev9;
    mod_write_ev _mod_write_ev10;
} mod_Init_Default_vals;
typedef struct {
    float array_vals[11];
    float slider_Pitch;
    float slider_X;
    float slider_Speed;
    float button_Freeze;
    float button_Freeze_nextvalue;
    float variable_var;
    mod_latch_ev _mod_latch_ev;
    float button_Restart_Trg;
    float button_Restart_Trg_nextvalue;
    mod_latch_ev _mod_latch_ev1;
    uint32_t table_unnamed_size;
    float* table_unnamed;
    mod_Counter_BiDir _mod_Counter_BiDir;
    float slider_Frmnts;
    float slider_Hyp_Shp;
    float slider_Hyp_Bnd;
    mod_Single_Voice _mod_Single_Voice;
    mod_P2F _mod_P2F;
    mod_Max _mod_Max;
    mod_Clip_Min _mod_Clip_Min;
    float variable_var1;
    float variable_var2;
    mod_8_VoiceChorusWet _mod_8_VoiceChorusWet;
    float slider_YSize;
    float slider_YOffs;
    mod_Counter1 _mod_Counter1;
    float slider_YSize1;
    float slider_YOffs1;
    float button_inc;
    float button_inc_nextvalue;
    uint8_t button_inc_event;
    uint8_t button_inc_nextevent;
    float button_dec;
    float button_dec_nextvalue;
    uint8_t button_dec_event;
    uint8_t button_dec_nextevent;
    float button_Enc_Push;
    float button_Enc_Push_nextvalue;
    uint8_t button_Enc_Push_event;
    uint8_t button_Enc_Push_nextevent;
    float table_step[11];
    float table_unnamed1[22];
    mod_EncoderControl _mod_EncoderControl;
    float slider_YOffs2;
    float slider_Y3offs;
    float slider_Y4offs;
    float slider_YOffs3;
    float slider_YSize2;
    mod_Sample_Draw _mod_Sample_Draw;
    float slider_YSize3;
    float display_gl_scope_value_x1[48];
    float display_gl_scope_value_y1[48];
    float display_gl_scope_value_x2[48];
    float display_gl_scope_value_y2[48];
    float display_gl_scope_value_x3[48];
    float display_gl_scope_value_y3[48];
    float display_gl_scope_value_y4[48];
    float display_gl_scope_value_a[48];
    float display_gl_scope_value_x5[48];
    float display_gl_scope_value_y5[48];
    float display_gl_scope_value_x6[48];
    float display_gl_scope_value_y6[48];
    float label_unnamed;
    float label_unnamed1;
    float label_unnamed2;
    float label_unnamed3;
    float label_unnamed4;
    float label_unnamed5;
    float label_unnamed6;
    float label_unnamed7;
    float label_unnamed8;
    float label_menu_idx;
    float label_unnamed9;
    mod_Init_Default_vals _mod_Init_Default_vals;
    float label_unnamed10;
    float label_unnamed11;
    float sampleRate;
    uint32_t bufferSize;
    uint8_t isDoubleFp;
} mod_device;
uint32_t g_sampleIndex;
float t_g_table_0_5964404961324983_0[11] = {
    0.10000000149011612, 1, 0.009999999776482582, 0.05000000074505806, 1, 0.05000000074505806, 0.05000000074505806, 0.05000000074505806,
    0.05000000074505806, 0.010000002570450306, 0.20000000298023224
};
float t_g_table_0_32365927376408377_0[22] = {
    0, 1, -24, 24, -1, 1, 0, 6, 0, 130, 0, 0.9300000071525574, 0, 1, 0, 1, 0, 1, 0, 2, 0, 10
};
void process_write_ev(mod_write_ev* _this, float in_value, uint8_t in_value_ev, float* in_data_memory, uint32_t in_data_memorysize) {
    float temp = *in_data_memory;
    *in_data_memory = (in_value_ev ? in_value : temp);
}
void process_latch_ev(mod_latch_ev* _this, float in_in, uint8_t in_in_ev, float* out_out, uint8_t* out_out_ev) {
    uint8_t temp = in_in_ev;
    process_write_ev(&_this->_mod_write_ev, in_in, temp, &_this->variable_var, 1);
    float temp1 = _this->variable_var;
    *out_out = temp1;
    *out_out_ev = temp;
}
void process_Counter_BiDir(mod_Counter_BiDir* _this, float in_F, uint8_t in_trg, float* in_wave_memory, uint32_t in_wave_memorysize, float* out_ph) {
    uint8_t temp = in_trg;
    float temp1 = _this->variable_unnamed;
    float temp2 = ((in_F / ((float)in_wave_memorysize)) + temp1);
    float temp3 = _this->variable_var;
    float temp4 = (((temp2 * ((float)((temp2 < 1.0f) ? 1.0f : 0.0f))) * ((float)((temp2 >= -1.0f) ? 1.0f : 0.0f))) * ((float)((((float)(temp ? 1.0f : 0.0f)) <= temp3) ? 1.0f : 0.0f)));
    _this->variable_unnamed = temp4;
    _this->variable_var = ((float)(temp ? 1.0f : 0.0f));
    *out_ph = temp4;
}
void process_P2F(mod_P2F* _this, float in_P, float* out_F) {
    *out_F = (8.1758f * exp2f((in_P / 12.0f)));
}
void process_Counter(mod_Counter* _this, float in_F, float* out_ph, uint8_t* out_0_, uint8_t* out_trg) {
    float temp = _this->variable_unnamed;
    float temp1 = ((in_F / SR) + temp);
    uint8_t temp2 = (temp1 < 1.0f);
    float temp3 = (temp1 * ((float)(temp2 ? 1.0f : 0.0f)));
    _this->variable_unnamed = temp3;
    *out_ph = temp3;
    *out_0_ = temp2;
    *out_trg = (temp1 < 0.15f);
}
void process_Clip_Min(mod_Clip_Min* _this, float in_min, float in_in, float* out_out) {
    float temp = in_min;
    float temp1 = in_in;
    *out_out = ((temp1 < temp) ? temp : temp1);
}
void process_X_Fade_lin(mod_X_Fade_lin* _this, float in_1, float in_2, float in_Ctrl, float* out_out) {
    float t_out;
    process_Clip_Min(&_this->_mod_Clip_Min, 0.0f, in_Ctrl, &t_out);
    float temp = ((t_out > 1.0f) ? 1.0f : t_out);
    *out_out = (((1.0f - temp) * in_1) + (temp * in_2));
}
void process_Float2Int(mod_Float2Int* _this, int32_t in_in, int32_t* out_out) {
    *out_out = in_in;
}
void process_Intfract(mod_Intfract* _this, float in_in, int32_t* out_int, float* out_frc) {
    float temp = in_in;
    int32_t t_out;
    process_Float2Int(&_this->_mod_Float2Int, ((int32_t)temp), &t_out);
    *out_int = t_out;
    *out_frc = (temp - ((float)t_out));
}
void process_Hyp_Shaper(mod_Hyp_Shaper* _this, float in_x, float in_b, float* out_y) {
    float temp = in_x;
    float temp1 = in_b;
    float temp2 = (1.0f - temp1);
    float temp3 = (1.0f + temp1);
    float temp4 = ((4.0f * temp1) / (temp2 * temp3));
    float temp5 = (temp2 / temp3);
    *out_y = (((temp4 + temp5) * temp) / ((((temp < 0.0f) ? (temp * -1.0f) : temp) * temp4) + temp5));
}
void process_Formants(mod_Formants* _this, float in_x, float in_formants, float* out_y) {
    float temp = (in_x * in_formants);
    int32_t t_out;
    process_Float2Int(&_this->_mod_Float2Int, ((int32_t)temp), &t_out);
    *out_y = (temp - ((float)t_out));
}
void process_Interpol_RT(mod_Interpol_RT* _this, float in_frc, float in_t_1, float in_t0, float in_t1, float in_t2, float* out_out) {
    float temp = in_frc;
    float temp1 = in_t0;
    float temp2 = in_t1;
    float temp3 = ((temp2 - in_t_1) * 0.5f);
    float temp4 = (temp1 - temp2);
    float temp5 = (temp3 + temp4);
    float temp6 = ((((in_t2 - temp1) * 0.5f) + temp5) + temp4);
    *out_out = (temp1 + (temp * (temp3 + (temp * ((temp * temp6) - (temp5 + temp6))))));
}
void process_GranPlay(mod_GranPlay* _this, float* in_buf_memory, uint32_t in_buf_memorysize, float in_idx, float in__, float* out_out) {
    float temp = in__;
    int32_t t_int;
    float t_frc;
    process_Intfract(&_this->_mod_Intfract, in_idx, &t_int, &t_frc);
    float temp1 = (((float)t_int) + ((float)((int32_t)temp)));
    int32_t temp2 = in_buf_memorysize;
    float temp3 = ((((float)((int32_t)temp1)) < 0.0f) ? (((float)((int32_t)temp1)) + ((float)temp2)) : ((float)((int32_t)temp1)));
    float temp4 = ((temp3 >= ((float)temp2)) ? (temp3 - ((float)temp2)) : temp3);
    float temp5 = in_buf_memory[((int32_t)((float)((int32_t)temp4)))];
    float temp6 = (((float)t_int) + 1.0f);
    float temp7 = (((float)((int32_t)temp6)) + ((float)((int32_t)temp)));
    int32_t temp8 = in_buf_memorysize;
    float temp9 = ((((float)((int32_t)temp7)) < 0.0f) ? (((float)((int32_t)temp7)) + ((float)temp8)) : ((float)((int32_t)temp7)));
    float temp10 = ((temp9 >= ((float)temp8)) ? (temp9 - ((float)temp8)) : temp9);
    float temp11 = in_buf_memory[((int32_t)((float)((int32_t)temp10)))];
    float temp12 = (temp6 + 1.0f);
    float temp13 = (((float)((int32_t)temp12)) + ((float)((int32_t)temp)));
    int32_t temp14 = in_buf_memorysize;
    float temp15 = ((((float)((int32_t)temp13)) < 0.0f) ? (((float)((int32_t)temp13)) + ((float)temp14)) : ((float)((int32_t)temp13)));
    float temp16 = ((temp15 >= ((float)temp14)) ? (temp15 - ((float)temp14)) : temp15);
    float temp17 = in_buf_memory[((int32_t)((float)((int32_t)temp16)))];
    float temp18 = (((float)((int32_t)(temp12 + 1.0f))) + ((float)((int32_t)temp)));
    int32_t temp19 = in_buf_memorysize;
    float temp20 = ((((float)((int32_t)temp18)) < 0.0f) ? (((float)((int32_t)temp18)) + ((float)temp19)) : ((float)((int32_t)temp18)));
    float temp21 = ((temp20 >= ((float)temp19)) ? (temp20 - ((float)temp19)) : temp20);
    float temp22 = in_buf_memory[((int32_t)((float)((int32_t)temp21)))];
    float t_out;
    process_Interpol_RT(&_this->_mod_Interpol_RT, t_frc, temp5, temp11, temp17, temp22, &t_out);
    *out_out = t_out;
}
void process_Single_Voice(mod_Single_Voice* _this, float in_P, float in_X, float in_Spd_offset, float in_Frmnt, float* in_wave_memory, uint32_t in_wave_memorysize, float in_smooth, float in_GrSize, float in_Hyp_Shp, float in_Hyp_Bnd, float* out_Out, float* out_idx, uint8_t* out_1_, uint8_t* out_trg, float* out_pos) {
    float temp = in_Frmnt;
    float temp1 = in_GrSize;
    float t_F;
    process_P2F(&_this->_mod_P2F, in_P, &t_F);
    float t_ph;
    uint8_t t_0_;
    uint8_t t_trg;
    process_Counter(&_this->_mod_Counter, t_F, &t_ph, &t_0_, &t_trg);
    float temp2 = ((2.0f * t_ph) - 1.0f);
    float temp3 = (temp2 * temp2);
    float temp4 = (temp3 * temp3);
    float temp5 = (temp4 * temp4);
    float temp6 = (temp5 * temp5);
    float temp7 = (temp6 * temp6);
    float temp8 = (temp7 * temp7);
    float temp9 = (temp8 * temp8);
    float t_out;
    process_X_Fade_lin(&_this->_mod_X_Fade_lin, (temp9 * temp9), temp4, in_smooth, &t_out);
    float temp10 = ((1.0f * in_X) + in_Spd_offset);
    float temp11 = (((float)in_wave_memorysize) / temp1);
    float temp12 = (temp10 * temp11);
    float temp13 = ((temp12 < 0.0f) ? (temp12 + temp11) : temp12);
    uint8_t temp14 = (1 - t_0_);
    float t_out1;
    uint8_t t_out_ev;
    process_latch_ev(&_this->_mod_latch_ev, ((temp13 >= temp11) ? (temp13 - temp11) : temp13), temp14, &t_out1, &t_out_ev);
    int32_t t_int;
    float t_frc;
    process_Intfract(&_this->_mod_Intfract, t_out1, &t_int, &t_frc);
    float t_out2;
    process_Clip_Min(&_this->_mod_Clip_Min, -0.99f, in_Hyp_Shp, &t_out2);
    float t_y;
    process_Hyp_Shaper(&_this->_mod_Hyp_Shaper, ((t_ph * 2.0f) + -1.0f), ((t_out2 > 0.99f) ? 0.99f : t_out2), &t_y);
    float t_out3;
    process_X_Fade_lin(&_this->_mod_X_Fade_lin1, t_ph, ((t_y * 0.5f) + 0.5f), in_Hyp_Bnd, &t_out3);
    float t_out4;
    uint8_t t_out_ev1;
    process_latch_ev(&_this->_mod_latch_ev1, ((((1.0f * ((temp < 0.0f) ? (0.5f * temp) : temp)) * 40.0f) + 20.0f) / t_F), temp14, &t_out4, &t_out_ev1);
    float temp15 = 0.01f;
    float temp16 = 24.0f;
    float temp17 = t_out4;
    float t_y1;
    process_Formants(&_this->_mod_Formants, t_out3, MAX(temp15, MIN(temp16, temp17)), &t_y1);
    float temp18 = (temp1 * t_y1);
    float temp19 = (temp1 * 1.0f);
    float temp20 = (((float)t_int) * temp19);
    float t_out5;
    process_GranPlay(&_this->_mod_GranPlay, in_wave_memory, in_wave_memorysize, temp18, (temp20 - temp19), &t_out5);
    float t_out6;
    process_GranPlay(&_this->_mod_GranPlay1, in_wave_memory, in_wave_memorysize, temp18, temp20, &t_out6);
    float temp21 = (temp20 + temp19);
    float t_out7;
    process_GranPlay(&_this->_mod_GranPlay2, in_wave_memory, in_wave_memorysize, temp18, temp21, &t_out7);
    float t_out8;
    process_GranPlay(&_this->_mod_GranPlay3, in_wave_memory, in_wave_memorysize, temp18, (temp21 + temp19), &t_out8);
    float t_out9;
    process_Interpol_RT(&_this->_mod_Interpol_RT, t_frc, t_out5, t_out6, t_out7, t_out8, &t_out9);
    *out_Out = ((1.0f - t_out) * t_out9);
    *out_idx = t_ph;
    *out_1_ = temp14;
    *out_trg = t_trg;
    *out_pos = temp10;
}
void process_Max(mod_Max* _this, float in_A, float in_B, float* out_out) {
    float temp = in_A;
    float temp1 = in_B;
    *out_out = ((temp > temp1) ? temp : temp1);
}
void process_LP1(mod_LP1* _this, float in_in, float in_F, float* out_out) {
    float temp = (in_F * (6.28319f / SR));
    float temp1 = _this->variable_var;
    float temp2 = ((((temp > 1.0f) ? 1.0f : temp) * (in_in - temp1)) + temp1);
    _this->variable_var = temp2;
    *out_out = temp2;
}
void process__smth(mod__smth* _this, float in_in, float* out_out) {
    float t_out;
    process_LP1(&_this->_mod_LP1, in_in, 100.0f, &t_out);
    *out_out = t_out;
}
void process_Counter1(mod_Counter1* _this, float in_N, float in_F, float* out_idx) {
    float temp = _this->variable_var;
    float temp1 = ((in_F / SR) + temp);
    _this->variable_var = (((float)((in_N > temp1) ? 1.0f : 0.0f)) * temp1);
    *out_idx = temp;
}
void process_Phase2Sine(mod_Phase2Sine* _this, float in_port, float* out_port) {
    float temp = in_port;
    int32_t t_out;
    process_Float2Int(&_this->_mod_Float2Int, ((int32_t)temp), &t_out);
    float temp1 = ((temp - ((float)t_out)) - 0.5f);
    float temp2 = (temp1 + temp1);
    float temp3 = (0.5f - ((temp2 < 0.0f) ? (temp2 * -1.0f) : temp2));
    float temp4 = (temp3 * temp3);
    *out_port = (((((((temp4 * -0.540347f) + 2.53566f) * temp4) + -5.16651f) * temp4) + 3.14159f) * temp3);
}
void process_MultiTapDelay1(mod_MultiTapDelay1* _this, float in_depth, float in_delay, float in_F_smooth, float in_counter, float in_width, float* out_1, float* out_2, float* out_3, float* out_4, float* out_5, float* out_6, float* out_7, float* out_8) {
    float temp = in_depth;
    float temp1 = in_delay;
    float temp2 = in_counter;
    float temp3 = in_width;
    float t_port;
    process_Phase2Sine(&_this->_mod_Phase2Sine, temp2, &t_port);
    float temp4 = (temp1 + (temp * t_port));
    float temp5 = (temp1 + (temp * (t_port * -1.0f)));
    float temp6 = (temp2 + temp3);
    float t_port1;
    process_Phase2Sine(&_this->_mod_Phase2Sine1, temp6, &t_port1);
    float temp7 = (temp1 + (temp * t_port1));
    float temp8 = (temp1 + (temp * (t_port1 * -1.0f)));
    float temp9 = (temp6 + temp3);
    float t_port2;
    process_Phase2Sine(&_this->_mod_Phase2Sine2, temp9, &t_port2);
    float temp10 = (temp1 + (temp * t_port2));
    float temp11 = (temp1 + (temp * (t_port2 * -1.0f)));
    float t_port3;
    process_Phase2Sine(&_this->_mod_Phase2Sine3, (temp9 + temp3), &t_port3);
    float temp12 = (temp1 + (temp * t_port3));
    float temp13 = (temp1 + (temp * (t_port3 * -1.0f)));
    *out_1 = temp4;
    *out_2 = temp5;
    *out_3 = temp7;
    *out_4 = temp8;
    *out_5 = temp10;
    *out_6 = temp11;
    *out_7 = temp12;
    *out_8 = temp13;
}
void process_Clip(mod_Clip* _this, float* in_arr_memory, uint32_t in_arr_memorysize, float in__, float* out_out) {
    float temp = (((float)in_arr_memorysize) - 2.0f);
    float t_out;
    process_Clip_Min(&_this->_mod_Clip_Min, 1.0f, in__, &t_out);
    *out_out = ((t_out > temp) ? temp : t_out);
}
void process_Counter2(mod_Counter2* _this, float in_N, float* out_idx) {
    float temp = in_N;
    float temp1 = _this->variable_var;
    float temp2 = (temp1 + 1.0f);
    float temp3 = ((temp2 < temp) ? temp2 : (temp2 - temp));
    _this->variable_var = temp3;
    *out_idx = temp3;
}
void process_Play(mod_Play* _this, int32_t in_idx, int32_t in__, float* in_arr_memory, uint32_t in_arr_memorysize, float* out_out) {
    int32_t temp = in_arr_memorysize;
    float temp1 = (((float)in_idx) - ((float)in__));
    float temp2 = ((((float)((int32_t)temp1)) < 0.0f) ? (((float)((int32_t)temp1)) + ((float)temp)) : ((float)((int32_t)temp1)));
    float temp3 = in_arr_memory[((int32_t)((float)((int32_t)((temp2 > ((float)temp)) ? ((float)temp) : temp2))))];
    *out_out = temp3;
}
void process_MultiTapDelay(mod_MultiTapDelay* _this, float in_L, float in_R, float in_Delay, float in_Depth, float in_Speed, float* out_1, float* out_2, float* out_3, float* out_4, float* out_5, float* out_6, float* out_7, float* out_8) {
    float temp = in_L;
    float temp1 = in_R;
    float temp2 = in_Depth;
    float temp3 = in_Speed;
    float t_idx;
    process_Counter1(&_this->_mod_Counter1, 1.0f, temp3, &t_idx);
    float t_1;
    float t_2;
    float t_3;
    float t_4;
    float t_5;
    float t_6;
    float t_7;
    float t_8;
    process_MultiTapDelay1(&_this->_mod_MultiTapDelay1, (1.0f * temp2), (1.0f * (in_Delay + temp2)), temp3, t_idx, 0.125f, &t_1, &t_2, &t_3, &t_4, &t_5, &t_6, &t_7, &t_8);
    float t_out;
    process_Clip(&_this->_mod_Clip, _this->array_unnamed, 48000, ((t_1 * 0.001f) * SR), &t_out);
    int32_t t_int;
    float t_frc;
    process_Intfract(&_this->_mod_Intfract, t_out, &t_int, &t_frc);
    float t_idx1;
    process_Counter2(&_this->_mod_Counter2, ((float)48000), &t_idx1);
    float t_out1;
    process_Clip_Min(&_this->_mod_Clip_Min, 0.0f, (-1.0f + ((float)t_int)), &t_out1);
    float t_out2;
    process_Play(&_this->_mod_Play, ((int32_t)t_idx1), ((int32_t)t_out1), _this->array_unnamed, 48000, &t_out2);
    float t_out3;
    process_Play(&_this->_mod_Play1, ((int32_t)t_idx1), t_int, _this->array_unnamed, 48000, &t_out3);
    float t_out4;
    process_Play(&_this->_mod_Play2, ((int32_t)t_idx1), ((int32_t)(1.0f + ((float)t_int))), _this->array_unnamed, 48000, &t_out4);
    float t_out5;
    process_Play(&_this->_mod_Play3, ((int32_t)t_idx1), ((int32_t)(2.0f + ((float)t_int))), _this->array_unnamed, 48000, &t_out5);
    float t_out6;
    process_Interpol_RT(&_this->_mod_Interpol_RT, t_frc, t_out2, t_out3, t_out4, t_out5, &t_out6);
    float t_out7;
    process_Clip(&_this->_mod_Clip1, _this->array_unnamed1, 48000, ((t_2 * 0.001f) * SR), &t_out7);
    int32_t t_int1;
    float t_frc1;
    process_Intfract(&_this->_mod_Intfract1, t_out7, &t_int1, &t_frc1);
    float t_idx2;
    process_Counter2(&_this->_mod_Counter21, ((float)48000), &t_idx2);
    float t_out8;
    process_Clip_Min(&_this->_mod_Clip_Min1, 0.0f, (-1.0f + ((float)t_int1)), &t_out8);
    float t_out9;
    process_Play(&_this->_mod_Play4, ((int32_t)t_idx2), ((int32_t)t_out8), _this->array_unnamed1, 48000, &t_out9);
    float t_out10;
    process_Play(&_this->_mod_Play5, ((int32_t)t_idx2), t_int1, _this->array_unnamed1, 48000, &t_out10);
    float t_out11;
    process_Play(&_this->_mod_Play6, ((int32_t)t_idx2), ((int32_t)(1.0f + ((float)t_int1))), _this->array_unnamed1, 48000, &t_out11);
    float t_out12;
    process_Play(&_this->_mod_Play7, ((int32_t)t_idx2), ((int32_t)(2.0f + ((float)t_int1))), _this->array_unnamed1, 48000, &t_out12);
    float t_out13;
    process_Interpol_RT(&_this->_mod_Interpol_RT1, t_frc1, t_out9, t_out10, t_out11, t_out12, &t_out13);
    float t_out14;
    process_Clip(&_this->_mod_Clip2, _this->array_unnamed2, 48000, ((t_3 * 0.001f) * SR), &t_out14);
    int32_t t_int2;
    float t_frc2;
    process_Intfract(&_this->_mod_Intfract2, t_out14, &t_int2, &t_frc2);
    float t_idx3;
    process_Counter2(&_this->_mod_Counter22, ((float)48000), &t_idx3);
    float t_out15;
    process_Clip_Min(&_this->_mod_Clip_Min2, 0.0f, (-1.0f + ((float)t_int2)), &t_out15);
    float t_out16;
    process_Play(&_this->_mod_Play8, ((int32_t)t_idx3), ((int32_t)t_out15), _this->array_unnamed2, 48000, &t_out16);
    float t_out17;
    process_Play(&_this->_mod_Play9, ((int32_t)t_idx3), t_int2, _this->array_unnamed2, 48000, &t_out17);
    float t_out18;
    process_Play(&_this->_mod_Play10, ((int32_t)t_idx3), ((int32_t)(1.0f + ((float)t_int2))), _this->array_unnamed2, 48000, &t_out18);
    float t_out19;
    process_Play(&_this->_mod_Play11, ((int32_t)t_idx3), ((int32_t)(2.0f + ((float)t_int2))), _this->array_unnamed2, 48000, &t_out19);
    float t_out20;
    process_Interpol_RT(&_this->_mod_Interpol_RT2, t_frc2, t_out16, t_out17, t_out18, t_out19, &t_out20);
    float t_out21;
    process_Clip(&_this->_mod_Clip3, _this->array_unnamed3, 48000, ((t_4 * 0.001f) * SR), &t_out21);
    int32_t t_int3;
    float t_frc3;
    process_Intfract(&_this->_mod_Intfract3, t_out21, &t_int3, &t_frc3);
    float t_idx4;
    process_Counter2(&_this->_mod_Counter23, ((float)48000), &t_idx4);
    float t_out22;
    process_Clip_Min(&_this->_mod_Clip_Min3, 0.0f, (-1.0f + ((float)t_int3)), &t_out22);
    float t_out23;
    process_Play(&_this->_mod_Play12, ((int32_t)t_idx4), ((int32_t)t_out22), _this->array_unnamed3, 48000, &t_out23);
    float t_out24;
    process_Play(&_this->_mod_Play13, ((int32_t)t_idx4), t_int3, _this->array_unnamed3, 48000, &t_out24);
    float t_out25;
    process_Play(&_this->_mod_Play14, ((int32_t)t_idx4), ((int32_t)(1.0f + ((float)t_int3))), _this->array_unnamed3, 48000, &t_out25);
    float t_out26;
    process_Play(&_this->_mod_Play15, ((int32_t)t_idx4), ((int32_t)(2.0f + ((float)t_int3))), _this->array_unnamed3, 48000, &t_out26);
    float t_out27;
    process_Interpol_RT(&_this->_mod_Interpol_RT3, t_frc3, t_out23, t_out24, t_out25, t_out26, &t_out27);
    float t_out28;
    process_Clip(&_this->_mod_Clip4, _this->array_unnamed4, 48000, ((t_5 * 0.001f) * SR), &t_out28);
    int32_t t_int4;
    float t_frc4;
    process_Intfract(&_this->_mod_Intfract4, t_out28, &t_int4, &t_frc4);
    float t_idx5;
    process_Counter2(&_this->_mod_Counter24, ((float)48000), &t_idx5);
    float t_out29;
    process_Clip_Min(&_this->_mod_Clip_Min4, 0.0f, (-1.0f + ((float)t_int4)), &t_out29);
    float t_out30;
    process_Play(&_this->_mod_Play16, ((int32_t)t_idx5), ((int32_t)t_out29), _this->array_unnamed4, 48000, &t_out30);
    float t_out31;
    process_Play(&_this->_mod_Play17, ((int32_t)t_idx5), t_int4, _this->array_unnamed4, 48000, &t_out31);
    float t_out32;
    process_Play(&_this->_mod_Play18, ((int32_t)t_idx5), ((int32_t)(1.0f + ((float)t_int4))), _this->array_unnamed4, 48000, &t_out32);
    float t_out33;
    process_Play(&_this->_mod_Play19, ((int32_t)t_idx5), ((int32_t)(2.0f + ((float)t_int4))), _this->array_unnamed4, 48000, &t_out33);
    float t_out34;
    process_Interpol_RT(&_this->_mod_Interpol_RT4, t_frc4, t_out30, t_out31, t_out32, t_out33, &t_out34);
    float t_out35;
    process_Clip(&_this->_mod_Clip5, _this->array_unnamed5, 48000, ((t_6 * 0.001f) * SR), &t_out35);
    int32_t t_int5;
    float t_frc5;
    process_Intfract(&_this->_mod_Intfract5, t_out35, &t_int5, &t_frc5);
    float t_idx6;
    process_Counter2(&_this->_mod_Counter25, ((float)48000), &t_idx6);
    float t_out36;
    process_Clip_Min(&_this->_mod_Clip_Min5, 0.0f, (-1.0f + ((float)t_int5)), &t_out36);
    float t_out37;
    process_Play(&_this->_mod_Play20, ((int32_t)t_idx6), ((int32_t)t_out36), _this->array_unnamed5, 48000, &t_out37);
    float t_out38;
    process_Play(&_this->_mod_Play21, ((int32_t)t_idx6), t_int5, _this->array_unnamed5, 48000, &t_out38);
    float t_out39;
    process_Play(&_this->_mod_Play22, ((int32_t)t_idx6), ((int32_t)(1.0f + ((float)t_int5))), _this->array_unnamed5, 48000, &t_out39);
    float t_out40;
    process_Play(&_this->_mod_Play23, ((int32_t)t_idx6), ((int32_t)(2.0f + ((float)t_int5))), _this->array_unnamed5, 48000, &t_out40);
    float t_out41;
    process_Interpol_RT(&_this->_mod_Interpol_RT5, t_frc5, t_out37, t_out38, t_out39, t_out40, &t_out41);
    float t_out42;
    process_Clip(&_this->_mod_Clip6, _this->array_unnamed6, 48000, ((t_7 * 0.001f) * SR), &t_out42);
    int32_t t_int6;
    float t_frc6;
    process_Intfract(&_this->_mod_Intfract6, t_out42, &t_int6, &t_frc6);
    float t_idx7;
    process_Counter2(&_this->_mod_Counter26, ((float)48000), &t_idx7);
    float t_out43;
    process_Clip_Min(&_this->_mod_Clip_Min6, 0.0f, (-1.0f + ((float)t_int6)), &t_out43);
    float t_out44;
    process_Play(&_this->_mod_Play24, ((int32_t)t_idx7), ((int32_t)t_out43), _this->array_unnamed6, 48000, &t_out44);
    float t_out45;
    process_Play(&_this->_mod_Play25, ((int32_t)t_idx7), t_int6, _this->array_unnamed6, 48000, &t_out45);
    float t_out46;
    process_Play(&_this->_mod_Play26, ((int32_t)t_idx7), ((int32_t)(1.0f + ((float)t_int6))), _this->array_unnamed6, 48000, &t_out46);
    float t_out47;
    process_Play(&_this->_mod_Play27, ((int32_t)t_idx7), ((int32_t)(2.0f + ((float)t_int6))), _this->array_unnamed6, 48000, &t_out47);
    float t_out48;
    process_Interpol_RT(&_this->_mod_Interpol_RT6, t_frc6, t_out44, t_out45, t_out46, t_out47, &t_out48);
    float t_out49;
    process_Clip(&_this->_mod_Clip7, _this->array_unnamed7, 48000, ((t_8 * 0.001f) * SR), &t_out49);
    int32_t t_int7;
    float t_frc7;
    process_Intfract(&_this->_mod_Intfract7, t_out49, &t_int7, &t_frc7);
    float t_idx8;
    process_Counter2(&_this->_mod_Counter27, ((float)48000), &t_idx8);
    float t_out50;
    process_Clip_Min(&_this->_mod_Clip_Min7, 0.0f, (-1.0f + ((float)t_int7)), &t_out50);
    float t_out51;
    process_Play(&_this->_mod_Play28, ((int32_t)t_idx8), ((int32_t)t_out50), _this->array_unnamed7, 48000, &t_out51);
    float t_out52;
    process_Play(&_this->_mod_Play29, ((int32_t)t_idx8), t_int7, _this->array_unnamed7, 48000, &t_out52);
    float t_out53;
    process_Play(&_this->_mod_Play30, ((int32_t)t_idx8), ((int32_t)(1.0f + ((float)t_int7))), _this->array_unnamed7, 48000, &t_out53);
    float t_out54;
    process_Play(&_this->_mod_Play31, ((int32_t)t_idx8), ((int32_t)(2.0f + ((float)t_int7))), _this->array_unnamed7, 48000, &t_out54);
    float t_out55;
    process_Interpol_RT(&_this->_mod_Interpol_RT7, t_frc7, t_out51, t_out52, t_out53, t_out54, &t_out55);
    _this->array_unnamed[((int32_t)t_idx1)] = temp;
    _this->array_unnamed1[((int32_t)t_idx2)] = temp1;
    _this->array_unnamed2[((int32_t)t_idx3)] = temp;
    _this->array_unnamed3[((int32_t)t_idx4)] = temp1;
    _this->array_unnamed4[((int32_t)t_idx5)] = temp;
    _this->array_unnamed5[((int32_t)t_idx6)] = temp1;
    _this->array_unnamed6[((int32_t)t_idx7)] = temp;
    _this->array_unnamed7[((int32_t)t_idx8)] = temp1;
    *out_1 = t_out6;
    *out_2 = t_out13;
    *out_3 = t_out20;
    *out_4 = t_out27;
    *out_5 = t_out34;
    *out_6 = t_out41;
    *out_7 = t_out48;
    *out_8 = t_out55;
}
void process_8VoiceChorusCore(mod_8VoiceChorusCore* _this, float in_L, float in_R, float in_Depth, float in_Speed, float* out_L, float* out_R) {
    float t_1;
    float t_2;
    float t_3;
    float t_4;
    float t_5;
    float t_6;
    float t_7;
    float t_8;
    process_MultiTapDelay(&_this->_mod_MultiTapDelay, in_L, in_R, 3.0f, in_Depth, in_Speed, &t_1, &t_2, &t_3, &t_4, &t_5, &t_6, &t_7, &t_8);
    *out_L = (((t_1 + t_3) + t_5) + t_7);
    *out_R = (((t_2 + t_4) + t_6) + t_8);
}
void process_8_VoiceChorusWet(mod_8_VoiceChorusWet* _this, float in_L, float in_R, float in_Speed, float in_Depth, float* out_L, float* out_R) {
    float temp = in_Speed;
    float temp1 = in_Depth;
    float t_out;
    process__smth(&_this->_mod__smth, (temp1 * temp1), &t_out);
    float t_L;
    float t_R;
    process_8VoiceChorusCore(&_this->_mod_8VoiceChorusCore, in_L, in_R, t_out, (temp * temp), &t_L, &t_R);
    *out_L = t_L;
    *out_R = t_R;
}
void process_EncoderControl(mod_EncoderControl* _this, float in_enc_val, float in_enc_ev, float in_push_val, float in_push_ev, float* in_step_memory, uint32_t in_step_memorysize, float* in_val_buf_memory, uint32_t in_val_buf_memorysize, float* in_Rng_buf_memory, uint32_t in_Rng_buf_memorysize, float* out_idx) {
    float temp = _this->variable_idx;
    float temp1 = in_step_memory[((int32_t)temp)];
    float temp2 = in_val_buf_memory[((int32_t)temp)];
    int32_t temp3 = (((int32_t)temp) * ((int32_t)2.0f));
    float temp4 = in_Rng_buf_memory[temp3];
    float temp5 = in_Rng_buf_memory[((int32_t)(((float)temp3) + 1.0f))];
    float temp6 = temp4;
    float temp7 = temp5;
    float temp8 = (((in_enc_val * in_enc_ev) * temp1) + temp2);
    in_val_buf_memory[((int32_t)temp)] = MAX(temp6, MIN(temp7, temp8));
    float temp9 = ((in_push_val * in_push_ev) + temp);
    _this->variable_idx = (((float)((temp9 < ((float)in_val_buf_memorysize)) ? 1.0f : 0.0f)) * temp9);
    *out_idx = temp;
}
void process_Sample_Draw(mod_Sample_Draw* _this, float* in_table_memory, uint32_t in_table_memorysize, float in_Display_id, float* out_sample) {
    int32_t temp = in_table_memorysize;
    float temp1 = ((0.5f * in_Display_id) * ((float)temp));
    float temp2 = in_table_memory[((int32_t)floorf(temp1))];
    float temp3 = 0.0f;
    float temp4 = ((float)temp);
    float temp5 = (temp1 + 1.0f);
    float temp6 = MAX(temp3, MIN(temp4, temp5));
    float temp7 = in_table_memory[((int32_t)floorf(temp6))];
    *out_sample = ((temp2 + temp7) / 2.0f);
}
void process_Init_Default_vals(mod_Init_Default_vals* _this, float* in_buf_memory, uint32_t in_buf_memorysize) {
    float temp = _this->variable_var;
    uint8_t temp1 = (1.0f > temp);
    process_write_ev(&_this->_mod_write_ev, 1.0f, temp1, &in_buf_memory[((int32_t)0.0f)], in_buf_memorysize);
    _this->variable_var = 1.0f;
    process_write_ev(&_this->_mod_write_ev1, 0.0f, temp1, &in_buf_memory[((int32_t)1.0f)], in_buf_memorysize);
    process_write_ev(&_this->_mod_write_ev2, 0.0f, temp1, &in_buf_memory[((int32_t)2.0f)], in_buf_memorysize);
    process_write_ev(&_this->_mod_write_ev3, 0.9f, temp1, &in_buf_memory[((int32_t)3.0f)], in_buf_memorysize);
    process_write_ev(&_this->_mod_write_ev4, 20.0f, temp1, &in_buf_memory[((int32_t)4.0f)], in_buf_memorysize);
    process_write_ev(&_this->_mod_write_ev5, 0.0f, temp1, &in_buf_memory[((int32_t)5.0f)], in_buf_memorysize);
    process_write_ev(&_this->_mod_write_ev6, 0.0f, temp1, &in_buf_memory[((int32_t)6.0f)], in_buf_memorysize);
    process_write_ev(&_this->_mod_write_ev7, 0.0f, temp1, &in_buf_memory[((int32_t)7.0f)], in_buf_memorysize);
    process_write_ev(&_this->_mod_write_ev8, 0.0f, temp1, &in_buf_memory[((int32_t)8.0f)], in_buf_memorysize);
    process_write_ev(&_this->_mod_write_ev9, 0.7f, temp1, &in_buf_memory[((int32_t)9.0f)], in_buf_memorysize);
    process_write_ev(&_this->_mod_write_ev10, 2.1f, temp1, &in_buf_memory[((int32_t)10.0f)], in_buf_memorysize);
}
void process_device(mod_device* _this, float in_Audio_In_L, float in_Audio_In_R, float* out_L, float* out_R, float* out_sync, float* out_raw) {
    float temp = in_Audio_In_L;
    float temp1 = in_Audio_In_R;
    float temp2 = _this->array_vals[((int32_t)3.0f)];
    float temp3 = _this->array_vals[((int32_t)1.0f)];
    float temp4 = _this->array_vals[((int32_t)2.0f)];
    float temp5 = _this->button_Freeze;
    _this->button_Freeze = _this->button_Freeze_nextvalue;
    float temp6 = _this->variable_var;
    float t_out;
    uint8_t t_out_ev;
    process_latch_ev(&_this->_mod_latch_ev, (_this->slider_Speed * (1.0f - temp5)), ((uint8_t)(temp6 != 0)), &t_out, &t_out_ev);
    float temp7 = _this->button_Restart_Trg;
    _this->button_Restart_Trg = _this->button_Restart_Trg_nextvalue;
    float t_out1;
    uint8_t t_out_ev1;
    process_latch_ev(&_this->_mod_latch_ev1, ((float)(((uint8_t)(temp7 != 0)) ? 1.0f : 0.0f)), ((uint8_t)(temp6 != 0)), &t_out1, &t_out_ev1);
    float t_ph;
    process_Counter_BiDir(&_this->_mod_Counter_BiDir, t_out, ((uint8_t)(t_out1 != 0)), _this->table_unnamed, _this->table_unnamed_size, &t_ph);
    float temp8 = _this->array_vals[((int32_t)0.0f)];
    float t_Out;
    float t_idx;
    uint8_t t_1_;
    uint8_t t_trg;
    float t_pos;
    process_Single_Voice(&_this->_mod_Single_Voice, ((temp3 + temp4) + _this->slider_Pitch), (0.0f + _this->slider_X), t_ph, _this->slider_Frmnts, _this->table_unnamed, _this->table_unnamed_size, temp8, 2048.0f, _this->slider_Hyp_Shp, _this->slider_Hyp_Bnd, &t_Out, &t_idx, &t_1_, &t_trg, &t_pos);
    float temp9 = _this->array_vals[((int32_t)4.0f)];
    float t_F;
    process_P2F(&_this->_mod_P2F, temp9, &t_F);
    float t_out2;
    process_Max(&_this->_mod_Max, t_F, 0.01f, &t_out2);
    float t_out3;
    process_Clip_Min(&_this->_mod_Clip_Min, 0.0f, t_out2, &t_out3);
    float temp10 = (t_out3 * (3.14159f / SR));
    float temp11 = ((temp10 > 1.50845f) ? 1.50845f : temp10);
    float temp12 = ((((((temp11 * -0.0896638f) + 0.0388452f) * temp11) + 1.00005f) * temp11) / ((((temp11 * -0.430871f) + 0.0404318f) * temp11) + 1.0f));
    float temp13 = (temp12 * 1.0f);
    float temp14 = (temp12 * temp13);
    float temp15 = _this->array_vals[((int32_t)5.0f)];
    float temp16 = (1.0f - ((temp15 > 1.8f) ? 1.8f : temp15));
    float temp17 = (temp16 + temp16);
    float temp18 = (temp13 * temp17);
    float temp19 = ((temp14 + temp18) * -1.0f);
    float temp20 = _this->variable_var1;
    float temp21 = _this->variable_var2;
    float temp22 = (temp21 + (0.0f * temp12));
    float temp23 = (temp20 + (temp22 * temp12));
    float temp24 = (temp22 * temp17);
    float temp25 = ((temp23 + temp24) * -1.0f);
    float temp26 = (1.0f / (1.0f - temp19));
    float temp27 = (t_Out + (((t_Out * temp19) + temp25) * temp26));
    float temp28 = (-3.0f);
    float temp29 = 3.0f;
    float temp30 = temp27;
    float temp31 = MAX(temp28, MIN(temp29, temp30));
    float temp32 = ((temp31 * (27.0f + (temp31 * temp31))) / (27.0f + ((9.0f * temp31) * temp31)));
    float temp33 = ((temp2 * temp2) * temp32);
    float temp34 = _this->array_vals[((int32_t)6.0f)];
    float temp35 = (temp34 * temp34);
    float temp36 = _this->array_vals[((int32_t)7.0f)];
    float temp37 = (temp32 * (temp36 * temp36));
    float temp38 = _this->array_vals[((int32_t)8.0f)];
    float temp39 = (temp38 * temp38);
    float temp40 = _this->array_vals[((int32_t)9.0f)];
    float temp41 = _this->array_vals[((int32_t)10.0f)];
    float t_L;
    float t_R;
    process_8_VoiceChorusWet(&_this->_mod_8_VoiceChorusWet, (temp37 + (temp * temp39)), (temp37 + (temp1 * temp39)), temp40, temp41, &t_L, &t_R);
    float t_idx1;
    process_Counter1(&_this->_mod_Counter1, 2.0f, 100.0f, &t_idx1);
    float temp42 = (t_idx1 - 1.0f);
    float temp43 = (t_idx1 * 5.5f);
    int32_t temp44 = 11;
    float temp45 = _this->button_inc;
    _this->button_inc = _this->button_inc_nextvalue;
    uint8_t temp46 = _this->button_inc_event;
    _this->button_inc_event = _this->button_inc_nextevent;
    uint8_t temp47 = temp46;
    _this->button_inc_nextevent = 0;
    float temp48 = _this->button_dec;
    _this->button_dec = _this->button_dec_nextvalue;
    uint8_t temp49 = _this->button_dec_event;
    _this->button_dec_event = _this->button_dec_nextevent;
    uint8_t temp50 = temp49;
    _this->button_dec_nextevent = 0;
    float temp51 = _this->button_Enc_Push;
    _this->button_Enc_Push = _this->button_Enc_Push_nextvalue;
    uint8_t temp52 = _this->button_Enc_Push_event;
    _this->button_Enc_Push_event = _this->button_Enc_Push_nextevent;
    uint8_t temp53 = temp52;
    _this->button_Enc_Push_nextevent = 0;
    float t_idx2;
    process_EncoderControl(&_this->_mod_EncoderControl, (temp45 + temp48), (((float)(temp47 ? 1.0f : 0.0f)) + ((float)(temp50 ? 1.0f : 0.0f))), temp51, ((float)(temp53 ? 1.0f : 0.0f)), _this->table_step, 11, _this->array_vals, 11, _this->table_unnamed1, 22, &t_idx2);
    float temp54 = (t_idx1 + -1.0f);
    float temp55 = abs(temp54);
    float temp56 = _this->slider_YOffs2;
    float temp57 = _this->slider_YOffs3;
    float t_sample;
    process_Sample_Draw(&_this->_mod_Sample_Draw, _this->table_unnamed, _this->table_unnamed_size, t_idx1, &t_sample);
    float temp58 = ((t_pos < 0.0f) ? (t_pos + 1.0f) : t_pos);
    float temp59 = ((((temp58 >= 1.0f) ? (temp58 - 1.0f) : temp58) * 2.0f) + -1.0f);
    _this->display_gl_scope_value_x1[g_sampleIndex] = ((2.0f * t_idx) - 1.0f);
    _this->display_gl_scope_value_y1[g_sampleIndex] = ((temp32 * _this->slider_YSize) + _this->slider_YOffs);
    _this->display_gl_scope_value_x2[g_sampleIndex] = temp42;
    _this->display_gl_scope_value_y2[g_sampleIndex] = ((((temp43 > 10.0f) ? (temp41 / 7.0f) : ((temp43 > 9.0f) ? (temp40 * 2.0f) : ((temp43 > 8.0f) ? temp38 : ((temp43 > 7.0f) ? temp36 : ((temp43 > 6.0f) ? (temp34 / 1.0f) : ((temp43 > 5.0f) ? (temp15 / 1.0f) : ((temp43 > 4.0f) ? (temp9 / 240.0f) : ((temp43 > 3.0f) ? (temp2 / 6.0f) : ((temp43 > 2.0f) ? (0.5f + (temp4 / 2.0f)) : ((temp43 > 1.0f) ? (0.5f + (temp3 / 48.0f)) : temp8)))))))))) * _this->slider_YSize1) + _this->slider_YOffs1);
    _this->display_gl_scope_value_x3[g_sampleIndex] = (((t_idx1 + -1.0f) / ((float)temp44)) + ((((t_idx2 / ((float)temp44)) * 2.0f) + -1.0f) + (1.0f / ((float)temp44))));
    _this->display_gl_scope_value_y3[g_sampleIndex] = (((-0.12f * temp55) + temp56) + _this->slider_Y3offs);
    _this->display_gl_scope_value_y4[g_sampleIndex] = (((-0.05f * temp55) + temp56) + _this->slider_Y4offs);
    _this->display_gl_scope_value_a[g_sampleIndex] = 1.0f;
    _this->display_gl_scope_value_x5[g_sampleIndex] = temp42;
    _this->display_gl_scope_value_y5[g_sampleIndex] = (temp57 + (_this->slider_YSize2 * t_sample));
    _this->display_gl_scope_value_x6[g_sampleIndex] = temp59;
    _this->display_gl_scope_value_y6[g_sampleIndex] = (temp57 + (_this->slider_YSize3 * temp54));
    _this->label_unnamed = temp8;
    _this->label_unnamed1 = temp3;
    _this->label_unnamed2 = temp4;
    _this->label_unnamed3 = temp36;
    _this->label_unnamed4 = temp38;
    _this->label_unnamed5 = temp40;
    _this->label_unnamed6 = temp41;
    _this->label_unnamed7 = temp2;
    _this->label_unnamed8 = temp34;
    _this->variable_var = ((float)(t_1_ ? 1.0f : 0.0f));
    _this->label_menu_idx = t_idx2;
    _this->label_unnamed9 = temp59;
    process_Init_Default_vals(&_this->_mod_Init_Default_vals, _this->array_vals, 11);
    _this->label_unnamed10 = temp9;
    _this->label_unnamed11 = temp15;
    float temp60 = (temp27 * temp12);
    float temp61 = (temp60 + temp21);
    float temp62 = (temp60 + temp61);
    _this->variable_var2 = temp62;
    float temp63 = (temp61 * temp12);
    float temp64 = (temp63 + temp20);
    float temp65 = (temp63 + temp64);
    _this->variable_var1 = temp65;
    *out_L = ((temp33 + (temp35 * temp)) + t_L);
    *out_R = ((temp33 + (temp35 * temp1)) + t_R);
    *out_sync = 0.0f;
    *out_raw = 0.0f;
}
VoiceInfo* get_voice_info(mod_device* dev) {
    return 0;
}
Controller* get_controller(mod_device* dev, uint8_t channel) {
    return 0;
}
typedef enum {
    PARAMETER_INVALID,
    PARAMETER_ISDOUBLEFP,
    PARAMETER_SLIDER_PITCH,
    PARAMETER_SLIDER_X,
    PARAMETER_SLIDER_SPEED,
    PARAMETER_BUTTON_FREEZE,
    PARAMETER_BUTTON_FREEZE_NEXTVALUE,
    PARAMETER_BUTTON_RESTART_TRG,
    PARAMETER_BUTTON_RESTART_TRG_NEXTVALUE,
    PARAMETER_SLIDER_FRMNTS,
    PARAMETER_SLIDER_HYP_SHP,
    PARAMETER_SLIDER_HYP_BND,
    PARAMETER_SLIDER_YSIZE,
    PARAMETER_SLIDER_YOFFS,
    PARAMETER_SLIDER_YSIZE1,
    PARAMETER_SLIDER_YOFFS1,
    PARAMETER_BUTTON_INC,
    PARAMETER_BUTTON_INC_NEXTVALUE,
    PARAMETER_BUTTON_INC_EVENT,
    PARAMETER_BUTTON_INC_NEXTEVENT,
    PARAMETER_BUTTON_DEC,
    PARAMETER_BUTTON_DEC_NEXTVALUE,
    PARAMETER_BUTTON_DEC_EVENT,
    PARAMETER_BUTTON_DEC_NEXTEVENT,
    PARAMETER_BUTTON_ENC_PUSH,
    PARAMETER_BUTTON_ENC_PUSH_NEXTVALUE,
    PARAMETER_BUTTON_ENC_PUSH_EVENT,
    PARAMETER_BUTTON_ENC_PUSH_NEXTEVENT,
    PARAMETER_SLIDER_YOFFS2,
    PARAMETER_SLIDER_Y3OFFS,
    PARAMETER_SLIDER_Y4OFFS,
    PARAMETER_SLIDER_YOFFS3,
    PARAMETER_SLIDER_YSIZE2,
    PARAMETER_SLIDER_YSIZE3,
    PARAMETER_DISPLAY_GL_SCOPE_VALUE_X1,
    PARAMETER_DISPLAY_GL_SCOPE_VALUE_Y1,
    PARAMETER_DISPLAY_GL_SCOPE_VALUE_X2,
    PARAMETER_DISPLAY_GL_SCOPE_VALUE_Y2,
    PARAMETER_DISPLAY_GL_SCOPE_VALUE_X3,
    PARAMETER_DISPLAY_GL_SCOPE_VALUE_Y3,
    PARAMETER_DISPLAY_GL_SCOPE_VALUE_Y4,
    PARAMETER_DISPLAY_GL_SCOPE_VALUE_A,
    PARAMETER_DISPLAY_GL_SCOPE_VALUE_X5,
    PARAMETER_DISPLAY_GL_SCOPE_VALUE_Y5,
    PARAMETER_DISPLAY_GL_SCOPE_VALUE_X6,
    PARAMETER_DISPLAY_GL_SCOPE_VALUE_Y6,
    PARAMETER_LABEL_UNNAMED,
    PARAMETER_LABEL_UNNAMED1,
    PARAMETER_LABEL_UNNAMED2,
    PARAMETER_LABEL_UNNAMED3,
    PARAMETER_LABEL_UNNAMED4,
    PARAMETER_LABEL_UNNAMED5,
    PARAMETER_LABEL_UNNAMED6,
    PARAMETER_LABEL_UNNAMED7,
    PARAMETER_LABEL_UNNAMED8,
    PARAMETER_LABEL_MENU_IDX,
    PARAMETER_LABEL_UNNAMED9,
    PARAMETER_LABEL_UNNAMED10,
    PARAMETER_LABEL_UNNAMED11,
    NUM_PARAMETERS
} PARAMETER;
double* get_double_ptr(mod_device* _this, PARAMETER parameter) {
    return 0;
}
float* get_float_ptr(mod_device* _this, PARAMETER parameter) {
    float* pointers[] = {
        0,
        0,
        &_this->slider_Pitch,
        &_this->slider_X,
        &_this->slider_Speed,
        &_this->button_Freeze,
        &_this->button_Freeze_nextvalue,
        &_this->button_Restart_Trg,
        &_this->button_Restart_Trg_nextvalue,
        &_this->slider_Frmnts,
        &_this->slider_Hyp_Shp,
        &_this->slider_Hyp_Bnd,
        &_this->slider_YSize,
        &_this->slider_YOffs,
        &_this->slider_YSize1,
        &_this->slider_YOffs1,
        &_this->button_inc,
        &_this->button_inc_nextvalue,
        0,
        0,
        &_this->button_dec,
        &_this->button_dec_nextvalue,
        0,
        0,
        &_this->button_Enc_Push,
        &_this->button_Enc_Push_nextvalue,
        0,
        0,
        &_this->slider_YOffs2,
        &_this->slider_Y3offs,
        &_this->slider_Y4offs,
        &_this->slider_YOffs3,
        &_this->slider_YSize2,
        &_this->slider_YSize3,
        _this->display_gl_scope_value_x1,
        _this->display_gl_scope_value_y1,
        _this->display_gl_scope_value_x2,
        _this->display_gl_scope_value_y2,
        _this->display_gl_scope_value_x3,
        _this->display_gl_scope_value_y3,
        _this->display_gl_scope_value_y4,
        _this->display_gl_scope_value_a,
        _this->display_gl_scope_value_x5,
        _this->display_gl_scope_value_y5,
        _this->display_gl_scope_value_x6,
        _this->display_gl_scope_value_y6,
        &_this->label_unnamed,
        &_this->label_unnamed1,
        &_this->label_unnamed2,
        &_this->label_unnamed3,
        &_this->label_unnamed4,
        &_this->label_unnamed5,
        &_this->label_unnamed6,
        &_this->label_unnamed7,
        &_this->label_unnamed8,
        &_this->label_menu_idx,
        &_this->label_unnamed9,
        &_this->label_unnamed10,
        &_this->label_unnamed11,
    };
    if (parameter > PARAMETER_INVALID && parameter < NUM_PARAMETERS) { return pointers[parameter]; }
    return 0;
}
uint8_t* get_bool_ptr(mod_device* _this, PARAMETER parameter) {
    uint8_t* pointers[] = {
        0,
        &_this->isDoubleFp,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        &_this->button_inc_event,
        &_this->button_inc_nextevent,
        0,
        0,
        &_this->button_dec_event,
        &_this->button_dec_nextevent,
        0,
        0,
        &_this->button_Enc_Push_event,
        &_this->button_Enc_Push_nextevent,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
    };
    if (parameter > PARAMETER_INVALID && parameter < NUM_PARAMETERS) { return pointers[parameter]; }
    return 0;
}
typedef enum {
    SLIDER_PITCH,
    SLIDER_SPEED,
    SLIDER_X,
    SLIDER_FRMNTS,
    SLIDER_Y3OFFS,
    SLIDER_Y4OFFS,
    SLIDER_YOFFS,
    SLIDER_YSIZE,
    SLIDER_YOFFS1,
    SLIDER_YSIZE1,
    SLIDER_YOFFS2,
    SLIDER_YOFFS3,
    SLIDER_YSIZE2,
    SLIDER_YSIZE3,
    SLIDER_HYP_SHP,
    SLIDER_HYP_BND,
    NUM_SLIDERS
} SLIDER;
/** @parameter normValue The slider value in the range [0, 1] to set. */
void set_slider_value(mod_device* device, SLIDER slider, float normValue) {
    switch (slider) {
        case SLIDER_PITCH:
            device->slider_Pitch = normValue * 60.0f  + 24.0f;
            return;
        case SLIDER_SPEED:
            device->slider_Speed = normValue * 4.0f  + (-2.0f);
            return;
        case SLIDER_X:
            device->slider_X = normValue  ;
            return;
        case SLIDER_FRMNTS:
            device->slider_Frmnts = normValue * 2.0f  + (-1.0f);
            return;
        case SLIDER_Y3OFFS:
            device->slider_Y3offs = normValue * 2.0f  + (-1.0f);
            return;
        case SLIDER_Y4OFFS:
            device->slider_Y4offs = normValue * 2.0f  + (-1.0f);
            return;
        case SLIDER_YOFFS:
            device->slider_YOffs = normValue * 2.0f  + (-1.0f);
            return;
        case SLIDER_YSIZE:
            device->slider_YSize = normValue  ;
            return;
        case SLIDER_YOFFS1:
            device->slider_YOffs1 = normValue * 2.0f  + (-1.0f);
            return;
        case SLIDER_YSIZE1:
            device->slider_YSize1 = normValue  ;
            return;
        case SLIDER_YOFFS2:
            device->slider_YOffs2 = normValue * 2.0f  + (-1.0f);
            return;
        case SLIDER_YOFFS3:
            device->slider_YOffs3 = normValue * 2.0f  + (-1.0f);
            return;
        case SLIDER_YSIZE2:
            device->slider_YSize2 = normValue  ;
            return;
        case SLIDER_YSIZE3:
            device->slider_YSize3 = normValue  ;
            return;
        case SLIDER_HYP_SHP:
            device->slider_Hyp_Shp = normValue * 2.0f  + (-1.0f);
            return;
        case SLIDER_HYP_BND:
            device->slider_Hyp_Bnd = normValue  ;
            return;
        case NUM_SLIDERS: break;
    }
}
float get_slider_value(mod_device* device, SLIDER slider) {
    switch (slider) {
        case SLIDER_PITCH: return (device->slider_Pitch - 24.0f) / 60.0f;
        case SLIDER_SPEED: return (device->slider_Speed - (-2.0f)) / 4.0f;
        case SLIDER_X: return device->slider_X;
        case SLIDER_FRMNTS: return (device->slider_Frmnts - (-1.0f)) / 2.0f;
        case SLIDER_Y3OFFS: return (device->slider_Y3offs - (-1.0f)) / 2.0f;
        case SLIDER_Y4OFFS: return (device->slider_Y4offs - (-1.0f)) / 2.0f;
        case SLIDER_YOFFS: return (device->slider_YOffs - (-1.0f)) / 2.0f;
        case SLIDER_YSIZE: return device->slider_YSize;
        case SLIDER_YOFFS1: return (device->slider_YOffs1 - (-1.0f)) / 2.0f;
        case SLIDER_YSIZE1: return device->slider_YSize1;
        case SLIDER_YOFFS2: return (device->slider_YOffs2 - (-1.0f)) / 2.0f;
        case SLIDER_YOFFS3: return (device->slider_YOffs3 - (-1.0f)) / 2.0f;
        case SLIDER_YSIZE2: return device->slider_YSize2;
        case SLIDER_YSIZE3: return device->slider_YSize3;
        case SLIDER_HYP_SHP: return (device->slider_Hyp_Shp - (-1.0f)) / 2.0f;
        case SLIDER_HYP_BND: return device->slider_Hyp_Bnd;
        case NUM_SLIDERS: break;
    }
    return 0.0f;
}
typedef enum {
    BUTTON_ENC_,
    BUTTON_ENC_PUSH,
    BUTTON_DEC,
    BUTTON_INC,
    BUTTON_RESTART_TRG,
    BUTTON_FREEZE,
    NUM_BUTTONS
} BUTTON;
/** @parameter state The button state, either being pressed=1, or released=0. */
void set_button_value(mod_device* device, BUTTON button, uint8_t state) {
    switch (button) {
        case BUTTON_ENC_PUSH:
            device->button_Enc_Push = state ? 1.0f : 0.0f;
            device->button_Enc_Push_nextvalue = state ? 1.0f : 0.0f;
            device->button_Enc_Push_event = 1;
            device->button_Enc_Push_nextevent = 0;
            return;
        case BUTTON_DEC:
            device->button_dec = state ? (-1.0f) : 0.0f;
            device->button_dec_nextvalue = state ? (-1.0f) : 0.0f;
            device->button_dec_event = 1;
            device->button_dec_nextevent = 0;
            return;
        case BUTTON_INC:
            device->button_inc = state ? 1.0f : 0.0f;
            device->button_inc_nextvalue = state ? 1.0f : 0.0f;
            device->button_inc_event = 1;
            device->button_inc_nextevent = 0;
            return;
        case BUTTON_RESTART_TRG:
            device->button_Restart_Trg = state ? 1.0f : 0.0f;
            device->button_Restart_Trg_nextvalue = state ? 1.0f : 0.0f;
            return;
        case BUTTON_FREEZE:
            device->button_Freeze = state ? 1.0f : 0.0f;
            device->button_Freeze_nextvalue = state ? 1.0f : 0.0f;
            return;
        case NUM_BUTTONS: break;
    }
}
uint8_t get_button_value(mod_device* device, BUTTON button) {
    switch (button) {
        case BUTTON_ENC_PUSH: return device->button_Enc_Push == 1.0f ? 1 : 0;
        case BUTTON_DEC: return device->button_dec == (-1.0f) ? 1 : 0;
        case BUTTON_INC: return device->button_inc == 1.0f ? 1 : 0;
        case BUTTON_RESTART_TRG: return device->button_Restart_Trg == 1.0f ? 1 : 0;
        case BUTTON_FREEZE: return device->button_Freeze == 1.0f ? 1 : 0;
        case NUM_BUTTONS: break;
    }
    return 0;
}
typedef enum {
    LABEL_UNNAMED,
    LABEL_UNNAMED1,
    LABEL_UNNAMED2,
    LABEL_UNNAMED3,
    LABEL_UNNAMED4,
    LABEL_UNNAMED5,
    LABEL_UNNAMED6,
    LABEL_UNNAMED7,
    LABEL_UNNAMED8,
    LABEL_MENU_IDX,
    LABEL_UNNAMED9,
    LABEL_UNNAMED10,
    LABEL_UNNAMED11,
    NUM_LABELS
} LABEL;
float get_label_value(mod_device* device, LABEL label) {
    switch (label) {
        case LABEL_UNNAMED: return device->label_unnamed;
        case LABEL_UNNAMED1: return device->label_unnamed1;
        case LABEL_UNNAMED2: return device->label_unnamed2;
        case LABEL_UNNAMED3: return device->label_unnamed3;
        case LABEL_UNNAMED4: return device->label_unnamed4;
        case LABEL_UNNAMED5: return device->label_unnamed5;
        case LABEL_UNNAMED6: return device->label_unnamed6;
        case LABEL_UNNAMED7: return device->label_unnamed7;
        case LABEL_UNNAMED8: return device->label_unnamed8;
        case LABEL_MENU_IDX: return device->label_menu_idx;
        case LABEL_UNNAMED9: return device->label_unnamed9;
        case LABEL_UNNAMED10: return device->label_unnamed10;
        case LABEL_UNNAMED11: return device->label_unnamed11;
        case NUM_LABELS: break;
    }
    return 0.0f;
}
typedef enum {
    DISPLAY_GL_SCOPE,
    NUM_DISPLAYS
} DISPLAY;
float get_display_value(mod_device* device, DISPLAY display, uint8_t portIdx) {
    switch (display) {
        case DISPLAY_GL_SCOPE:
            switch (portIdx) {
                case 0: return device->display_gl_scope_value_x1[0];
                case 1: return device->display_gl_scope_value_y1[0];
                case 2: return device->display_gl_scope_value_x2[0];
                case 3: return device->display_gl_scope_value_y2[0];
                case 4: return device->display_gl_scope_value_x3[0];
                case 5: return device->display_gl_scope_value_y3[0];
                case 6: return device->display_gl_scope_value_y4[0];
                case 7: return device->display_gl_scope_value_a[0];
                case 8: return device->display_gl_scope_value_x5[0];
                case 9: return device->display_gl_scope_value_y5[0];
                case 10: return device->display_gl_scope_value_x6[0];
                case 11: return device->display_gl_scope_value_y6[0];
            }
            break;
        case NUM_DISPLAYS: break;
    }
    return 0;
}
void set_display_value(mod_device* device, DISPLAY display, uint8_t portIdx, float value) {
    switch (display) {
        case DISPLAY_GL_SCOPE:
            switch (portIdx) {
                    case 0: for (size_t i = 0; i < 48; i++) { device->display_gl_scope_value_x1[i] = value; }; return;
                    case 1: for (size_t i = 0; i < 48; i++) { device->display_gl_scope_value_y1[i] = value; }; return;
                    case 2: for (size_t i = 0; i < 48; i++) { device->display_gl_scope_value_x2[i] = value; }; return;
                    case 3: for (size_t i = 0; i < 48; i++) { device->display_gl_scope_value_y2[i] = value; }; return;
                    case 4: for (size_t i = 0; i < 48; i++) { device->display_gl_scope_value_x3[i] = value; }; return;
                    case 5: for (size_t i = 0; i < 48; i++) { device->display_gl_scope_value_y3[i] = value; }; return;
                    case 6: for (size_t i = 0; i < 48; i++) { device->display_gl_scope_value_y4[i] = value; }; return;
                    case 7: for (size_t i = 0; i < 48; i++) { device->display_gl_scope_value_a[i] = value; }; return;
                    case 8: for (size_t i = 0; i < 48; i++) { device->display_gl_scope_value_x5[i] = value; }; return;
                    case 9: for (size_t i = 0; i < 48; i++) { device->display_gl_scope_value_y5[i] = value; }; return;
                    case 10: for (size_t i = 0; i < 48; i++) { device->display_gl_scope_value_x6[i] = value; }; return;
                    case 11: for (size_t i = 0; i < 48; i++) { device->display_gl_scope_value_y6[i] = value; }; return;
            }
            break;
        case NUM_DISPLAYS: break;
    }
}
typedef struct {
} mod_vertex_shader;
void process_vertex_shader(float in_x1, float in_y1, float in_x2, float in_y2, float in_x3, float in_y3, float in_y4, float in_a, float in_vertexID, float in_x5, float in_y5, float in_x6, float in_y6, float* out_x, float* out_y, float* out_z, float* out_w, float* out_a) {
    float temp = fmod(in_vertexID, 6.0f);
    uint8_t temp1 = (temp > 0.0f);
    uint8_t temp2 = (temp > 1.0f);
    uint8_t temp3 = (temp > 3.0f);
    uint8_t temp4 = (temp > 4.0f);
    *out_x = (temp4 ? in_x6 : (temp3 ? in_x5 : (temp2 ? in_x3 : (temp1 ? in_x2 : in_x1))));
    *out_y = (temp4 ? in_y6 : (temp3 ? in_y5 : ((temp > 2.0f) ? in_y4 : (temp2 ? in_y3 : (temp1 ? in_y2 : in_y1)))));
    *out_z = 0.0f;
    *out_w = 1.0f;
    *out_a = in_a;
}
typedef struct {
} mod_fragment_shader;
void process_fragment_shader(float in_x, float in_y, float in_z, float in_w, float in_a, float* out_r, float* out_g, float* out_b, float* out_a) {
    *out_r = 0.0f;
    *out_g = 1.0f;
    *out_b = 0.0f;
    *out_a = 1.0f;
}
void draw_point(DISPLAY display, float x, float y, float z, float w, float r, float g, float b, float a, float size);
void process_display(mod_device* device, DISPLAY display, uint32_t bufferSize) {
    if (display == DISPLAY_GL_SCOPE) {
        float w = 1, size = 1, x = 0, y = 0, z = 0, r = 0, g = 0, b = 0, a = 0, _a = 0;
        for (int vertexID = 0; vertexID < bufferSize; vertexID++) {
            process_vertex_shader(
                device->display_gl_scope_value_x1[vertexID],
                device->display_gl_scope_value_y1[vertexID],
                device->display_gl_scope_value_x2[vertexID],
                device->display_gl_scope_value_y2[vertexID],
                device->display_gl_scope_value_x3[vertexID],
                device->display_gl_scope_value_y3[vertexID],
                device->display_gl_scope_value_y4[vertexID],
                device->display_gl_scope_value_a[vertexID],
                vertexID,
                device->display_gl_scope_value_x5[vertexID],
                device->display_gl_scope_value_y5[vertexID],
                device->display_gl_scope_value_x6[vertexID],
                device->display_gl_scope_value_y6[vertexID],
                &x,
                &y,
                &z,
                &w,
                &_a
            );
            process_fragment_shader(
                x,
                y,
                z,
                w,
                _a,
                &r,
                &g,
                &b,
                &a
            );
            draw_point(display, x, y, z, w, r, g, b, a, size);
        }
    }
}
void set_buffer_size(mod_device* device, uint32_t bufferSize) {
    if (device->bufferSize != bufferSize) {
        device->bufferSize = bufferSize;
    }
}
void set_sample_rate(mod_device* device, uint32_t sampleRate) {
    device->sampleRate = sampleRate;
}
void array_process_device(mod_device* device, const float* const* inputs, size_t numInputs, float** outputs, size_t numOutputs, uint32_t bufferSize) {
    float empty;
    for (uint32_t b = 0; b < 48; b++) {
        g_sampleIndex = b;
        process_device(device, 0.0f, 0.0f, &empty, &empty, &empty, &empty);
    }
}
mod_device* create(void) {
    static mod_device mod;
    size_t size = sizeof(mod_device);
    memset(&mod, 0, size);
    mod.isDoubleFp = 0;
    mod._mod_8_VoiceChorusWet._mod_8VoiceChorusCore._mod_MultiTapDelay.array_unnamed = (float*) dspstudio_malloc(48000 * sizeof(float));
    memset(mod._mod_8_VoiceChorusWet._mod_8VoiceChorusCore._mod_MultiTapDelay.array_unnamed, 0, 48000 * sizeof(float));
    mod._mod_8_VoiceChorusWet._mod_8VoiceChorusCore._mod_MultiTapDelay.array_unnamed1 = (float*) dspstudio_malloc(48000 * sizeof(float));
    memset(mod._mod_8_VoiceChorusWet._mod_8VoiceChorusCore._mod_MultiTapDelay.array_unnamed1, 0, 48000 * sizeof(float));
    mod._mod_8_VoiceChorusWet._mod_8VoiceChorusCore._mod_MultiTapDelay.array_unnamed2 = (float*) dspstudio_malloc(48000 * sizeof(float));
    memset(mod._mod_8_VoiceChorusWet._mod_8VoiceChorusCore._mod_MultiTapDelay.array_unnamed2, 0, 48000 * sizeof(float));
    mod._mod_8_VoiceChorusWet._mod_8VoiceChorusCore._mod_MultiTapDelay.array_unnamed3 = (float*) dspstudio_malloc(48000 * sizeof(float));
    memset(mod._mod_8_VoiceChorusWet._mod_8VoiceChorusCore._mod_MultiTapDelay.array_unnamed3, 0, 48000 * sizeof(float));
    mod._mod_8_VoiceChorusWet._mod_8VoiceChorusCore._mod_MultiTapDelay.array_unnamed4 = (float*) dspstudio_malloc(48000 * sizeof(float));
    memset(mod._mod_8_VoiceChorusWet._mod_8VoiceChorusCore._mod_MultiTapDelay.array_unnamed4, 0, 48000 * sizeof(float));
    mod._mod_8_VoiceChorusWet._mod_8VoiceChorusCore._mod_MultiTapDelay.array_unnamed5 = (float*) dspstudio_malloc(48000 * sizeof(float));
    memset(mod._mod_8_VoiceChorusWet._mod_8VoiceChorusCore._mod_MultiTapDelay.array_unnamed5, 0, 48000 * sizeof(float));
    mod._mod_8_VoiceChorusWet._mod_8VoiceChorusCore._mod_MultiTapDelay.array_unnamed6 = (float*) dspstudio_malloc(48000 * sizeof(float));
    memset(mod._mod_8_VoiceChorusWet._mod_8VoiceChorusCore._mod_MultiTapDelay.array_unnamed6, 0, 48000 * sizeof(float));
    mod._mod_8_VoiceChorusWet._mod_8VoiceChorusCore._mod_MultiTapDelay.array_unnamed7 = (float*) dspstudio_malloc(48000 * sizeof(float));
    memset(mod._mod_8_VoiceChorusWet._mod_8VoiceChorusCore._mod_MultiTapDelay.array_unnamed7, 0, 48000 * sizeof(float));
    size_t t_table_size;
    float* t_table_data;
    dspstudio_load_table("Granuxe2_clip_compr.wav", &t_table_data, &t_table_size);
    float* t_table_unnamed = (float*) dspstudio_malloc(t_table_size * sizeof(float));
    mod.table_unnamed = t_table_unnamed;
    for (size_t i = 0; i < t_table_size; i++) t_table_unnamed[i] = t_table_data[i];
    mod.table_unnamed_size = t_table_size;
    float* t_table_step = mod.table_step;
    for (size_t i = 0; i < 11; i++) t_table_step[i] = t_g_table_0_5964404961324983_0[i];
    float* t_table_unnamed1 = mod.table_unnamed1;
    for (size_t i = 0; i < 22; i++) t_table_unnamed1[i] = t_g_table_0_32365927376408377_0[i];
    mod.slider_Pitch = 29.0f;
    mod.slider_Speed = 0.7000000000000002f;
    mod.slider_X = 0.0f;
    mod.slider_Frmnts = 0.19999999999999998f;
    mod.slider_Y3offs = (-0.584f);
    mod.slider_Y4offs = (-0.6639999999999999f);
    mod.slider_YOffs = (-0.528f);
    mod.slider_YSize = 0.36f;
    mod.slider_YOffs1 = (-0.008000000000000014f);
    mod.slider_YSize1 = 0.28f;
    mod.slider_YOffs2 = 0.52f;
    mod.slider_YOffs3 = 0.6799999999999998f;
    mod.slider_YSize2 = 0.336f;
    mod.slider_YSize3 = 0.18000000000000002f;
    mod.slider_Hyp_Shp = (-0.11199999999999992f);
    mod.slider_Hyp_Bnd = 0.4720000000000001f;
    mod.button_Enc_Push = 0.0f;
    mod.button_Enc_Push_nextvalue = 0.0f;
    mod.button_dec = 0.0f;
    mod.button_dec_nextvalue = 0.0f;
    mod.button_inc = 0.0f;
    mod.button_inc_nextvalue = 0.0f;
    mod.button_Restart_Trg = 0.0f;
    mod.button_Restart_Trg_nextvalue = 0.0f;
    mod.button_Freeze = 0.0f;
    mod.button_Freeze_nextvalue = 0.0f;
    return &mod;
}
void destroy(mod_device* device) { set_buffer_size(device, 0); }

#define _DSPSTUDIO_MAPPING_CONTROL_KNOB_1     SLIDER_X
#define _DSPSTUDIO_MAPPING_TYPE_KNOB_1        SLIDER
#define _DSPSTUDIO_MAPPING_CONTROL_KNOB_2     SLIDER_Y
#define _DSPSTUDIO_MAPPING_TYPE_KNOB_2        SLIDER

#define _DSPSTUDIO_MAPPING_CONTROL_GATE_IN    SLIDER_X
#define _DSPSTUDIO_MAPPING_TYPE_GATE_IN       SLIDER

#define _DSPSTUDIO_MAPPING_CONTROL_MIDI_IN_CHANNEL 2
#define _DSPSTUDIO_MAPPING_TYPE_MIDI_IN_CHANNEL 2

#define _DSPSTUDIO_MAPPING_CONTROL_LED_KNOB_1 LABEL_UNNAMED
#define _DSPSTUDIO_MAPPING_TYPE_LED_KNOB_1    LABEL
#define _DSPSTUDIO_MAPPING_CONTROL_LED_KNOB_2 LABEL_UNNAMED1
#define _DSPSTUDIO_MAPPING_TYPE_LED_KNOB_2    LABEL
#define _DSPSTUDIO_MAPPING_CONTROL_LED_KNOB_3 LABEL_UNNAMED2
#define _DSPSTUDIO_MAPPING_TYPE_LED_KNOB_3    LABEL
#define _DSPSTUDIO_MAPPING_CONTROL_LED_KNOB_4 LABEL_UNNAMED3
#define _DSPSTUDIO_MAPPING_TYPE_LED_KNOB_4    LABEL
#define _DSPSTUDIO_MAPPING_CONTROL_LED_KNOB_5 LABEL_UNNAMED4
#define _DSPSTUDIO_MAPPING_TYPE_LED_KNOB_5    LABEL
#define _DSPSTUDIO_MAPPING_CONTROL_LED_KNOB_6 LABEL_UNNAMED5
#define _DSPSTUDIO_MAPPING_TYPE_LED_KNOB_6    LABEL

#define _DSPSTUDIO_MAPPING_CONTROL_MIDI_OUT_CHANNEL 1
#define _DSPSTUDIO_MAPPING_TYPE_MIDI_OUT_CHANNEL 1

#define _DSPSTUDIO_MAPPING_CONTROL_DISPLAY    DISPLAY_GL_SCOPE
#define _DSPSTUDIO_MAPPING_TYPE_DISPLAY       DISPLAY
#define _DSPSTUDIO_MAPPING_CONTROL_ENC        BUTTON_ENC_
#define _DSPSTUDIO_MAPPING_TYPE_ENC           BUTTON     
#define _DSPSTUDIO_MAPPING_CONTROL_ENC_BUT    BUTTON_ENC_PUSH
#define _DSPSTUDIO_MAPPING_TYPE_ENC_BUT       BUTTON
