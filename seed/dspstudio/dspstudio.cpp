#include "daisysp.h"
#include "daisy_seed.h"
namespace dspstudio_device {
#include "dspstudio_generated.h"
}
#include "dspstudio_gluecode.h"
#include "dspstudio_util.h"
#include "usbd/UsbMidiHandler.h"

using namespace daisysp;
using namespace daisy;
using namespace wavloader;

static DaisySeed hw;
static UsbMidiHandler usbMidi;

using namespace daisy;
using namespace dspstudio_device;

dspstudio_device::mod_device* g_dev;

static void ProcessControls();

static void AudioCallback(const float* const* in, float** out, size_t size)
{
    ProcessControls();
    dspstudio_device::array_process_device(g_dev, in, 2, out, 2, size);
}

int main(void)
{
    InitializeUsbMappings();
    hw.Configure();
    hw.Init(cpuBoost);
    g_dev = dspstudio_device::create();
    InitializeAudioRates(hw);

    usbMidi.Init(UsbMidiHandler::FS_INTERNAL);
    usbMidi.StartReceive();

    hw.StartAudio(AudioCallback);

    for(;;)
    {
        usbMidi.Listen();
        while(usbMidi.HasEvents())
        {
            HandleMidiMessage(usbMidi.PopEvent());
        }
    }
}


static void ProcessControls() {

}