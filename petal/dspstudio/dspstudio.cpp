#include "daisysp.h"
#include "daisy_petal.h"
namespace dspstudio_device {
#include "dspstudio_generated.h"
}
#include "dspstudio_gluecode.h"
#include "dspstudio_util.h"
#include "usbd/UsbMidiHandler.h"

using namespace daisysp;
using namespace daisy;
using namespace wavloader;

static DaisyPetal hw;
static UsbMidiHandler usbMidi;

using namespace daisy;
using namespace dspstudio_device;

dspstudio_device::mod_device* g_dev;

static void ProcessControls();

static void AudioCallback(const float* const* in, float** out, size_t size)
{
    ProcessControls();
    dspstudio_device::array_process_device(g_dev, in, 2, out, 2, size);
}
int main(void)
{
    InitializeUsbMappings();
    hw.Init(cpuBoost);

    g_dev = dspstudio_device::create();
    InitializeAudioRates(hw.seed);

    usbMidi.Init(UsbMidiHandler::FS_INTERNAL);
    usbMidi.StartReceive();

    hw.StartAdc();
    hw.StartAudio(AudioCallback);

    uint32_t last = 0;
    for(;;)
    {

        usbMidi.Listen();
        while(usbMidi.HasEvents())
        {
            HandleMidiMessage(usbMidi.PopEvent());
        }

        uint32_t now = System::GetUs();
        if (now - last > 30000) {
            hw.UpdateLeds();
            last = now;
        }
    }
}


static void ProcessControls() {

    hw.ProcessAnalogControls();
    hw.ProcessDigitalControls();

    // Sensors: Knob 1-6
    {
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_KNOB_1
            _DSPSTUDIO_MAPPING_TYPE_KNOB_1::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_KNOB_1, hw.knob[DaisyPetal::KNOB_1].Value(), true);
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_KNOB_2
            _DSPSTUDIO_MAPPING_TYPE_KNOB_2::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_KNOB_2, hw.knob[DaisyPetal::KNOB_2].Value(), true);
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_KNOB_3
            _DSPSTUDIO_MAPPING_TYPE_KNOB_3::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_KNOB_3, hw.knob[DaisyPetal::KNOB_3].Value(), true);
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_KNOB_4
            _DSPSTUDIO_MAPPING_TYPE_KNOB_4::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_KNOB_4, hw.knob[DaisyPetal::KNOB_4].Value(), true);
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_KNOB_5
            _DSPSTUDIO_MAPPING_TYPE_KNOB_5::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_KNOB_5, hw.knob[DaisyPetal::KNOB_5].Value(), true);
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_KNOB_6
            _DSPSTUDIO_MAPPING_TYPE_KNOB_6::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_KNOB_6, hw.knob[DaisyPetal::KNOB_6].Value(), true);
        #endif
    }

    // Sensor: Expression Pedal
    {
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_EXPRESSION
            _DSPSTUDIO_MAPPING_TYPE_EXPRESSION::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_EXPRESSION, hw.GetExpression(), true);
        #endif
    }

    // Sensor: Encoder Knob
    {
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_ENC
            _DSPSTUDIO_MAPPING_TYPE_ENC::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_ENC, hw.encoder.Increment() == 1, hw.encoder.Increment() != 0);
        #endif
    }

    // Sensor: Encoder Button
    {
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_ENC_BUT
            _DSPSTUDIO_MAPPING_TYPE_ENC_BUT::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_ENC_BUT, hw.encoder.Pressed() || hw.encoder.RisingEdge(), hw.encoder.FallingEdge() || hw.encoder.RisingEdge());
        #endif
    }

    // Sensor: Foot Switch 1-4
    {
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_FOOT_SWITCH_1
        {
            Switch& sw = hw.switches[DaisyPetal::SW_1];
            _DSPSTUDIO_MAPPING_TYPE_FOOT_SWITCH_1::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_FOOT_SWITCH_1, sw.Pressed() || sw.RisingEdge(), sw.FallingEdge() || sw.RisingEdge());
        }
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_FOOT_SWITCH_2
        {
            Switch& sw = hw.switches[DaisyPetal::SW_2];
            _DSPSTUDIO_MAPPING_TYPE_FOOT_SWITCH_2::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_FOOT_SWITCH_2, sw.Pressed() || sw.RisingEdge(), sw.FallingEdge() || sw.RisingEdge());
        }
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_FOOT_SWITCH_3
        {
            Switch& sw = hw.switches[DaisyPetal::SW_3];
            _DSPSTUDIO_MAPPING_TYPE_FOOT_SWITCH_3::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_FOOT_SWITCH_3, sw.Pressed() || sw.RisingEdge(), sw.FallingEdge() || sw.RisingEdge());
        }
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_FOOT_SWITCH_4
        {
            Switch& sw = hw.switches[DaisyPetal::SW_4];
            _DSPSTUDIO_MAPPING_TYPE_FOOT_SWITCH_4::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_FOOT_SWITCH_4, sw.Pressed() || sw.RisingEdge(), sw.FallingEdge() || sw.RisingEdge());
        }
        #endif
    }

    // Sensor: Toggle Switch 1-3
    {
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_TOGGLE_SWITCH_1
        {
            Switch& sw = hw.switches[DaisyPetal::SW_5];
            _DSPSTUDIO_MAPPING_TYPE_TOGGLE_SWITCH_1::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_TOGGLE_SWITCH_1, sw.Pressed() || sw.RisingEdge(), sw.FallingEdge() || sw.RisingEdge());
        }
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_TOGGLE_SWITCH_2
        {
            Switch& sw = hw.switches[DaisyPetal::SW_6];
            _DSPSTUDIO_MAPPING_TYPE_TOGGLE_SWITCH_2::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_TOGGLE_SWITCH_2, sw.Pressed() || sw.RisingEdge(), sw.FallingEdge() || sw.RisingEdge());
        }
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_TOGGLE_SWITCH_3
        {
            Switch& sw = hw.switches[DaisyPetal::SW_7];
            _DSPSTUDIO_MAPPING_TYPE_TOGGLE_SWITCH_3::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_TOGGLE_SWITCH_3, sw.Pressed() || sw.RisingEdge(), sw.FallingEdge() || sw.RisingEdge());
        }
        #endif
    }

    // Actor: Ring Led 1-8
    {
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_RING_LED_1
        {
            Color color = _DSPSTUDIO_MAPPING_TYPE_RING_LED_1::get_color(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_RING_LED_1);
            hw.SetRingLed(DaisyPetal::RING_LED_1, color.Red(), color.Green(), color.Blue());
        }
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_RING_LED_2
        {
            Color color = _DSPSTUDIO_MAPPING_TYPE_RING_LED_2::get_color(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_RING_LED_2);
            hw.SetRingLed(DaisyPetal::RING_LED_2, color.Red(), color.Green(), color.Blue());
        }
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_RING_LED_3
        {
            Color color = _DSPSTUDIO_MAPPING_TYPE_RING_LED_3::get_color(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_RING_LED_3);
            hw.SetRingLed(DaisyPetal::RING_LED_3, color.Red(), color.Green(), color.Blue());
        }
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_RING_LED_4
        {
            Color color = _DSPSTUDIO_MAPPING_TYPE_RING_LED_4::get_color(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_RING_LED_4);
            hw.SetRingLed(DaisyPetal::RING_LED_4, color.Red(), color.Green(), color.Blue());
        }
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_RING_LED_5
        {
            Color color = _DSPSTUDIO_MAPPING_TYPE_RING_LED_5::get_color(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_RING_LED_5);
            hw.SetRingLed(DaisyPetal::RING_LED_5, color.Red(), color.Green(), color.Blue());
        }
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_RING_LED_6
        {
            Color color = _DSPSTUDIO_MAPPING_TYPE_RING_LED_6::get_color(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_RING_LED_6);
            hw.SetRingLed(DaisyPetal::RING_LED_6, color.Red(), color.Green(), color.Blue());
        }
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_RING_LED_7
        {
            Color color = _DSPSTUDIO_MAPPING_TYPE_RING_LED_7::get_color(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_RING_LED_7);
            hw.SetRingLed(DaisyPetal::RING_LED_7, color.Red(), color.Green(), color.Blue());
        }
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_RING_LED_8
        {
            Color color = _DSPSTUDIO_MAPPING_TYPE_RING_LED_8::get_color(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_RING_LED_8);
            hw.SetRingLed(DaisyPetal::RING_LED_8, color.Red(), color.Green(), color.Blue());
        }
        #endif
    }

    // Actors: Footswitch Led 1-4
    {
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_FOOT_SWITCH_LED_1
            hw.SetFootswitchLed(DaisyPetal::FOOTSWITCH_LED_1, to_float(_DSPSTUDIO_MAPPING_TYPE_FOOT_SWITCH_LED_1::get_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_FOOT_SWITCH_LED_1)));
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_FOOT_SWITCH_LED_2
            hw.SetFootswitchLed(DaisyPetal::FOOTSWITCH_LED_2, to_float(_DSPSTUDIO_MAPPING_TYPE_FOOT_SWITCH_LED_2::get_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_FOOT_SWITCH_LED_2)));
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_FOOT_SWITCH_LED_3
            hw.SetFootswitchLed(DaisyPetal::FOOTSWITCH_LED_3, to_float(_DSPSTUDIO_MAPPING_TYPE_FOOT_SWITCH_LED_3::get_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_FOOT_SWITCH_LED_3)));
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_FOOT_SWITCH_LED_4
            hw.SetFootswitchLed(DaisyPetal::FOOTSWITCH_LED_4, to_float(_DSPSTUDIO_MAPPING_TYPE_FOOT_SWITCH_LED_4::get_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_FOOT_SWITCH_LED_4)));
        #endif
    }
}