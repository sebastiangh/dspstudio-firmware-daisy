#!/usr/bin/env bash
version=`node -p "require('./package.json').version.split('-')[0]"`
npm version $version-`date +%Y%m%d%H%M%S` --no-git-tag-version