#!/bin/bash
set -e # fail if any command fails

unameOut="$(uname -s)"
case "${unameOut}" in
    Darwin*)    nproc=$(sysctl -n hw.logicalcpu);;
    *)          nproc=$(nproc);;
esac

START_DIR=$PWD
LIBDAISY_DIR=$PWD/libdaisy
DAISYSP_DIR=$PWD/DaisySP

echo "building libDaisy . . ."
cd "$LIBDAISY_DIR" && make clean > /dev/null && make -j$nproc > /dev/null
echo "done."

echo "building DaisySP . . ."
cd "$DAISYSP_DIR" && make clean > /dev/null && make -j$nproc > /dev/null
echo "done."

