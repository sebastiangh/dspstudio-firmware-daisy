#!/bin/bash
set -e # fail if any command fails



unameOut="$(uname -s)"
case "${unameOut}" in
    Darwin*)    nproc=$(sysctl -n hw.logicalcpu);;
    *)          nproc=$(nproc);;
esac

echo "building examples:"
start_dir=$PWD
example_dirs=(granuform)
for e in ${example_dirs[@]}; do
    for d in $e/*/; do
        echo "building $d"
        cd "$d"
        make clean > /dev/null
        make -j$nproc > /dev/null
        cd "$start_dir"
        echo "done"
    done
done
