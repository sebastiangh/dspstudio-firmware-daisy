#include "daisysp.h"
#include "daisy_field.h"
namespace dspstudio_device {
#include "dspstudio_generated.h"
}
#include "dspstudio_gluecode.h"
#include "dspstudio_util.h"
#include "usbd/UsbMidiHandler.h"

using namespace daisysp;
using namespace daisy;
using namespace wavloader;

static DaisyField hw;
static UsbMidiHandler usbMidi;

extern ErrorCode lastLoadTableError;
extern size_t lastLoadTableFrames;

using namespace daisy;
using namespace dspstudio_device;

dspstudio_device::mod_device* g_dev;

static void ProcessControls();
static void ProcessDisplay();

static void AudioCallback(const float* const* in, float** out, size_t size) {
    cpuMeasurementBegin();
    ProcessControls();
    dspstudio_device::array_process_device(g_dev, in, 2, out, 2, g_dev->bufferSize); 
    cpuMeasurementEnd();
}

int main(void)
{
    InitializeUsbMappings();
    hw.Init(cpuBoost);

    g_dev = dspstudio_device::create();
    InitializeAudioRates(hw.seed);

    hw.midi.StartReceive();
    
    usbMidi.Init(UsbMidiHandler::FS_INTERNAL);
    usbMidi.StartReceive();

    hw.StartAdc();
    hw.StartAudio(AudioCallback);

    uint32_t last = 0;
    for(;;)
    {
        hw.midi.Listen();
        while(hw.midi.HasEvents())
        {
            HandleMidiMessage(hw.midi.PopEvent());
        }

        usbMidi.Listen();
        while(usbMidi.HasEvents())
        {
            HandleMidiMessage(usbMidi.PopEvent());
        }

        uint32_t now = System::GetUs();
        if (now - last > 30000) {
            ProcessDisplay();
            hw.led_driver.SwapBuffersAndTransmit();
            last = now;
        }
    }
}

namespace dspstudio_device {
void draw_point(dspstudio_device::DISPLAY display, float x, float y, float z, float w, float r, float g, float b, float a, float size) {
    if (x >= -1 && x <= 1 && y >= -1 && y <= 1) {
        hw.display.DrawPixel(
            (x + 1.0f) * 0.5f * 60 + 64 + 2,
            (-y + 1.0f) * 0.5f * 60 + 2,
            1//grey > 0.5
        );
    }
}
}

static void ProcessControls() {
    hw.ProcessDigitalControls();
    hw.ProcessAnalogControls();


    // Sensors: Knobs 1-8
    {
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_KNOB_1
            _DSPSTUDIO_MAPPING_TYPE_KNOB_1::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_KNOB_1, hw.knob[DaisyField::KNOB_1].Value(), true);
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_KNOB_2
            _DSPSTUDIO_MAPPING_TYPE_KNOB_2::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_KNOB_2, hw.knob[DaisyField::KNOB_2].Value(), true);
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_KNOB_3
            _DSPSTUDIO_MAPPING_TYPE_KNOB_3::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_KNOB_3, hw.knob[DaisyField::KNOB_3].Value(), true);
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_KNOB_4
            _DSPSTUDIO_MAPPING_TYPE_KNOB_4::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_KNOB_4, hw.knob[DaisyField::KNOB_4].Value(), true);
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_KNOB_5
            _DSPSTUDIO_MAPPING_TYPE_KNOB_5::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_KNOB_5, hw.knob[DaisyField::KNOB_5].Value(), true);
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_KNOB_6
            _DSPSTUDIO_MAPPING_TYPE_KNOB_6::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_KNOB_6, hw.knob[DaisyField::KNOB_6].Value(), true);
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_KNOB_7
            _DSPSTUDIO_MAPPING_TYPE_KNOB_7::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_KNOB_7, hw.knob[DaisyField::KNOB_7].Value(), true);
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_KNOB_8
            _DSPSTUDIO_MAPPING_TYPE_KNOB_8::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_KNOB_8, hw.knob[DaisyField::KNOB_8].Value(), true);
        #endif
    }

    // Sensors: Switches Sw 1-2
    {
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_SW_1
        {
            Switch* sw = *hw.GetSwitch(DaisyField::SW_1);
            _DSPSTUDIO_MAPPING_TYPE_SW_1::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_SW_1, sw->Pressed() || sw->RisingEdge(), sw->FallingEdge() || sw->RisingEdge());
        }
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_SW_2
        {
            Switch* sw = hw.GetSwitch(DaisyField::SW_2);
            _DSPSTUDIO_MAPPING_TYPE_SW_2::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_SW_2, sw->Pressed() || sw->RisingEdge(), sw->FallingEdge() || sw->RisingEdge());
        }
        #endif
    }

    // Sensors: Keyboard Row A1-8
    {
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_BUTTON_A1
            _DSPSTUDIO_MAPPING_TYPE_BUTTON_A1::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_BUTTON_A1, hw.KeyboardState(8) || hw.KeyboardRisingEdge(8), hw.KeyboardRisingEdge(8) || hw.KeyboardFallingEdge(8));
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_BUTTON_A2
            _DSPSTUDIO_MAPPING_TYPE_BUTTON_A2::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_BUTTON_A2, hw.KeyboardState(9) || hw.KeyboardRisingEdge(9), hw.KeyboardRisingEdge(9) || hw.KeyboardFallingEdge(9));
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_BUTTON_A3
            _DSPSTUDIO_MAPPING_TYPE_BUTTON_A3::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_BUTTON_A3, hw.KeyboardState(10) || hw.KeyboardRisingEdge(10), hw.KeyboardRisingEdge(10) || hw.KeyboardFallingEdge(10));
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_BUTTON_A4
            _DSPSTUDIO_MAPPING_TYPE_BUTTON_A4::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_BUTTON_A4, hw.KeyboardState(11) || hw.KeyboardRisingEdge(11), hw.KeyboardRisingEdge(11) || hw.KeyboardFallingEdge(11));
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_BUTTON_A5
            _DSPSTUDIO_MAPPING_TYPE_BUTTON_A5::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_BUTTON_A5, hw.KeyboardState(12) || hw.KeyboardRisingEdge(12), hw.KeyboardRisingEdge(12) || hw.KeyboardFallingEdge(12));
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_BUTTON_A6
            _DSPSTUDIO_MAPPING_TYPE_BUTTON_A6::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_BUTTON_A6, hw.KeyboardState(13) || hw.KeyboardRisingEdge(13), hw.KeyboardRisingEdge(13) || hw.KeyboardFallingEdge(13));
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_BUTTON_A7
            _DSPSTUDIO_MAPPING_TYPE_BUTTON_A7::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_BUTTON_A7, hw.KeyboardState(14) || hw.KeyboardRisingEdge(14), hw.KeyboardRisingEdge(14) || hw.KeyboardFallingEdge(14));
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_BUTTON_A8
            _DSPSTUDIO_MAPPING_TYPE_BUTTON_A8::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_BUTTON_A8, hw.KeyboardState(15) || hw.KeyboardRisingEdge(15), hw.KeyboardRisingEdge(15) || hw.KeyboardFallingEdge(15));
        #endif
    }    
    
    // Sensors: Keyboard Row B1-8
    {
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_BUTTON_B1
            _DSPSTUDIO_MAPPING_TYPE_BUTTON_B1::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_BUTTON_B1, hw.KeyboardState(0) || hw.KeyboardRisingEdge(0), hw.KeyboardRisingEdge(0) || hw.KeyboardFallingEdge(0));
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_BUTTON_B2
            _DSPSTUDIO_MAPPING_TYPE_BUTTON_B2::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_BUTTON_B2, hw.KeyboardState(1) || hw.KeyboardRisingEdge(1), hw.KeyboardRisingEdge(1) || hw.KeyboardFallingEdge(1));
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_BUTTON_B3
            _DSPSTUDIO_MAPPING_TYPE_BUTTON_B3::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_BUTTON_B3, hw.KeyboardState(2) || hw.KeyboardRisingEdge(2), hw.KeyboardRisingEdge(2) || hw.KeyboardFallingEdge(2));
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_BUTTON_B4
            _DSPSTUDIO_MAPPING_TYPE_BUTTON_B4::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_BUTTON_B4, hw.KeyboardState(3) || hw.KeyboardRisingEdge(3), hw.KeyboardRisingEdge(3) || hw.KeyboardFallingEdge(3));
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_BUTTON_B5
            _DSPSTUDIO_MAPPING_TYPE_BUTTON_B5::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_BUTTON_B5, hw.KeyboardState(4) || hw.KeyboardRisingEdge(4), hw.KeyboardRisingEdge(4) || hw.KeyboardFallingEdge(4));
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_BUTTON_B6
            _DSPSTUDIO_MAPPING_TYPE_BUTTON_B6::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_BUTTON_B6, hw.KeyboardState(5) || hw.KeyboardRisingEdge(5), hw.KeyboardRisingEdge(5) || hw.KeyboardFallingEdge(5));
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_BUTTON_B7
            _DSPSTUDIO_MAPPING_TYPE_BUTTON_B7::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_BUTTON_B7, hw.KeyboardState(6) || hw.KeyboardRisingEdge(6), hw.KeyboardRisingEdge(6) || hw.KeyboardFallingEdge(6));
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_BUTTON_B8
            _DSPSTUDIO_MAPPING_TYPE_BUTTON_B8::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_BUTTON_B8, hw.KeyboardState(7) || hw.KeyboardRisingEdge(7), hw.KeyboardRisingEdge(7) || hw.KeyboardFallingEdge(7));
        #endif
    }

    // Sensors: CV In 1-4
    {
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_CV_IN_1
            _DSPSTUDIO_MAPPING_TYPE_CV_IN_1::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_CV_IN_1, hw.GetCvValue(0), true);
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_CV_IN_2
            _DSPSTUDIO_MAPPING_TYPE_CV_IN_2::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_CV_IN_2, hw.GetCvValue(1), true);
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_CV_IN_3
            _DSPSTUDIO_MAPPING_TYPE_CV_IN_3::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_CV_IN_3, hw.GetCvValue(2), true);
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_CV_IN_3
            _DSPSTUDIO_MAPPING_TYPE_CV_IN_3::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_CV_IN_3, hw.GetCvValue(3), true);
        #endif
    }

    // Sensor: Gate In 1
    {
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_GATE_IN_1
            _DSPSTUDIO_MAPPING_TYPE_GATE_IN_1::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_GATE_IN_1, hw.gate_in.State(), true);
        #endif
    }
 
    // Actors: Led Knob 1-8
    {
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_LED_KNOB_1
            hw.led_driver.SetLed(DaisyField::LED_KNOB_1, to_float(_DSPSTUDIO_MAPPING_TYPE_LED_KNOB_1::get_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_LED_KNOB_1)));            
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_LED_KNOB_2
            hw.led_driver.SetLed(DaisyField::LED_KNOB_2, to_float(_DSPSTUDIO_MAPPING_TYPE_LED_KNOB_2::get_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_LED_KNOB_2)));            
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_LED_KNOB_3
            hw.led_driver.SetLed(DaisyField::LED_KNOB_3, to_float(_DSPSTUDIO_MAPPING_TYPE_LED_KNOB_3::get_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_LED_KNOB_3)));            
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_LED_KNOB_4
            hw.led_driver.SetLed(DaisyField::LED_KNOB_4, to_float(_DSPSTUDIO_MAPPING_TYPE_LED_KNOB_4::get_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_LED_KNOB_4)));            
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_LED_KNOB_5
            hw.led_driver.SetLed(DaisyField::LED_KNOB_5, to_float(_DSPSTUDIO_MAPPING_TYPE_LED_KNOB_5::get_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_LED_KNOB_5)));            
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_LED_KNOB_6
            hw.led_driver.SetLed(DaisyField::LED_KNOB_6, to_float(_DSPSTUDIO_MAPPING_TYPE_LED_KNOB_6::get_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_LED_KNOB_6)));            
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_LED_KNOB_7
            hw.led_driver.SetLed(DaisyField::LED_KNOB_7, to_float(_DSPSTUDIO_MAPPING_TYPE_LED_KNOB_7::get_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_LED_KNOB_7)));            
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_LED_KNOB_8
            hw.led_driver.SetLed(DaisyField::LED_KNOB_8, to_float(_DSPSTUDIO_MAPPING_TYPE_LED_KNOB_8::get_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_LED_KNOB_8)));            
        #endif
    }

    // Actors: Led Key A1-8
    {
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_LED_KEY_A1
            hw.led_driver.SetLed(DaisyField::LED_KEY_A1, to_float(_DSPSTUDIO_MAPPING_TYPE_LED_KEY_A1::get_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_LED_KEY_A1)));            
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_LED_KEY_A2
            hw.led_driver.SetLed(DaisyField::LED_KEY_A2, to_float(_DSPSTUDIO_MAPPING_TYPE_LED_KEY_A2::get_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_LED_KEY_A2)));            
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_LED_KEY_A3
            hw.led_driver.SetLed(DaisyField::LED_KEY_A3, to_float(_DSPSTUDIO_MAPPING_TYPE_LED_KEY_A3::get_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_LED_KEY_A3)));            
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_LED_KEY_A4
            hw.led_driver.SetLed(DaisyField::LED_KEY_A4, to_float(_DSPSTUDIO_MAPPING_TYPE_LED_KEY_A4::get_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_LED_KEY_A4)));            
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_LED_KEY_A5
            hw.led_driver.SetLed(DaisyField::LED_KEY_A5, to_float(_DSPSTUDIO_MAPPING_TYPE_LED_KEY_A5::get_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_LED_KEY_A5)));            
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_LED_KEY_A6
            hw.led_driver.SetLed(DaisyField::LED_KEY_A6, to_float(_DSPSTUDIO_MAPPING_TYPE_LED_KEY_A6::get_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_LED_KEY_A6)));            
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_LED_KEY_A7
            hw.led_driver.SetLed(DaisyField::LED_KEY_A7, to_float(_DSPSTUDIO_MAPPING_TYPE_LED_KEY_A7::get_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_LED_KEY_A7)));            
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_LED_KEY_A8
            hw.led_driver.SetLed(DaisyField::LED_KEY_A8, to_float(_DSPSTUDIO_MAPPING_TYPE_LED_KEY_A8::get_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_LED_KEY_A8)));
        #endif
    }

    // Actors: Led Key B1-8
    {
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_LED_KEY_B1
            hw.led_driver.SetLed(DaisyField::LED_KEY_B1, to_float(_DSPSTUDIO_MAPPING_TYPE_LED_KEY_B1::get_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_LED_KEY_B1)));            
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_LED_KEY_B2
            hw.led_driver.SetLed(DaisyField::LED_KEY_B2, to_float(_DSPSTUDIO_MAPPING_TYPE_LED_KEY_B2::get_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_LED_KEY_B2)));            
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_LED_KEY_B3
            hw.led_driver.SetLed(DaisyField::LED_KEY_B3, to_float(_DSPSTUDIO_MAPPING_TYPE_LED_KEY_B3::get_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_LED_KEY_B3)));            
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_LED_KEY_B4
            hw.led_driver.SetLed(DaisyField::LED_KEY_B4, to_float(_DSPSTUDIO_MAPPING_TYPE_LED_KEY_B4::get_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_LED_KEY_B4)));            
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_LED_KEY_B5
            hw.led_driver.SetLed(DaisyField::LED_KEY_B5, to_float(_DSPSTUDIO_MAPPING_TYPE_LED_KEY_B5::get_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_LED_KEY_B5)));            
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_LED_KEY_B6
            hw.led_driver.SetLed(DaisyField::LED_KEY_B6, to_float(_DSPSTUDIO_MAPPING_TYPE_LED_KEY_B6::get_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_LED_KEY_B6)));            
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_LED_KEY_B7
            hw.led_driver.SetLed(DaisyField::LED_KEY_B7, to_float(_DSPSTUDIO_MAPPING_TYPE_LED_KEY_B7::get_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_LED_KEY_B7)));            
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_LED_KEY_B8
            hw.led_driver.SetLed(DaisyField::LED_KEY_B8, to_float(_DSPSTUDIO_MAPPING_TYPE_LED_KEY_B8::get_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_LED_KEY_B8)));
        #endif
    }

    // Actors: CV Out 1-2
    {
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_CV_OUT_1
            hw.SetCvOut1(static_cast<uint16_t>(4095.99f * to_float(_DSPSTUDIO_MAPPING_TYPE_CV_OUT_1::get_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_CV_OUT_1))))
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_CV_OUT_2
            hw.SetCvOut2(static_cast<uint16_t>(4095.99f * to_float(_DSPSTUDIO_MAPPING_TYPE_CV_OUT_2::get_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_CV_OUT_2))))
        #endif
    }
    
    // Actor: Gate Out
    {
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_GATE_OUT
            dsy_gpio_write(&hw.gate_output, to_bool(_DSPSTUDIO_MAPPING_TYPE_GATE_OUT::get_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_GATE_OUT))))
        #endif
    }

    // Actor: Display
    {
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_DISPLAY
            process_display(g_dev, dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_DISPLAY, g_dev->bufferSize);
        #endif
    }
}


char line1[30], line2[30], line3[30], line4[30], line5[30], line6[30];

static void ProcessDisplay() {

    float maxBlockTime = 1.0f * hw.AudioBlockSize() / hw.AudioSampleRate() * 1000 * 1000; // allowed time to pass for one block to be calculated
    
    float wcet, avg;
    cpuMeasurementEval(maxBlockTime, wcet, avg);

    sprintf(line1, "AVG: %d.%02d%%", (int)avg, ((int)(avg * 100)) % 100);
    sprintf(line2, "Frms: %d", (int)lastLoadTableFrames);
    if (lastLoadTableError == Success) {
        sprintf(line3, "Success");
    } else if (lastLoadTableError == FileNotFound) {
        sprintf(line3, "FileNotFound");
    } else if (lastLoadTableError == BrokenFile) {
        sprintf(line3, "BrokenFile");
    } else if (lastLoadTableError == UnsupportedFormat) {
        sprintf(line3, "UnsupportedFmt");
    } else if (lastLoadTableError == InvalidChannel) {
        sprintf(line3, "InvalidChan");
    }


    hw.display.SetCursor(0, 0);
    hw.display.WriteString(line1, Font_6x8, false);
    hw.display.SetCursor(0, 8);
    hw.display.WriteString(line2, Font_6x8, false);
    hw.display.SetCursor(0, 16);
    hw.display.WriteString(line3, Font_6x8, false);


    hw.display.Update();

    hw.display.Fill(true);
    hw.display.DrawRect(64+1, 1, 128-2, 64-2, false, true);
}
