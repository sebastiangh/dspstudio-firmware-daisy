#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))
#define CLAMP(x, lower, upper) (MIN(upper, MAX(x, lower)))
typedef struct {
    float pitch;
    float onvelocity;
    float offvelocity;
    uint8_t pressed;
    uint8_t active;
} Voice;
typedef struct {
} Voices;
typedef struct {
    Voice** voices;
    uint16_t* poly;
    uint16_t num_voices;
} VoiceInfo;
typedef struct {
    float controller[129]; // 128 elements, first starts at position 1
    float aftertouch;
    float pitchwheel;
} Controller;
typedef struct {
} Controllers;
typedef struct {
    float sampleRate;
    uint32_t bufferSize;
    uint8_t isDoubleFp;
} mod_device;
uint32_t g_sampleIndex;
void process_device(mod_device* _this) {
}
VoiceInfo* get_voice_info(mod_device* dev) {
    return 0;
}
Controller* get_controller(mod_device* dev, uint8_t channel) {
    return 0;
}
typedef enum {
    PARAMETER_INVALID,
    PARAMETER_ISDOUBLEFP,
    NUM_PARAMETERS
} PARAMETER;
double* get_double_ptr(mod_device* _this, PARAMETER parameter) {
    return 0;
}
float* get_float_ptr(mod_device* _this, PARAMETER parameter) {
    return 0;
}
uint8_t* get_bool_ptr(mod_device* _this, PARAMETER parameter) {
    uint8_t* pointers[] = {
        0,
        &_this->isDoubleFp,
    };
    if (parameter > PARAMETER_INVALID && parameter < NUM_PARAMETERS) { return pointers[parameter]; }
    return 0;
}
typedef enum {
    NUM_SLIDERS
} SLIDER;
/** @parameter normValue The slider value in the range [0, 1] to set. */
void set_slider_value(mod_device* device, SLIDER slider, float normValue) {
}
float get_slider_value(mod_device* device, SLIDER slider) {
    return 0.0f;
}
typedef enum {
    NUM_BUTTONS
} BUTTON;
/** @parameter state The button state, either being pressed=1, or released=0. */
void set_button_value(mod_device* device, BUTTON button, uint8_t state) {
}
uint8_t get_button_value(mod_device* device, BUTTON button) {
    return 0;
}
typedef enum {
    NUM_LABELS
} LABEL;
float get_label_value(mod_device* device, LABEL label) {
    return 0.0f;
}
typedef enum {
    NUM_DISPLAYS
} DISPLAY;
float get_display_value(mod_device* device, DISPLAY display, uint8_t portIdx) {
    return 0;
}
void set_display_value(mod_device* device, DISPLAY display, uint8_t portIdx, float value) {
}
void set_buffer_size(mod_device* device, uint32_t bufferSize) {
    if (device->bufferSize != bufferSize) {
        device->bufferSize = bufferSize;
    }
}
void set_sample_rate(mod_device* device, uint32_t sampleRate) {
    device->sampleRate = sampleRate;
}
void array_process_device(mod_device* device, const float* const* inputs, size_t numInputs, float** outputs, size_t numOutputs, uint32_t bufferSize) {
    for (uint32_t b = 0; b < bufferSize; b++) {
        g_sampleIndex = b;
        process_device(device);
    }
}
mod_device* create(void) {
    static mod_device mod;
    size_t size = sizeof(mod_device);
    memset(&mod, 0, size);
    mod.isDoubleFp = 0;
    return &mod;
}
void destroy(mod_device* device) { }