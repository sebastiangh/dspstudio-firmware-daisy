# ACL Granuform

Powered from standard Eurorack Power. (+/- 12V)

## Features

- _x Audio Outputs
- 4x Knobs
- 4x CV inputs
- 2x Gate Inputs
- 1x Gate Output
- 2x buttons
- 1x toggle
- USB Host for loading wav file
