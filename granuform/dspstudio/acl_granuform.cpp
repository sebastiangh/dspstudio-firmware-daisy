#include "acl_granuform.h"
#include "dev/codec_ak4556.h"

using namespace daisy;
using namespace acl;

// Hardware Definitions
#define PIN_ENC_CLICK 0
#define PIN_ENC_B 13
#define PIN_ENC_A 14
#define PIN_OLED_DC 9
#define PIN_OLED_RESET 22
#define PIN_GATE_IN_1 20
#define PIN_GATE_IN_2 19
#define PIN_MAIN_SWITCH 1
#define PIN_LED_MAIN 2

#define PIN_CTRL_1 15
#define PIN_CTRL_2 16
#define PIN_CTRL_3 21
#define PIN_CTRL_4 18
#define PIN_CTRL_5 17
#define PIN_CTRL_6 23

void AclGranuform::InitGranu(bool boost)
{
    // Configure Seed first
    seed.Configure();
    seed.Init(boost);
    InitAudio();
    InitDisplay();
    InitEncoder();
    InitGates();
    InitDisplay();
    InitControls();
    InitSwitchWithLED();
    // Set Screen update vars
    screen_update_period_ = 17; // roughly 60Hz
    screen_update_last_   = seed.system.GetNow();
}

void AclGranuform::SetHidUpdateRates()
{
    for(size_t i = 0; i < GR_CTRL_LAST; i++)
    {
        granuControls[i].SetSampleRate(AudioCallbackRate());
    }
}

// Private Function Implementations
// set SAI2 stuff -- run this between seed configure and init
void AclGranuform::InitAudio()
{
    // Handle Seed Audio as-is and then
    SaiHandle::Config sai_config[1];
    // Internal Codec
    if(seed.CheckBoardVersion() == DaisySeed::BoardVersion::DAISY_SEED_1_1)
    {
        sai_config[0].pin_config.sa = {DSY_GPIOE, 6};
        sai_config[0].pin_config.sb = {DSY_GPIOE, 3};
        sai_config[0].a_dir         = SaiHandle::Config::Direction::RECEIVE;
        sai_config[0].b_dir         = SaiHandle::Config::Direction::TRANSMIT;
    }
    else
    {
        sai_config[0].pin_config.sa = {DSY_GPIOE, 6};
        sai_config[0].pin_config.sb = {DSY_GPIOE, 3};
        sai_config[0].a_dir         = SaiHandle::Config::Direction::TRANSMIT;
        sai_config[0].b_dir         = SaiHandle::Config::Direction::RECEIVE;
    }
    sai_config[0].periph          = SaiHandle::Config::Peripheral::SAI_1;
    sai_config[0].sr              = SaiHandle::Config::SampleRate::SAI_48KHZ;
    sai_config[0].bit_depth       = SaiHandle::Config::BitDepth::SAI_24BIT;
    sai_config[0].a_sync          = SaiHandle::Config::Sync::MASTER;
    sai_config[0].b_sync          = SaiHandle::Config::Sync::SLAVE;
    sai_config[0].pin_config.fs   = {DSY_GPIOE, 4};
    sai_config[0].pin_config.mclk = {DSY_GPIOE, 2};
    sai_config[0].pin_config.sck  = {DSY_GPIOE, 5};

    SaiHandle sai_handle[2];
    sai_handle[0].Init(sai_config[0]); // Only intialise one SaiHandle

    // Reinit Audio for internal codec
    AudioHandle::Config cfg;
    cfg.blocksize  = 48;
    cfg.samplerate = SaiHandle::Config::SampleRate::SAI_48KHZ;
    cfg.postgain   = 0.5f;
    seed.audio_handle.Init(cfg, sai_handle[0], sai_handle[1]);
}

void AclGranuform::InitControls()
{
    AdcChannelConfig cfg[GR_CTRL_LAST];

    // Init ADC channels with Pins
    cfg[GR_CTRL_1].InitSingle(seed.GetPin(PIN_CTRL_1));
    cfg[GR_CTRL_2].InitSingle(seed.GetPin(PIN_CTRL_2));
    cfg[GR_CTRL_3].InitSingle(seed.GetPin(PIN_CTRL_3));
    cfg[GR_CTRL_4].InitSingle(seed.GetPin(PIN_CTRL_4));
    cfg[GR_CTRL_5].InitSingle(seed.GetPin(PIN_CTRL_5));
    cfg[GR_CTRL_6].InitSingle(seed.GetPin(PIN_CTRL_6));

    // Initialize ADC
    seed.adc.Init(cfg, GR_CTRL_LAST);

    // Initialize AnalogControls, with flip set to true
    for(size_t i = 0; i < GR_CTRL_LAST; i++)
    {
        granuControls[i].Init(seed.adc.GetPtr(i), AudioCallbackRate(), true);
    }
}

void AclGranuform::InitDisplay()
{
    OledDisplay<SSD130x4WireSpi128x64Driver>::Config display_config;

    display_config.driver_config.transport_config.pin_config.dc
        = seed.GetPin(PIN_OLED_DC);
    display_config.driver_config.transport_config.pin_config.reset
        = seed.GetPin(PIN_OLED_RESET);

    display.Init(display_config);
}

void AclGranuform::InitEncoder()
{
    encoder.Init(seed.GetPin(PIN_ENC_A),
                 seed.GetPin(PIN_ENC_B),
                 seed.GetPin(PIN_ENC_CLICK));
}

void AclGranuform::InitGates()
{
    // Gate Output
    // gate_output.pin  = seed.GetPin(PIN_GATE_OUT);
    // gate_output.mode = DSY_GPIO_MODE_OUTPUT_PP;
    // gate_output.pull = DSY_GPIO_NOPULL;
    // dsy_gpio_init(&gate_output);

    // Gate Inputs
    dsy_gpio_pin pin;
    pin = seed.GetPin(PIN_GATE_IN_1);
    gate_input[GATE_IN_1].Init(&pin);
    pin = seed.GetPin(PIN_GATE_IN_2);
    gate_input[GATE_IN_2].Init(&pin);
}

void AclGranuform::InitSwitchWithLED()
{
    dsy_gpio_pin pin;
    pin = seed.GetPin(PIN_MAIN_SWITCH);
    mainSwitch.Init(pin);
    pin = seed.GetPin(PIN_LED_MAIN);
    GPIO::Config config;
    config.mode = GPIO::Mode::OUTPUT;
    config.pin = daisy::Pin(static_cast<daisy::GPIOPort>(pin.port), pin.pin);
    mainLed.Init(config);
    mainLed.Write(false); // Main Led off
}

void AclGranuform::ProcessGranuAnalogControls()
{
    for(size_t i = 0; i < GR_CTRL_LAST; i++)
    {
        granuControls[i].Process();
    }
}

void AclGranuform::CalibrateCvInputs()
{
    display.Fill(false);
    
    display.SetCursor(20, 20);
    display.WriteString("CV Input 1", Font_6x8, true);
    
    display.SetCursor(20, 40);
    display.WriteString("Apply 1 Volt", Font_6x8, true);

    display.Update();
    DelayMs(3000);
}
