#pragma once
#include <daisy.h>
#include <string>
#include "acl_granuform.h"

extern const float g_sampleRate;
extern const float g_maxSample;

class WaveDrawer {
public:
    WaveDrawer(acl::AclGranuform* const hwp);
    void setWaveName(std::string name);
    void setGeometry(const uint8_t x, const uint8_t y, const uint8_t width, const uint8_t height);
    void writeName();
    static inline void drawPositionMarker(bool on)
    {
        /* Draw position marker */
        hw->display.DrawLine(xPosition + xResolution / 2, 10, xPosition + xResolution / 2, 6 + yResolution, on);
    }
    
    static inline void drawWaveform(const float * const table, const uint32_t tableSize, const float currentPosition, const float timeInSeconds, bool on)
    {
        if(xResolution != 0 && yResolution != 0) {

            /* Calculate start position and wave resolution */ 
            xPhasor = currentPosition - (timeInSeconds / 2.0f) * (g_sampleRate / static_cast<float>(tableSize));
            xPhasorInc = ((timeInSeconds * g_sampleRate) / static_cast<float>(tableSize)) / static_cast<float>(xResolution);

            /* XPhasor can be negative at this point, 
            * make it positive and begin taking samples from the tail of the wavetable
            */
            if(xPhasor < 0.0f) {
                xPhasor = 1.0f + xPhasor;
            }

            /* Draw position marker */
            drawPositionMarker(on);

            uint8_t yZeroLevel = yPosition + yResolution / 2;  // Zero level y-axis
            uint8_t yPixelOld = yZeroLevel;
            uint8_t yPixel{0};

            float tableIndex;
            float sample;		
            for(int n = 0; n < xResolution; n++){
                tableIndex = static_cast<float>((tableSize - 1) * xPhasor);
                sample = table[static_cast<int>(round(tableIndex))];
                //sample = interpolate(table, tableSize, tableIndex);
                yPixel = yZeroLevel - (sample / g_maxSample) * (yResolution / 2);

                if((yPixel - yPixelOld) > 4) {
                  	hw->display.DrawLine(xPosition + n - 1, yPixelOld, xPosition + n - 1, yPixel, on);
                }
                if(n < xResolution) {
                    hw->display.DrawPixel(xPosition + n, yPixel, on);
                }

                yPixelOld = yPixel;
                xPhasor += xPhasorInc;
                if(xPhasor > 1.0f) {
                    xPhasor -= 1.0f;
                    if(n > 0) {
                        hw->display.DrawLine(xPosition + n, yPosition, xPosition + n, yPosition + yResolution, on);
                    }
                }
            }
        } else {
            //debug_break("width or height not 0");
        }
    }

    static inline float interpolate(const float* const table, const uint32_t waveSize, const float index)
    {
        double index_int, index_fract;
        uint16_t nm1, n, np1, np2;
        float snm1, sn, snp1, snp2;

        index_fract = modf(index, &index_int);
        n = (uint16_t)index_int;

        // Modulo index
        np1 = (n + 1) % waveSize;
        np2 = (n + 2) % waveSize;
        nm1 = n == 0 ? waveSize - 1 : n - 1;

        sn   = table[n];
        snm1 = table[nm1];
        snp1 = table[np1];
        snp2 = table[np2];

        return hermiteInterpolate((float)index_fract, snm1, sn, snp1, snp2);
    }

private:
    static acl::AclGranuform* hw;
    static uint8_t xPosition;
    static uint8_t yPosition;
    static uint8_t xResolution;
    static uint8_t yResolution;
    static float xPhasor;
    static float xPhasorInc;
    std::string waveName;
    
    // laurent de soras
    // https://www.musicdsp.org/en/latest/Other/93-hermite-interpollation.html
    static inline float hermiteInterpolate(float frac_pos, float xm1, float x0, float x1, float x2)
    {
        float    c     = (x1 - xm1) * 0.5f;
        float    v     = x0 - x1;
        float    w     = c + v;
        float    a     = w + v + (x2 - x0) * 0.5f;
        float    b_neg = w + a;

        float res = ((((a * frac_pos) - b_neg) * frac_pos + c) * frac_pos + x0);
        return res;
    }
};
