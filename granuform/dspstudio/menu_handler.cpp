#include "menu_handler.h"
#include "printf.h"

static const char* PARAMETER_NAMES[] = {
	"SMOOTHNESS",
	"TRANSPOSE",
	"FINE TUNE",
	"GRAN LEVEL",
	"HP CUTOFF",
	"HP RESONANCE",
	"EXT IN LEVEL",
	"GRAN CHORUS",
	"EXT IN CHORUS",
	"CHORUS SPEED",
	"CHORUS DEPTH",
    0
};

static char valueBuffer[20];

ACTIVE_VIEW MenuHandler::activeView = HOME;

MENU_ITEM Menu::getItem(uint8_t index) {
    return items[index];
}

uint8_t Menu::itemCount() {
    return items.size();
}

void Menu::initItems(const char** parameterNames, float* values, size_t length) {
    int index = 0;
    while(parameterNames[index] && index < length) {
        MENU_ITEM item;
        item.title = parameterNames[index];
        sprintf(valueBuffer, "%.2f", values[index]);
        item.value = std::string(valueBuffer);
        items.push_back(item);
        index++;
    }
}

MenuHandler::MenuHandler(acl::AclGranuform* const hw) : hw{hw}, initialized{false}
{
    // activeView = HOME;
    focus = FOCUS_IDX0;
    lastFocus = FOCUS_IDX0;
    mainMenu.itemsPerPage = 6;
    mainMenu.pageCount = 2;
    mainMenu.currentPage = 0;
}

void MenuHandler::init(float* values, size_t length)
{
    if(!initialized) {
        mainMenu.initItems(PARAMETER_NAMES, values, length);
        initialized = true;
    }   
}

ACTIVE_VIEW MenuHandler::getActiveView() {
    return activeView;
}

void MenuHandler::setPage(uint8_t page) {
    if(activeView == HOME) {

    } else if(activeView == MAIN_MENU) {
        mainMenu.currentPage = page;
        hw->display.Fill(false);
        /* Draw page */
        for(int i=0; i<mainMenu.itemsPerPage; i++) {

		    if(i + mainMenu.currentPage * mainMenu.itemsPerPage < (int)mainMenu.itemCount()) {

                /* Write parameter name */
                hw->display.SetCursor(0, i * 10);
                hw->display.WriteString(mainMenu.getItem(i + mainMenu.currentPage * mainMenu.itemsPerPage).title.data(), Font_6x8, true);

                /* Write parameter value */
			    if(i + mainMenu.currentPage * mainMenu.itemsPerPage < (int)mainMenu.itemCount()) {
				    if(mainMenu.getItem(i + mainMenu.currentPage * mainMenu.itemsPerPage).value.length() > 0) {
                        hw->display.SetCursor(88, i * 10);
                        hw->display.WriteString(mainMenu.getItem(i + mainMenu.currentPage * mainMenu.itemsPerPage).value.data(), Font_6x8, true);
				    }
			    }
		    }
	    }
    }
}

void MenuHandler::drawMenu() {
    setPage(mainMenu.currentPage);
    drawCursor(focus, true);
}

void MenuHandler::drawCursor(FOCUS_INDEX index, bool visible) {
    int yFocus = index * 10;
    if(visible) {
        hw->display.SetCursor(81, yFocus);
        hw->display.WriteChar('>', Font_6x8, true);

        // TEST
        // hw->display.SetCursor(100, yFocus);
        // hw->display.WriteString(std::to_string(deviceInternalIndex).data(), Font_6x8, true);

    } else {
        hw->display.DrawRect(81, yFocus, 87, yFocus + 8, false, true);
    }
}

void MenuHandler::moveFocus(uint8_t index) {
    if((index < mainMenu.itemCount()) && (index >= 0)) {
        uint8_t newPage = index / mainMenu.itemsPerPage;
        drawCursor(focus, false);
        focus = static_cast<FOCUS_INDEX>(index - newPage * mainMenu.itemsPerPage);
        setPage(newPage);
        drawCursor(focus, true);
    }
}

void MenuHandler::processEncoder(uint8_t index, float value) { 
    if(activeView == MAIN_MENU) {
        deviceInternalIndex = index;
        moveFocus(index);
    }
}

uint8_t MenuHandler::getActiveItem() {
    return activeItem;
}

void MenuHandler::setActiveItem(uint8_t index) {
    activeItem = index;
}


void MenuHandler::processMainButton() {
    if(activeView == HOME) {
        activeView = MAIN_MENU;
    } else {
        activeView = HOME;
    }
}

void MenuHandler::updateValue(float value) {
    static float lastValue = 0.0f;
    if(lastValue != value) {
        lastValue = value;
        mainMenu.setItemValue(deviceInternalIndex, value);
        if(focus + mainMenu.currentPage * mainMenu.itemsPerPage < (int)mainMenu.itemCount()) {
	        if(mainMenu.getItem(focus + mainMenu.currentPage * mainMenu.itemsPerPage).value.length() > 0) {
                hw->display.SetCursor(88, focus * 10);
                hw->display.WriteString(mainMenu.getItem(focus + mainMenu.currentPage * mainMenu.itemsPerPage).value.data(), Font_6x8, true);
	        }
	    }
    }
}

void Menu::setItemValue(uint8_t index, float value) {
    sprintf(valueBuffer, "%.2f", value);
    items[index].value = std::string(valueBuffer);
}