#include "data_storage.h"

#define PIN_EE_WP     3    // EEPROM write protection pin

using acl::DataStorage;
using daisy::I2CHandle;
using daisy::GPIO;

DataStorage::DataStorage()
    : i2cHandle_{}
    , writeProtect_{}
{
}

bool DataStorage::Init()
{
    bool retValue = false;

    /* Init write protection pin */
    GPIO::Config wpConfig;
    dsy_gpio_pin wpPin = daisy::DaisySeed::GetPin(PIN_EE_WP);
    wpConfig.mode = GPIO::Mode::OUTPUT;
    wpConfig.pin = daisy::Pin(static_cast<daisy::GPIOPort>(wpPin.port), wpPin.pin);
    writeProtect_.Init(wpConfig);
    
    /* Write protection on */
    writeProtect_.Write(true);

    /* Init I2C */
    i2cConfig_.periph = I2CHandle::Config::Peripheral::I2C_1;
    i2cConfig_.speed = I2CHandle::Config::Speed::I2C_400KHZ;
    i2cConfig_.mode = I2CHandle::Config::Mode::I2C_MASTER;
    i2cConfig_.pin_config.scl = {DSY_GPIOB, 8};
    i2cConfig_.pin_config.sda = {DSY_GPIOB, 9};
    
    if(i2cHandle_.Init(i2cConfig_) == daisy::I2CHandle::Result::OK) {
        retValue = true;
        
        /* Write protection off */
        writeProtect_.Write(false);
    }

    /* Let the chip get ready */
    daisy::System::Delay(50);

    return retValue;
}

I2CHandle::Result DataStorage::WriteData(uint32_t address, uint8_t pData[], uint32_t size)
{
    I2CHandle::Result result = I2CHandle::Result::ERR;
    uint8_t i2cAddress = DEV_ADDRESS;

    // Set the 17th bit of address (see CAT24M01 datasheet)
    i2cAddress |= (address & 0x00010000) ? 0x01 : 0x00;
    
    result = i2cHandle_.WriteDataAtAddress(i2cAddress << 1, static_cast<uint16_t>(address), 2, pData, size, 10);
    return result;
}

I2CHandle::Result DataStorage::ReadData(uint32_t address, uint8_t pData[], uint32_t size)
{
    I2CHandle::Result result = I2CHandle::Result::ERR;
    uint8_t i2cAddress = DEV_ADDRESS;
    i2cAddress |= (address & 0x00010000) ? 0x01 : 0x00;

    result = i2cHandle_.ReadDataAtAddress(i2cAddress << 1, static_cast<uint16_t>(address), 2, pData, size, 50);

    return result;
}

I2CHandle::Result DataStorage::loadSettings()
{
    I2CHandle::Result result = I2CHandle::Result::ERR;



    return result;
}

daisy::I2CHandle::Result DataStorage::restore()
{
    I2CHandle::Result result = I2CHandle::Result::ERR;

    acl::Parameter parameter;
    parameter.setType(acl::Parameter::Type::FLOAT);
    
    parameter.setValue(0.5f);
    parameter.key_ = Parameter::Key::SMOOTHNESS;
    WriteParameter(acl::EEPROM_Address::P_SMOOTHNESS, &parameter);

    parameter.setValue(0.5f);
    parameter.key_ = Parameter::Key::TRANSPOSE;
    WriteParameter(acl::EEPROM_Address::P_TRANSPOSE, &parameter);

    parameter.setValue(0.0f);
    parameter.key_ = Parameter::Key::FINE_TUNE;
    WriteParameter(acl::EEPROM_Address::P_FINE_TUNE, &parameter);

    parameter.setValue(0.9f);
    parameter.key_ = Parameter::Key::GRAN_LEVEL;
    WriteParameter(acl::EEPROM_Address::P_GRAN_LEVEL, &parameter);

    parameter.setValue(20.0f);
    parameter.key_ = Parameter::Key::HP_CUTOFF;
    WriteParameter(acl::EEPROM_Address::P_HP_CUTOFF, &parameter);

    parameter.setValue(0.0f);
    parameter.key_ = Parameter::Key::HP_RESONANCE;
    WriteParameter(acl::EEPROM_Address::P_HP_RESONANCE, &parameter);

    parameter.setValue(0.0f);
    parameter.key_ = Parameter::Key::EXT_IN_LEVEL;
    WriteParameter(acl::EEPROM_Address::P_EXT_IN_LEVEL, &parameter);

    parameter.setValue(0.0f);
    parameter.key_ = Parameter::Key::GRAN_CHORUS;
    WriteParameter(acl::EEPROM_Address::P_GRAN_CHORUS, &parameter);

    parameter.setValue(0.0f);
    parameter.key_ = Parameter::Key::EXT_IN_CHORUS;
    WriteParameter(acl::EEPROM_Address::P_EXT_IN_CHORUS, &parameter);

    parameter.setValue(0.7f);
    parameter.key_ = Parameter::Key::CHORUS_SPEED;
    WriteParameter(acl::EEPROM_Address::P_CHORUS_SPEED, &parameter);

    parameter.setValue(2.1f);
    parameter.key_ = Parameter::Key::CHORUS_DEPTH;
    WriteParameter(acl::EEPROM_Address::P_CHORUS_DEPTH, &parameter);

    return result;
}
