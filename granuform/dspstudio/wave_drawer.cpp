#include "wave_drawer.h"

void debug_break(const char *msg);

acl::AclGranuform* WaveDrawer::hw = nullptr;
uint8_t WaveDrawer::xPosition = 0;
uint8_t WaveDrawer::yPosition = 0;
uint8_t WaveDrawer::xResolution = 0;
uint8_t WaveDrawer::yResolution = 0;
float WaveDrawer::xPhasor = 0.0f;
float WaveDrawer::xPhasorInc = 0.0f;

WaveDrawer::WaveDrawer(acl::AclGranuform* const hwp)
{
	hw = hwp;
	xPosition = 0;
	yPosition = 0;
	xResolution = 0;
	yResolution = 0;
}

void WaveDrawer::setGeometry(const uint8_t x, const uint8_t y, const uint8_t width, const uint8_t height)
{
	xPosition = x;
	yPosition = y;
	xResolution = width;
	yResolution = height;
}

void WaveDrawer::setWaveName(std::string name)
{
	waveName = name;
}

void WaveDrawer::writeName()
{
	/* Show wav file name */
	hw->display.SetCursor(0, 0);
	hw->display.WriteString(waveName.data(), Font_6x8, true);
}