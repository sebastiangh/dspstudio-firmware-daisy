#pragma once

#include "acl_granuform.h"
#include <string>
#include <vector>

typedef struct
{
	std::string title;
	std::string value;
} MENU_ITEM;

typedef enum
{
    FOCUS_IDX0,
    FOCUS_IDX1,
    FOCUS_IDX2,
    FOCUS_IDX3,
    FOCUS_IDX4,
    FOCUS_IDX5
} FOCUS_INDEX;

/* Current view */
typedef enum
{
	HOME,
    MAIN_MENU
} ACTIVE_VIEW;

class Menu {
public:
    uint8_t currentPage;
    uint8_t pageCount;
    uint8_t itemsPerPage;

    void initItems(const char** parameterNames, float* values, size_t length);
    MENU_ITEM getItem(uint8_t index);
    uint8_t itemCount();
    void setItemValue(uint8_t index, float value);

private:
    std::vector<MENU_ITEM> items;
};

/* Manages menu pages and items */
class MenuHandler {
public:
    MenuHandler(acl::AclGranuform* const hw);
    void init(float* values, size_t length);
    static ACTIVE_VIEW getActiveView();
    void setPage(uint8_t page);
    void processEncoder(uint8_t index, float value);
    void processMainButton();
    void updateValue(float value);
    void drawMenu();
    uint8_t getActiveItem();
    void setActiveItem(uint8_t index);

private:
    acl::AclGranuform *hw;
    
    bool initialized;
    static ACTIVE_VIEW activeView;
    FOCUS_INDEX focus;
    FOCUS_INDEX lastFocus;
    Menu mainMenu;
    uint8_t deviceInternalIndex;
    uint8_t activeItem;
    
    void moveFocus(uint8_t index);
    void drawCursor(FOCUS_INDEX focus, bool visible);
    
};