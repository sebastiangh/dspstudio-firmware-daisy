#pragma once

#include "daisy_seed.h"

namespace acl
{

enum class EEPROM_Address
{
    ADC_VALUE_1V    = 0x0000,
    ADC_VALUE_3V    = 0x0008,
    P_SMOOTHNESS    = 0x0010,
    P_TRANSPOSE     = 0x0018,
    P_FINE_TUNE     = 0x0020,
    P_GRAN_LEVEL    = 0x0028,
    P_HP_CUTOFF     = 0x0030,
    P_HP_RESONANCE  = 0x0038,
    P_EXT_IN_LEVEL  = 0x0040,
    P_GRAN_CHORUS   = 0x0048,
    P_EXT_IN_CHORUS = 0x0050,
    P_CHORUS_SPEED  = 0x0058,
    P_CHORUS_DEPTH  = 0x0060
};

class Parameter
{
public:
    enum class Type
    {
        FLOAT,
        UINT_8A,
        UINT_32
    };

    enum class Key
    {
        SMOOTHNESS,
        TRANSPOSE,
        FINE_TUNE,
        GRAN_LEVEL,
        HP_CUTOFF,
        HP_RESONANCE,
        EXT_IN_LEVEL,
        GRAN_CHORUS,
        EXT_IN_CHORUS,
        CHORUS_SPEED,
        CHORUS_DEPTH
    };

    union Value
    {
        float f;
        uint8_t u8[4];
        uint32_t u32;
    };

    Parameter() = default;

    void setType(const Type& type)
    {
        type_ = type;
    }

    Type getType()
    {
        return type_;
    }

    template<typename T>
    void setValue(const T& value)
    {
        switch(type_)
        {
        case Type::FLOAT:
            value_.f = static_cast<float>(value);
            break;
        case Type::UINT_32:
            value_.u32 = static_cast<uint32_t>(value);
            break;
        default:
            break;
        }
    }

    
    const Value& getValue()
    {
        return value_;
    }

    static const uint8_t PARAMETER_SIZE{sizeof(float_t) + 1};

    Key key_{Key::SMOOTHNESS};

private:
    Type type_{Type::FLOAT};
    Value value_{};
};

/**
    @brief Class that handles external eeprom \n 
    @author Sebastian Gonzalez
    @date June 2022
    @ingroup boards
*/
class DataStorage
{
public:
    /** Constructor */
    DataStorage();

    /** Initialize GPIO and I2C Interface */
    bool Init();

    /** Write data to eeprom */
    daisy::I2CHandle::Result WriteData(uint32_t address, uint8_t pData[], uint32_t size);

    /** Read data from eeprom */
    daisy::I2CHandle::Result ReadData(uint32_t address, uint8_t pData[], uint32_t size);

    /** Load stored settings from EEPROM */
    daisy::I2CHandle::Result loadSettings();

    /** Restore default settings in EEPROM */
    daisy::I2CHandle::Result restore();

    /** Write parameter to eeprom */
    inline daisy::I2CHandle::Result WriteParameter(EEPROM_Address address , Parameter* value)
    {
        daisy::I2CHandle::Result result = daisy::I2CHandle::Result::ERR;
        uint8_t data[Parameter::PARAMETER_SIZE] = {
            static_cast<uint8_t>(value->getType()),
            value->getValue().u8[0],
            value->getValue().u8[1],
            value->getValue().u8[2],
            value->getValue().u8[3]
        };
        result = WriteData(static_cast<uint32_t>(address), data, Parameter::PARAMETER_SIZE);
        return result;
    }

    /** Read parameter from eeprom */
    inline daisy::I2CHandle::Result ReadParameter(EEPROM_Address address, Parameter* value)
    {
        daisy::I2CHandle::Result result = daisy::I2CHandle::Result::ERR;
        uint8_t data[Parameter::PARAMETER_SIZE] = {0};
        result = ReadData(static_cast<uint32_t>(address), data, Parameter::PARAMETER_SIZE);
        value->setType(static_cast<acl::Parameter::Type>(data[0]));
        for(int i=0; i<4; i++) {
            value->setValue(data[i+1]);
        }
        return result;
    }


private:
    daisy::I2CHandle i2cHandle_;
    daisy::I2CHandle::Config i2cConfig_;
    daisy::GPIO writeProtect_;
    
    /** EEPROM CAT24M01 I2C slave address from datasheet */
    const uint8_t DEV_ADDRESS{0x50}; // 7 bit address, must be left shifted once
};
}
