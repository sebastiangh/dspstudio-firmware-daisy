#pragma once
#include "daisy_patch.h"

namespace acl
{
/**
    @brief Class that handles initializing all of the hardware specific to the Granuform module. \n 
    Public member variables and functions are inherited from DaisyPatch.
    @author Sebastian Gonzalez
    @date December 2021
    @ingroup boards
*/
class AclGranuform : public daisy::DaisyPatch
{
  public:
    /** Enum of Ctrls to represent the six CV/Knob combos on the Granuform
     */
    enum GranuCtrl
    {
        GR_CTRL_1,    /**< */
        GR_CTRL_2,    /**< */
        GR_CTRL_3,    /**< */
        GR_CTRL_4,    /**< */
        GR_CTRL_5,    /**< */
        GR_CTRL_6,    /**< */
        GR_CTRL_LAST, /**< */
    };

    /** Constructor */
    AclGranuform() : daisy::DaisyPatch(){}
    
    /** Initializes the daisy seed, and Granuform hardware. */
    void InitGranu(bool boost = false);

    /** Call at same rate as reading controls for good reads. */
    void ProcessGranuAnalogControls();

    /** Calibration routine */
    void CalibrateCvInputs();

    daisy::Switch mainSwitch; /**< Switch object */
    daisy::GPIO mainLed;
    daisy::AnalogControl granuControls[GR_CTRL_LAST]; /**< Array of controls*/

  private:
    void SetHidUpdateRates();
    void InitAudio();
    void InitControls();
    void InitDisplay();
    void InitEncoder();
    void InitGates();
    void InitSwitchWithLED();

    uint32_t screen_update_last_, screen_update_period_;
};

} // namespace acl
