#include "daisysp.h"
#include "acl_granuform.h"
namespace dspstudio_device {
#include "dspstudio_generated.h"
}
#include "dspstudio_gluecode.h"
#include "dspstudio_wavloader.h"
#include "menu_handler.h"
#include "wave_drawer.h"
#include "printf.h"
#include "data_storage.h"
#include "system.h"

extern daisy::ApplicationTypeDef Appli_state;
extern constexpr float g_sampleRate{float(SR)};
extern constexpr float g_maxSample{1.0f};

static acl::AclGranuform hw;
static daisy::USBHostHandle usbMsc;
static daisy::FatFSInterface fatFs;
static wavloader::WavLoader loader;
static acl::DataStorage dataStorage;
static FIL file;
static MenuHandler menuHandler(&hw);
static WaveDrawer waveDrawer(&hw);
static bool diskMounted = false;
static bool menuInitialized = false;
static char line1[30];
static const std::string appVersion = "1.3.0";
wavloader::ErrorCode lastLoadTableError = wavloader::ErrorCode::Success;
size_t lastLoadTableFrames = 0;

// The dspstudio generated device
dspstudio_device::mod_device* g_dev = nullptr;

static void ProcessControls();
static void ProcessDisplay();

void debug_break(const char *msg) {
    hw.display.SetCursor(0, 0);
    hw.display.WriteString(msg, Font_6x8, true);
    hw.display.Update();
    hw.seed.SetLed(true);
    asm("bkpt 255");
}

namespace dspstudio_device {
void dspstudio_load_table(const char* filename, float** data, size_t* size) {
    if(f_open(&file, filename, FA_READ) == FR_OK)
    {
        loader.load(&file);
        wavloader::ErrorCode errorCode = loader.getErrorCode();
        size_t framesTotal = loader.getMetaData().frames;
        float* buffer = (float*) dspstudio_device::dspstudio_malloc(framesTotal * sizeof(float));
        size_t framesLoaded = loader.getChannelData(0, buffer, framesTotal);
        f_close(&file);

        for (size_t s = framesLoaded; s < framesTotal; s++) {
            buffer[s] = 0.0f;
        }

        *data = buffer;
        *size = framesTotal;

        lastLoadTableError = errorCode;
        lastLoadTableFrames = framesLoaded;
        // char line[128] = {0};
        // sprintf(line, "%d, %d", errorCode, framesLoaded);
        // debug_break(line);
    } else {
        lastLoadTableError = wavloader::ErrorCode::FileNotFound;
        lastLoadTableFrames = 0;
        debug_break("f_open() != FR_OK");
    }

}
}

static volatile bool firstTimeProcessed = false;
static void AudioCallback(const float* const* in, float** out, size_t size) {
    // cpuMeasurementBegin();
    ProcessControls();
    dspstudio_device::array_process_device(g_dev, in, 4, out, 4, size);
    firstTimeProcessed = true;
    // cpuMeasurementEnd();
}

int main(void)
{
    hw.InitGranu(cpuBoost);
    if(usbMsc.Init(daisy::USBHostHandle::Config())!= daisy::USBHostHandle::Result::OK) {
        debug_break("USB init failed");
    }
    if(fatFs.Init(daisy::FatFSInterface::Config::MEDIA_USB) != daisy::FatFSInterface::Result::OK) {
        debug_break("FatFS init failed");
    }
    if(!dataStorage.Init()) {
        debug_break("EEPROM init failed");
    }

    // Show version
    hw.display.Fill(false);
    hw.display.SetCursor(20, 20);
    hw.display.WriteString("ACL Granuform", Font_6x8, true);
    hw.display.SetCursor(20, 40);
    hw.display.WriteString(("Version " + appVersion).data(), Font_6x8, true);
    
    hw.display.Update();
    hw.DelayMs(3000);

    // Start the ADC for CV inputs
    hw.StartAdc();

    // Set geometry of main wave on display (x, y, width, height) in pixel
    waveDrawer.setGeometry(0, 8, 128, 20);

    // TODO: let user set the wav file
    waveDrawer.setWaveName(dspstudio_device::g_wavName.data());

    // if(dataStorage.loadSettings() != daisy::I2CHandle::Result::OK) {
    //     hw.display.Fill(false);
    //     hw.display.SetCursor(10, 0);
    //     hw.display.WriteString("Load settings failed", Font_6x8, true);
    //     hw.display.SetCursor(10, 20);
    //     hw.display.WriteString("Setting defaults...", Font_6x8, true);
    //     hw.display.Update();
    //     hw.DelayMs(1000);

    //     dataStorage.restore();
    //     if(dataStorage.loadSettings() != daisy::I2CHandle::Result::OK) {
    //         hw.display.Fill(false);
    //         debug_break("No settings loaded");
    //     }
    // }
    // Calibrate, skip with button
    // hw.CalibrateCvInputs();
    
    uint32_t last{0};
    for(;;)
    {
        /* Process USB Host until device is created */
        if(Appli_state != daisy::APPLICATION_DISCONNECT) {
            usbMsc.Process();
        }

        if(usbMsc.GetReady()) {
            if(!diskMounted) {
    			/* Register the file system object to the FatFs module */
                if(f_mount(&fatFs.GetUSBFileSystem(), "/", 1) == FR_OK)
    			{
                    diskMounted = true;
    			} else {
                    /* FatFs Initialization Error */
    				debug_break("f_mount != FR_OK");
                }
    			
                if(!g_dev && diskMounted) {
                    /* Do only once */

                    // TODO: Open dialog to choose wav file

                    // TODO: Create device after the user has chosen a file
                    g_dev = dspstudio_device::create();

                    // Run audio
                    InitializeAudioRates(hw.seed);
                    hw.StartAudio(AudioCallback);
                    
                    // Deinit USB host. It can be reinitialized if needed
                    usbMsc.Deinit();
                    Appli_state = daisy::APPLICATION_DISCONNECT;
                    diskMounted = false;
                }
    		} 
        }

        if(!menuInitialized && firstTimeProcessed) {
            /* Do only once and after first call of array_process_device() */
            menuHandler.init(g_dev->array_vals, sizeof(g_dev->array_vals) / sizeof(float));
            menuInitialized = true;
        }

        uint32_t now = daisy::System::GetUs();
        if (now - last > 40000) {
            ProcessDisplay();
            last = now;
        }
    }
}

/* These functions under namespace dspstudio_device are declared in file dspstudio_generated.h and used there */
namespace dspstudio_device {
void draw_point(dspstudio_device::DISPLAY display, float x, float y, float z, float w, float r, float g, float b, float a, float size) {
    if (x >= -1 && x <= 1 && y >= -1 && y <= 1) {
        if(MenuHandler::getActiveView() == HOME) {
            hw.display.DrawPixel(
                (x + 1.0f) * 0.5f * 60 + 32 + 2,
                (-y + 1.0f) * 0.5f * 60 + 2,
                1//grey > 0.5
            );
        }
    }
}
void indexUpdate(uint8_t index) {
    menuHandler.processEncoder(index, g_dev->array_vals[index]);
}

void mainSwitchPressed() {
    menuHandler.processMainButton();
    if(MenuHandler::getActiveView() == HOME) {
        g_homeView = true;
    } else if(MenuHandler::getActiveView() == MAIN_MENU) {
        g_homeView = false;
    }
}

void valueUpdate(float value) {
    menuHandler.updateValue(value);
}

void positionUpdate(float position) {
    wavePosition = position;
}

void enterEditMode(uint8_t index) {
    menuHandler.setActiveItem(index);
    g_activeEditing = true;
}

void leaveEditMode() {
    menuHandler.setActiveItem(0);
    g_activeEditing = false;
}
}

static void ProcessControls() {
    hw.ProcessDigitalControls();
    hw.ProcessGranuAnalogControls();
    hw.mainSwitch.Debounce();

    #ifdef _DSPSTUDIO_MAPPING_CONTROL_MAIN_SW
        _DSPSTUDIO_MAPPING_TYPE_MAIN_SW::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_MAIN_SW, hw.mainSwitch.Pressed() || hw.mainSwitch.RisingEdge(), hw.mainSwitch.FallingEdge() || hw.mainSwitch.RisingEdge());
    #endif 

    // Sensors: Ctrl 1-6
    {
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_KNOB_1
            _DSPSTUDIO_MAPPING_TYPE_KNOB_1::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_KNOB_1, hw.granuControls[acl::AclGranuform::GR_CTRL_1].Value(), true);
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_KNOB_2
            _DSPSTUDIO_MAPPING_TYPE_KNOB_2::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_KNOB_2, hw.granuControls[acl::AclGranuform::GR_CTRL_2].Value(), true);
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_KNOB_3
            _DSPSTUDIO_MAPPING_TYPE_KNOB_3::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_KNOB_3, hw.granuControls[acl::AclGranuform::GR_CTRL_3].Value(), true);
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_KNOB_4
            _DSPSTUDIO_MAPPING_TYPE_KNOB_4::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_KNOB_4, hw.granuControls[acl::AclGranuform::GR_CTRL_4].Value(), true);
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_KNOB_5
            _DSPSTUDIO_MAPPING_TYPE_KNOB_5::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_KNOB_5, hw.granuControls[acl::AclGranuform::GR_CTRL_5].Value(), true);
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_KNOB_6
            _DSPSTUDIO_MAPPING_TYPE_KNOB_6::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_KNOB_6, hw.granuControls[acl::AclGranuform::GR_CTRL_6].Value(), true);
        #endif
    }

    // Sensor: Encoder Knob
    {
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_ENC
            _DSPSTUDIO_MAPPING_TYPE_ENC::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_ENC, hw.encoder.Increment() == 1, hw.encoder.Increment() != 0);
        #endif
    }

    // Sensor: Encoder Button
    {
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_ENC_BUT
            if(MenuHandler::getActiveView() == MAIN_MENU) {
                _DSPSTUDIO_MAPPING_TYPE_ENC_BUT::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_ENC_BUT, hw.encoder.Pressed() || hw.encoder.RisingEdge(), hw.encoder.FallingEdge() || hw.encoder.RisingEdge());
            }
        #endif
    }

    // Sensor: Gate In 1-2
    {
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_GATE_IN_1
            _DSPSTUDIO_MAPPING_TYPE_GATE_IN_1::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_GATE_IN_1, hw.gate_input[acl::AclGranuform::GATE_IN_1].State(), true);
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_GATE_IN_2
            _DSPSTUDIO_MAPPING_TYPE_GATE_IN_2::set_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_GATE_IN_2, hw.gate_input[acl::AclGranuform::GATE_IN_2].State(), true);
            
        #endif
    }

    // Actors: CV Out 1-2
    {
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_CV_OUT_1
            hw.seed.dac.WriteValue(DacHandle::Channel::ONE, static_cast<uint16_t>(4095.99f * to_float(_DSPSTUDIO_MAPPING_TYPE_CV_OUT_1::get_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_CV_OUT_1))));
        #endif
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_CV_OUT_2
            hw.seed.dac.WriteValue(DacHandle::Channel::TWO, static_cast<uint16_t>(4095.99f * to_float(_DSPSTUDIO_MAPPING_TYPE_CV_OUT_2::get_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_CV_OUT_2))));
        #endif
    }

    // Actor: Gate Out
    {
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_GATE_OUT
            dsy_gpio_write(&hw.gate_output, to_bool(_DSPSTUDIO_MAPPING_TYPE_GATE_OUT::get_value(dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_GATE_OUT)));
        #endif
    }

    // Actor: Display
    {
        #ifdef _DSPSTUDIO_MAPPING_CONTROL_DISPLAY
            process_display(g_dev, dspstudio_device::_DSPSTUDIO_MAPPING_CONTROL_DISPLAY, g_dev->bufferSize);
        #endif
    }
}

static void ProcessDisplay() {

    static bool ledState = false;
    static bool menuOn = true;
    // float maxBlockTime = 1.0f * hw.AudioBlockSize() / hw.AudioSampleRate() * 1000 * 1000; // allowed time to pass for one block to be calculated
    
    // float wcet, avg;
    // cpuMeasurementEval(maxBlockTime, wcet, avg);

    // sprintf(line1, "AVG: %d.%02d%%", (int)avg, ((int)(avg * 100)) % 100);
    // sprintf(line2, "Frms: %d", (int)lastLoadTableFrames);
    
    /* Device has written to the framebuffer, display it's contents */
    hw.display.Update();

    /* Prepare next frame */
    switch(Appli_state) {
        case daisy::APPLICATION_IDLE:
            hw.display.Fill(false);
            hw.display.SetCursor(0, 0);
            hw.display.WriteString("Insert USB flash disk", Font_6x8, true);
            break;
        case daisy::APPLICATION_START:
            hw.display.Fill(false);
            hw.display.SetCursor(0, 0);
            hw.display.WriteString("Starting...", Font_6x8, true);
            break;
        case daisy::APPLICATION_READY:
        case daisy::APPLICATION_DISCONNECT:
            if(g_dev) {
                switch(MenuHandler::getActiveView()) {
                case HOME:
                    hw.display.DrawRect(0, 8, 128, 64, false, true);
                    waveDrawer.drawWaveform(g_dev->table_unnamed, static_cast<uint32_t>(g_dev->table_unnamed_size), dspstudio_device::wavePosition, 0.075f, true);
                    if(menuOn) {
                        hw.mainLed.Write(false);
                        hw.display.DrawRect(0, 0, 128, 7, false, true);
                        waveDrawer.writeName();
                        menuOn = false;
                    }
                    break;
                case MAIN_MENU:
                    if(!menuOn) {
                        hw.mainLed.Write(true);
                        hw.display.Fill(false);
                        menuHandler.drawMenu();
                        menuOn = true;
                    }
                    break;
                }

            } else {
                switch(lastLoadTableError) {
                case wavloader::Success:
                    sprintf(line1, "Success");
                    break;
                case wavloader::FileNotFound:
                    sprintf(line1, "FileNotFound");
                    break;
                case wavloader::BrokenFile:
                    sprintf(line1, "BrokenFile");
                    break;
                case wavloader::UnsupportedFormat:
                    sprintf(line1, "UnsupportedFmt");
                    break;
                case wavloader::InvalidChannel:
                    sprintf(line1, "InvalidChan");
                    break;
                }
                hw.display.SetCursor(0, 0);
                hw.display.WriteString(line1, Font_6x8, true);
            }
            break;
        default:
            break;
    }

    hw.seed.SetLed(ledState);
    ledState = !ledState;
}
